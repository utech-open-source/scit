#ifndef OUTPUT_H
#define OUTPUT_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "defines.h"

void setFileName(string fileName);

string getFileName();

/*
** create a file
*/
bool createFile();

/*
** open a file in append mode
*/
FILE * updateFile();

/*
** output
** displays data to screen and uses WriteToFile function
*/
void output(string data);

/*
** WriteToFile
**
*/
void writeToFile(string data );


#endif // OUTPUT_H

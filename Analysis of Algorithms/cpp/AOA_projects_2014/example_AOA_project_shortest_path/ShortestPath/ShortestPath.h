#ifndef SHORTEST_PATH_H
#define SHORTEST_PATH_H

#include "Heap.h"
#include "Vertice.h"

#define DISTANCE 1
#define TIME 2

const int verticeSize = 22;

//find shortest path
void shortestPath(Heap<Vertice *> * heap, int size, int type);

//show shortest path
void showShortestPath(Vertice * vertices[], int size, int type);

//return the vertices
void getVertices(Vertice * vertices[]);

#endif
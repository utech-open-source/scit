/**
* Generic Queue datastructure built from generic Link List
**/

#ifndef QUEUE
#define QUEUE

#include"List.h"

template <typename QUEUETYPE>
class Queue : private List<QUEUETYPE>
{
public:
	bool queueFront(QUEUETYPE & data){
        return this->getFront(data);
	}

	bool enqueue(QUEUETYPE & data){
        return this->insertAtBack(data);
	}

	bool dequeue(QUEUETYPE & data){
        return this->removeFromFront(data);
	}

	bool isQueueEmpty(){ 
        return this->isEmpty();
	}
};

#endif

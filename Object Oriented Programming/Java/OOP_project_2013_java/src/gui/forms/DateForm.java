package gui.forms;

import java.awt.Dimension;
import java.awt.GridLayout;

import gui.Resource;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class DateForm extends Form{
	/*
	 * Labels For Attributes in the Date class
	 */
	private JLabel dayLb;
	private JLabel monthLb;
	private JLabel yearLb;
	private String title;
	
	/*
	 * TextField For Attributes in the Date class
	 */
	private JTextField dayTf;
	private JTextField monthTf;
	private JTextField yearTf;
	
	public DateForm()
	{
		super();
		
		setTitle(Resource.dateTitle);
		
		setDayLb(Label(Resource.dayLb, 90, 30));
		setMonthLb(Label(Resource.monthLb, 90, 30));
		setYearLb(Label(Resource.yearLb, 90, 30));
		
		setDayTf(TextField(Resource.dayTf, 80, 30));
		setMonthTf(TextField(Resource.monthTf, 80, 30));
		setYearTf(TextField(Resource.yearTf, 80, 30));
	}

	/**
	 * @return the dayLb
	 */
	public JLabel getDayLb() {
		return dayLb;
	}

	/**
	 * @param dayLb the dayLb to set
	 */
	public void setDayLb(JLabel dayLb) {
		this.dayLb = dayLb;
	}

	/**
	 * @return the monthLb
	 */
	public JLabel getMonthLb() {
		return monthLb;
	}

	/**
	 * @param monthLb the monthLb to set
	 */
	public void setMonthLb(JLabel monthLb) {
		this.monthLb = monthLb;
	}

	/**
	 * @return the yearLb
	 */
	public JLabel getYearLb() {
		return yearLb;
	}

	/**
	 * @param yearLb the yearLb to set
	 */
	public void setYearLb(JLabel yearLb) {
		this.yearLb = yearLb;
	}

	/**
	 * @return the dayTf
	 */
	public JTextField getDayTf() {
		return dayTf;
	}

	/**
	 * @param dayTf the dayTf to set
	 */
	public void setDayTf(JTextField dayTf) {
		this.dayTf = dayTf;
	}

	/**
	 * @return the monthTf
	 */
	public JTextField getMonthTf() {
		return monthTf;
	}

	/**
	 * @param monthTf the monthTf to set
	 */
	public void setMonthTf(JTextField monthTf) {
		this.monthTf = monthTf;
	}

	/**
	 * @return the yearTf
	 */
	public JTextField getYearTf() {
		return yearTf;
	}

	/**
	 * @param yearTf the yearTf to set
	 */
	public void setYearTf(JTextField yearTf) {
		this.yearTf = yearTf;
	}
	
	public void addToPanel()
	{
		JPanel tmp = new JPanel(new GridLayout(1,1));
		
		tmp.add(wrapY(getDayLb(), getDayTf()));
		
		tmp.add(wrapY(getMonthLb(), getMonthTf()));
		
		tmp.add(wrapY(getYearLb(), getYearTf()));
		
		getFormPanel().add(tmp);
		
		getFormPanel().setMaximumSize(new Dimension(200, 100));
		
		setFormPanel(wrap(Label(getTitle()), getFormPanel()));
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}

#ifndef DEFINES_H
#define DEFINES_H

#ifndef _WINDOWS_VER
    #define _WINDOWS_VER
#endif

#ifndef string
    #define string char *
#endif

#define true 1
#define false 0
#ifndef NULL
    #define NULL 0
#endif

#ifndef bool
    #define bool int
#endif

#define WAITING 0
#define RUNNING 1

#define FILENAME_LIMIT 32

#define SYSTEM 1
#define INTERACTIVE 2
#define BATCH 3

#define PRIORITY_PRE 4
#define SHORTEST_REMAINING_BURST_TIME 5
#define ROUND_ROBIN 6

#define ROUND_ROBIN_QUANTUM 3

//determines which algorithm is ran by which process type
#define SYSTEM_ALGO SHORTEST_REMAINING_BURST_TIME
#define INTERACTIVE_ALGO ROUND_ROBIN
#define BATCH_ALGO PRIORITY_PRE

#endif // DEFINES_H

package gui.forms;

import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JComponent;

import logging.Log;
import gui.Event;
import gui.Resource;
import intervention.Address;
import intervention.Animal;
import intervention.Client;
import intervention.ClientVisit;
import intervention.Date;
import intervention.Intervention;
import intervention.RemovalRequest;

public class FormData {
	
	private static ArrayList<String[]> errors = new ArrayList<String[]>();
	
	/**
	 * @return the errors
	 */
	public static Object[] getErrors() {
		return errors.toArray();
	}

	/**
	 * 
	 * @param error
	 */
	public static void setError(String error, String msg) {
		FormData.errors.add(new String[]{error, msg});
	}

	/**
	 * @param errors the errors to set
	 */
	public static void setErrors(ArrayList<String[]> errors) {
		FormData.errors = errors;
	}
	
	public static String errorToString()
	{
		Object[] errors = getErrors();
		
		String invalidMsg = "";
		
		for(Object error : errors)
		{
			String[] out = (String[]) error;
			invalidMsg += out[0] +": "+ out[1] + "\n";
		}
		
		//clear the error message one it has being retrieved
		setErrors(new ArrayList<String[]>());
		
		return invalidMsg;
	}

	/**
	 * 
	 * @param form
	 * @return
	 */
	private static Animal getAnimalData(AnimalForm form)
	{
		Animal data = new Animal();
		
		try{
			//get age
			data.setAge(Integer.parseInt(form.getAgeTf().getText()));
				
		} catch (NumberFormatException e) {
			if(Resource.DEBUG)
				Log.LogError("Error getting age");
			setError("Animal "+form.getAgeLb().getText(), "Invalid value entered");
		}
		
		data.setBreed(form.getBreedTf().getText());
		data.setGender(form.getGenderTf().getSelectedItem().toString());
		
		String sel = form.getTypeTf().getSelectedItem().toString();
		
		if(sel.equalsIgnoreCase(Resource.defaultSelection) == false)
			data.setType(sel);
		else
			setError("Animal "+form.getTypeLb().getText(), FormValidation.selection);
		
		return data;
	}
	
	/**
	 * 
	 * @param form
	 * @param animal
	 */
	private static void setAnimalData(AnimalForm form, Animal animal)
	{	
		int animalIndex = getIndex(Resource.typeTf, animal.getType());
		int genderIndex = getIndex(Resource.genderTf, animal.getGender());
		
		form.getAgeTf().setText(animal.getAge()+"");
		form.getBreedTf().setText(animal.getBreed());
		
		if( animalIndex != -1 )
			form.getTypeTf().setSelectedIndex(animalIndex);
		
		if( genderIndex != -1 )
			form.getGenderTf().setSelectedIndex(genderIndex);
	}
	
	/**
	 * 
	 * @param form
	 * @return
	 */
	private static Client getClientData(ClientForm form)
	{
		Client data = new Client();
		
		data.setFname(form.getFirstNameTf().getText());
		data.setLname(form.getLastNameTf().getText());
		data.setContactNum(form.getContactNumTf().getText());
		
		//validate first name
		if( FormValidation.isblank(data.getFname()) )
		{
			setError("Client "+form.getFirstNameLb().getText(), FormValidation.blank);
		}
		
		//validate the last name
		if( FormValidation.isblank(data.getLname()) )
		{
			setError("Client "+form.getLastNameLb().getText(), FormValidation.blank);
		}
			
		//validate first number
		if(FormValidation.isPhoneNumber(form.getContactNumTf().getText()) == false)
		{
			setError("Client "+form.getContactNumLb().getText(), FormValidation.phone);
		}
		
		String sel = form.getPayTypeTf().getSelectedItem().toString();
		
		if(sel.equalsIgnoreCase(Resource.defaultSelection) == false)
			data.setPayType(sel);
		else
			setError("Client "+form.getPayTypeLb().getText(), FormValidation.selection);
		
		return data;
	}
	
	/**
	 * 
	 * @param form
	 * @param client
	 */
	private static void setClientData(ClientForm form, Client client)
	{
		int payTypeIndex = getIndex(Resource.payTypeTf, client.getPayType());
		
		form.getFirstNameTf().setText(client.getFname());
		form.getLastNameTf().setText(client.getLname());
		form.getContactNumTf().setText(client.getContactNum());
		
		if(payTypeIndex != -1)
			form.getPayTypeTf().setSelectedIndex(payTypeIndex);
	}
	
	/**
	 * 
	 * @param form
	 * @return
	 */
	private static Date getDateData(DateForm form)
	{
		Date data = new Date();
		
		try {
			
			data.setDay(Integer.parseInt(form.getDayTf().getText().trim()));
			data.setMonth(Integer.parseInt(form.getMonthTf().getText().trim()));
			data.setYear(Integer.parseInt(form.getYearTf().getText().trim()));
			
		} catch (NumberFormatException e) {
			if(Resource.DEBUG)
				Log.LogError("Error getting date");
			
			setError(form.getTitle(), FormValidation.date);
		}
		
		//validate date lengths
		if(!FormValidation.checkDate(data))
			setError(form.getTitle(), FormValidation.date);	
		
		return data;
	}
	
	/**
	 * 
	 * @param form
	 * @param date
	 */
	private static void setDateData(DateForm form, Date date)
	{
		form.getDayTf().setText(date.getDay() + "");
		form.getMonthTf().setText(date.getMonth() + "");
		form.getYearTf().setText(date.getYear()+"");
	}
	
	/**
	 * 
	 * @param form
	 * @return
	 */
	private static Address getAddressData(AddressForm form)
	{
		Address data = new Address();
		
		//check if address is selection
		if(form.isSlection)
		{
			String sel = form.getAddressTf().getSelectedItem().toString();
			
			if(sel.equalsIgnoreCase(Resource.defaultSelection) == false)
			{
				String [] selection = Resource.winchesterRoad;
				
				if(form.getAddressTf().getSelectedItem().toString().
							compareTo(Resource.caymanasTrackLimited[1]) == 0)
				{
					selection = Resource.caymanasTrackLimited;					
				}
				
				//add addresss appropriately
				data.setLotNumber(selection[0]);
				data.setStreet(selection[1]);
				data.setParish(selection[2]);
			}
			else
				setError(form.getAddressSl().getText(), FormValidation.selection);
		}// else not a selection query the address from
		else{
			data.setLotNumber(form.getLotNumTf().getText());
			
			String sel = form.getParishTf().getSelectedItem().toString();
			
			if(sel.equalsIgnoreCase(Resource.defaultSelection) == false)
				data.setParish(sel);
			else
				setError(form.getParishLb().getText(), FormValidation.selection);
			
			data.setStreet(form.getStreetTf().getText());
		}
		
		return data;
	}
	
	/**
	 * 
	 * @param form
	 * @param address
	 */
	private static void setAddressData(AddressForm form, Address address)
	{	
		int parishIndex = getIndex(Resource.parishTf, address.getParish());
		int addressIndex = getIndex(Resource.addressTf, address.getStreet());
		
		form.getLotNumTf().setText(address.getLotNumber());
		
		if( parishIndex != -1 )
			form.getParishTf().setSelectedIndex(parishIndex);
		
		form.getStreetTf().setText(address.getStreet());
		
		if( addressIndex != -1)
			form.getAddressTf().setSelectedIndex(addressIndex);
	}
	
	/**
	 * 
	 * @param form
	 * @return
	 */
	private static Intervention getInterventionData(BaseForm form)
	{
		Intervention data = new Intervention();
		
		Client client = getClientData(form.getClientForm());
		Date date = getDateData(form.getDateForm());
		Address address =  getAddressData(form.getAddressForm());
		
		data.setAddress(address);
		data.setClient(client);
		data.setDate(date);
		data.setIdCode(form.getIdCodeTf().getText());
		data.setTime(form.getTimeTf().getText());
		
		return data;
	}
	
	
	private static void setInterventionData(BaseForm form, Intervention intervention)
	{
		form.getIdCodeTf().setText(intervention.getIdCode());
		form.getTimeTf().setText(intervention.getTime());
		
		setAddressData(form.getAddressForm(), intervention.getAddress());
		setDateData(form.getDateForm(), intervention.getDate());
	}
	
	/**
	 * 
	 * @param form
	 * @return
	 */
	public static ClientVisit getClientVisitData(ClientVisitForm form)
	{
		Intervention intervention = getInterventionData(form);
		Animal animal = getAnimalData(form.getAnimalForm());
		Date visitDate = getDateData(form.getVisitDateForm());
		
		ClientVisit data = new ClientVisit(intervention.getIdCode(),
					intervention.getClient(), intervention.getDate(),
					intervention.getTime(), intervention.getAddress(),
					animal, form.getReasonTf().getText(), visitDate,
					0
				);
		
		return data;
	}
	
	
	/**
	 * 
	 * @param form
	 * @param clientVisit
	 */
	public static void setClientVisitData(ClientVisitForm form, ClientVisit clientVisit)
	{
		setAnimalData(form.getAnimalForm(), clientVisit.getAnimal());
		setDateData(form.getVisitDateForm(), clientVisit.getVisitDate());
		setInterventionData(form, clientVisit);
		setClientData(form.getClientForm(), clientVisit.getClient());
		form.getReasonTf().setText(clientVisit.getReason());
	}
	
	/**
	 * @param form
	 * @return
	 */
	public static RemovalRequest getRemovalRequestData(RemovalRequestForm form)
	{		
		Intervention intervention = getInterventionData(form);
		
		String sel = form.getOutcomeTf().getSelectedItem().toString();
		
		if(sel.equalsIgnoreCase(Resource.defaultSelection) == false)
			sel = form.getOutcomeTf().getSelectedItem().toString();
		else
			setError(form.getOutcomeLb().getText(), FormValidation.selection);

		RemovalRequest data = new RemovalRequest(intervention.getIdCode(),
					intervention.getClient(), intervention.getDate(),
					intervention.getTime(), intervention.getAddress(),
					sel,
					0
				);
		
		return data;
	}
	
	
	/**
	 * 
	 * @param form
	 * @param clientVisit
	 */
	public static void setRemovalRequestData(RemovalRequestForm form, RemovalRequest removalRequest)
	{
		int outcomeIndex = getIndex(Resource.outcomeTf, removalRequest.getOutcome());
		
		setInterventionData(form, removalRequest);
		setClientData(form.getClientForm(), removalRequest.getClient());
		
		if( outcomeIndex != -1)
			form.getOutcomeTf().setSelectedItem(removalRequest.getOutcome());
	}

	
	/**
	 * 
	 * @param form
	 * @return
	 */
	public static Object[] getReportFormData(ClientVisitReportForm form)
	{
		Object [] obj;
				
		Animal animal = getAnimalData(form.getAnimalForm());
		Date visitDate = getDateData(form.getVisitDateForm());
		Address address =  getAddressData(form.getAddressForm());
		
		obj = new Object[]{address, animal, visitDate};
		
		return obj;
	}
	
	/**
	 * 
	 * @param form
	 * @return
	 */
	public static Object[] getReportFormData(RemovalRequestReportForm form)
	{
		Object [] obj;
				
		String outcome = form.getOutcomeTf().getSelectedItem().toString();

		if(outcome.equalsIgnoreCase(Resource.defaultSelection))
			outcome = "";
		
		Date date = getDateData(form.getDateForm());
		Address address =  getAddressData(form.getAddressForm());
		
		obj = new Object[]{address, outcome, date};
		
		return obj;
	}
	
	/**
	 * 
	 * @param items
	 * @param item
	 * @return
	 */
	public static int getIndex(String [] items, String item)
	{
		for(int i=0; i < items.length; i++)
		{
			if(items[i].equalsIgnoreCase(item))
			{
				return i;
			}
		}
		
		return -1;
	}
	
	/**
	 * 
	 * @param form
	 */
	public static void addCurDate(DateForm form)
	{
		//add current date to base form
		//split the time portion off
		Date date = Date.fromString( FormValidation.getDate(null).split(" ")[0] );
		form.getDayTf().setText(date.getDay()+"");
		form.getMonthTf().setText(date.getMonth()+"");
		form.getYearTf().setText(date.getYear()+"");
	}
	
	
	/**
	 * 
	 * @param base
	 */
	public static void modifyForm(BaseForm base)
	{
		//change some of the base form attributes
		base.getFormPanel().setBorder(BorderFactory.createEmptyBorder(20, 30, 20, 20));
		base.getFormPanel().setAlignmentX(JComponent.CENTER_ALIGNMENT);	
		
		//add intervention id to the base form
		base.getIdCodeTf().setText((Event.IP.getDb().getDatabase().getLastInsert("Intervention") + 1)+"");
		
		//add current date to form
		FormData.addCurDate(base.getDateForm());
		
		//add current time to base form
		base.getTimeTf().setText( FormValidation.getDate(null).split(" ")[1] );
	}
}

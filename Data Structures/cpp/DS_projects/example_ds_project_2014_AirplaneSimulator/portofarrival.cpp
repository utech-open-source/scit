#include "portofarrival.h"
// -- Constructor --
// default constructor
PortOfArrival::PortOfArrival():
		cursor(0), total(0) {
	strcpy_s(this->name, "PORT ?\0");
	// initialise the acceptance list
	this->lstAccept[0] = true;
	this->lstAccept[1] = true;
	this->lstAccept[2] = false;
	this->lstAccept[3] = true;
	this->lstAccept[4] = false;
	SYSTEM->display(IDC_TEXT_DISPLAY_PORT, " '"+ string(this->name)+"' Is Ready For AirPlanes.", 1);
}
// default constructor
PortOfArrival::PortOfArrival(char* n):
		cursor(0), total(0) {
	if (n != NULL)
		strcpy_s(this->name, n);
	else
		strcpy_s(this->name, "PORT ?\0");
	// initialise the acceptance list
	this->lstAccept[0] = true;
	this->lstAccept[1] = true;
	this->lstAccept[2] = false;
	this->lstAccept[3] = true;
	this->lstAccept[4] = false;
	SYSTEM->display(IDC_TEXT_DISPLAY_PORT, " '"+ string(this->name)+"' Is Ready For AirPlanes.", 1);
}
// -- Other Methods --
// attempts to add an airplane to the port
void PortOfArrival::addAirPlane(AirPlane* p) {
	if (p != NULL) {
		this->total += 1;
		SYSTEM->display(IDC_TEXT_DISPLAY_PORT, " '" + string(this->name) + "' Accepted AirPlane#"+ SYSTEM->toString<long>(p->getIdnum())+".", 1);
	}
}

/*
* function efficency O(1)
* tells whether this port currently can accept an airplane
*/
bool PortOfArrival::canAccept() {
	if (this->lstAccept[this->cursor%5]) {
		SYSTEM->display(IDC_TEXT_DISPLAY_PORT, " '" + string(this->name) + "' Can Accept AirPlanes.", 1);
	} else {
		SYSTEM->display(IDC_TEXT_DISPLAY_PORT, " '" + string(this->name) + "' Cannot Accept AirPlanes.", 2);
	}
	this->cursor = this->cursor % 5;
	return (this->lstAccept[this->cursor++]);
}

/*
* function efficency O(1)
* returns the total number of airplanes that have used this port
*/
int PortOfArrival::totalAccepted() const {
	return this->total;
}

// returns the name of the port currently trying to accepting airplane
char * PortOfArrival::getName() const{
	return (char *)this->name;
}
#ifndef JFF_AIRPLANEGEN_H
#define JFF_AIRPLANEGEN_H

#define TOTALPLANES 32//273

#include <vector>
#include <time.h>
#include <iostream>
#include "airplane.h"
#include "utility.h"
using namespace std;
// sets the begin time to the current system time
class AirPlaneGenerator {
	private:
		// the list of airplanes to be served
		static vector<AirPlane> lstPlanes;
		int cursor;									// used as a marker for the current airplane to be served
		int interval;								// the time interval (passes) before releasing another plane
		int elapsed;						  // the time elapsed (passes) since the last airplane was generated

	public:
	  // -- Constructors --
		AirPlaneGenerator(int=14);	// primary constructor
		// -- Other Interfaces --
		AirPlane* getNext();				// get the next task in the list
};
#endif
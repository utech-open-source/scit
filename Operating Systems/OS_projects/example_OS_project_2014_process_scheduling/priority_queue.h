#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

#include "defines.h"
#include "process_control_block.h"

/*
** Node to create pcb queue
*/
typedef struct Node PCBNode, * PCBNodePtr;
struct Node{
	PCBPtr block;
	PCBNodePtr nextNode;
	PCBNodePtr prevNode;
};

/*
** Process queue front and black
*/
typedef struct Queue PQueue, * PQueuePtr;
struct Queue{
    PCBNodePtr front;
    PCBNodePtr back;
};

/*
** check if pcb queue is full
*/
bool isFull();

/*
** check if pcb queue is empty
*/
bool isEmpty(PCBNodePtr front);

/*
** insert at the back of pcb queue
*/
bool enqueue(PCBPtr node, PQueuePtr ptr);

/*
** remove pcb from the front of queue
*/
PCBPtr dequeue(PQueuePtr ptr);

/*
** create a new pcb node pointer
*/
PCBNodePtr newNode(PCBPtr block);

/*
** create Process queue by initializing the the values
** to null
*/
void createPQueue(PQueuePtr ptr);

/*
** insert value in to queue based on key
*/
bool insert(int key, PCBPtr node, PQueuePtr ptr);

/*
** min the min value of the queue
*/
bool min(PCBPtr node, PQueuePtr ptr);

/*
** remove the min node
*/
PCBPtr removeMin(PQueuePtr ptr);

/*
** shift the value of node up based on the key
*/
void shiftUp(int key, PCBNodePtr node);

#endif // PRIORITY_QUEUE_H

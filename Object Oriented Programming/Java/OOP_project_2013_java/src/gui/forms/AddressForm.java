package gui.forms;

import gui.Resource;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AddressForm extends Form{
	/*
	 * Labels For Attributes in the Address class
	 */
	private JLabel lotNumLb;
	private JLabel streetLb;
	private JLabel parishLb;

	private JLabel addressSl;
	
	/*
	 * TextField For Attributes in the Address class
	 */
	private JTextField lotNumTf;
	private JTextField streetTf;
	private JComboBox<String> parishTf;
	private JComboBox<String> addressTf;
	
	public boolean isSlection = false;
	
	public AddressForm()
	{
		super();
		
		//set section title
		setSecTitle(Resource.secAddressTitle);
		
		setLotNumLb(Label(Resource.lotNumLb));
		setStreetLb(Label(Resource.streetLb));
		setParishLb(Label(Resource.parishLb));
		
		setLotNumTf(TextField(Resource.lotNumTf));
		setStreetTf(TextField(Resource.streetTf));
		setParishTf(ComboBox(Resource.parishTf));
		
		setAddressSl(Label("Address"));
		setAddressTf(ComboBox(Resource.addressTf));
	}


	/**
	 * @return the lotNumLb
	 */
	public JLabel getLotNumLb() {
		return lotNumLb;
	}


	/**
	 * @param lotNumLb the lotNumLb to set
	 */
	public void setLotNumLb(JLabel lotNumLb) {
		this.lotNumLb = lotNumLb;
	}


	/**
	 * @return the streetLb
	 */
	public JLabel getStreetLb() {
		return streetLb;
	}


	/**
	 * @param streetLb the streetLb to set
	 */
	public void setStreetLb(JLabel streetLb) {
		this.streetLb = streetLb;
	}


	/**
	 * @return the parishLb
	 */
	public JLabel getParishLb() {
		return parishLb;
	}


	/**
	 * @param parishLb the parishLb to set
	 */
	public void setParishLb(JLabel parishLb) {
		this.parishLb = parishLb;
	}


	/**
	 * @return the lotNumTf
	 */
	public JTextField getLotNumTf() {
		return lotNumTf;
	}


	/**
	 * @param lotNumTf the lotNumTf to set
	 */
	public void setLotNumTf(JTextField lotNumTf) {
		this.lotNumTf = lotNumTf;
	}


	/**
	 * @return the streetTf
	 */
	public JTextField getStreetTf() {
		return streetTf;
	}


	/**
	 * @param streetTf the streetTf to set
	 */
	public void setStreetTf(JTextField streetTf) {
		this.streetTf = streetTf;
	}


	/**
	 * @return the parishTf
	 */
	public JComboBox<String> getParishTf() {
		return parishTf;
	}


	/**
	 * @param parishTf the parishTf to set
	 */
	public void setParishTf(JComboBox<String> parishTf) {
		this.parishTf = parishTf;
	}
	
	/**
	 * @return the addressSl
	 */
	public JLabel getAddressSl() {
		return addressSl;
	}


	/**
	 * @param addressSl the addressSl to set
	 */
	public void setAddressSl(JLabel addressSl) {
		this.addressSl = addressSl;
	}


	/**
	 * @return the addressTf
	 */
	public JComboBox<String> getAddressTf() {
		return addressTf;
	}


	/**
	 * @param addressTf the addressTf to set
	 */
	public void setAddressTf(JComboBox<String> addressTf) {
		this.addressTf = addressTf;
	}

	/**
	 * Override
	 */
	public void addToPanel()
	{
		if(isSlection)
		{
			getFormPanel().add(wrap(getAddressSl(), getAddressTf()));
		}
		else
		{
			getFormPanel().add(wrap(getLotNumLb(), getLotNumTf()));
			
			getFormPanel().add(wrap(getStreetLb(),getStreetTf()));
			
			getFormPanel().add(wrap(getParishLb(),getParishTf()));
		}
	}
}

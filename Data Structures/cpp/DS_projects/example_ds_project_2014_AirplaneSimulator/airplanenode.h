#ifndef AIRPLANENODE_H
#define AIRPLANENODE_H

#include "airplane.h"

class AirPlaneNode{

private:
	AirPlane * airPlane;
	AirPlaneNode * next;
	AirPlaneNode * prev;

public:
	AirPlaneNode(); //default constructor
	AirPlaneNode(AirPlane * = NULL, AirPlaneNode * = NULL, AirPlaneNode * = NULL);
	//Accessors
	AirPlaneNode * getNext() const;
	AirPlaneNode * getPrev() const;
	AirPlane * getAirPlane() const;
	//Mutators
	void setPrev(AirPlaneNode *);
	void setNext(AirPlaneNode *);
	void setAirPlane(AirPlane *);
};

#endif
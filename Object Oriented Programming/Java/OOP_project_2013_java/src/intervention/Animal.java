package intervention;

public class Animal {
	private String type;
	private String breed;
	private String gender;
	private int age;
	private int animalId;
	
	public Animal()
	{
		type="";
		breed="";
		gender="";
		age=0;
		animalId = 0;
	}
	
	public Animal(String typ,String bree, String gende,int ag, int id)
	{
		type = typ;
		breed = bree;
		gender = gende;
		age = ag;
		animalId = id;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBreed() {
		return breed;
	}
	public void setBreed(String breed) {
		this.breed = breed;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	public int getAnimalId() {
		return animalId;
	}

	public void setAnimalId(int animalId) {
		this.animalId = animalId;
	}
	

}

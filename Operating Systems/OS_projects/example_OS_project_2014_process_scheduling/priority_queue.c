#include "priority_queue.h"

/*
** remove pcb from the front of queue
*/
PCBPtr dequeue(PQueuePtr ptr)
{
    PCBPtr block = NULL;
	if(!isEmpty(ptr->front))
	{
		PCBNodePtr temp = ptr->front;
		if(ptr->front == ptr->back)
		{
			ptr->back = NULL;
		}
		ptr->front = ptr->front->nextNode;
		if(!isEmpty(ptr->front))
		{
			ptr->front->prevNode = NULL;
		}
		//check here
		block = temp->block;
		//free(temp);
		//displayProcessControlBlock( *block );
	}
	return block;
}

/*
** insert at the back of pcb queue
*/
bool enqueue(PCBPtr block, PQueuePtr ptr)
{
	if(!isFull())
	{
		PCBNodePtr temp = newNode(block);

		if(isEmpty(ptr->front))
		{
			ptr->back = ptr->front = temp;
		}
		else {
			ptr->back->nextNode = temp;
			temp->prevNode = ptr->back;
			ptr->back = temp;
		}

		//displayProcessControlBlock( *ptr->front->block );
		return true;
	}
	return false;
}

/*
** check if pcb queue is full
*/
bool isFull()
{
	PCBNodePtr temp = (PCBNodePtr)malloc(sizeof(PCBNode));
	if(temp == NULL)
	{
		return true;
	}
	free(temp); //free memory allocated by malloc
	return false;
}

/*
** check if pcb queue is empty
*/
bool isEmpty(PCBNodePtr front)
{
	return ( front == NULL );
}

/*
** create a new pcb node pointer
*/
PCBNodePtr newNode(PCBPtr block)
{
	PCBNodePtr temp = (PCBNodePtr)malloc(sizeof(PCBNode));
	temp->block = block;
	temp->nextNode = NULL;
	temp->prevNode = NULL;
	return temp;
}

/*
**
*/
void createPQueue(PQueuePtr ptr)
{
    ptr->back = NULL;
    ptr->front = NULL;
}

/*
** insert value in to queue based on key
*/
bool insert(int key, PCBPtr node, PQueuePtr ptr)
{
    if( enqueue(node, ptr) )
    {
        shiftUp(key, ptr->back);
        return true;
    }
    return false;
}

/*
** min the min value of the queue
*/
bool min(PCBPtr node, PQueuePtr ptr)
{
	if(!isEmpty(ptr->front))
	{
		*node = *ptr->front->block;
		return true;
	}
	return false;
}

/*
** remove the min node
*/
PCBPtr removeMin(PQueuePtr ptr)
{
    return dequeue(ptr);
}

/*
** shift the value of node up based on the key
*/
void shiftUp(int key, PCBNodePtr node)
{
    PCBNodePtr tmp = node;

    while(tmp != NULL )
    {
        PCBPtr tmpBlock = tmp->block;

        if(tmp->prevNode != NULL)
        {

            switch(key)
            {
            case PRIORITY_PRE:
                    //swap
                    if(tmp->block->priority < tmp->prevNode->block->priority)
                    {
                        tmp->block = tmp->prevNode->block;
                        tmp->prevNode->block = tmpBlock;
                    }
                break;
            case SHORTEST_REMAINING_BURST_TIME:
                    //swap
                    if(tmp->block->remaining_burst_time < tmp->prevNode->block->remaining_burst_time)
                    {
                        tmp->block = tmp->prevNode->block;
                        tmp->prevNode->block = tmpBlock;
                    }
                break;
            default:
                return;
                break;
            }
        }
        tmp = tmp->prevNode;
    }
}

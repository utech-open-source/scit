#ifndef TREE_H
#define TREE_H

#include "Node.h"

class Tree{

private:
	Node * root;

public:
	Tree();

	Node * getRoot() const;
	void insertNode(int);
	bool isEmpty();
	bool isFull();

	void preOrderTraversal(Node *);
	void inOrderTraversal(Node *);
	void postOrderTraversal(Node *);

protected:
	void display(Node *);


};
#endif
#ifndef SCHEDULING_QUEUE_H
#define SCHEDULING_QUEUE_H

#include "defines.h"
#include "priority_queue.h"

/*
** creates the sheduling queues
*/
void createShedulingQueues();

/*
** Add pcb to the sheduling queue
*/
//bool AddToShedulingQueue(PCBptr block, PCBNodePtr SQFront, PCBNodePtr SQBack);

/*
** Remove pcb from the front of the sheduling queue
*/
//bool RemoveFromShedulingQueue(PCBptr block, PCBNodePtr SQFront, PCBNodePtr SQBack);

/*
** get copy of the front of the sheduling queue
*/
//bool FrontOfShedulingQueue(PCBptr block, PCBNodePtr SQFront);

/* --------------------SYSTEM--------------------------------------*/

/*
** Add pcb to the system sheduling queue
*/
bool AddToShedulingQueueSys(int key, PCBPtr block);

/*
** Remove pcb from the front of the system sheduling queue
*/
PCBPtr RemoveFromShedulingQueueSys();

/*
** get copy of the front of the system sheduling queue
*/
bool FrontOfShedulingQueueSys(PCBPtr block);

int getSQSysCnt();

/* --------------------INTERACTIVE--------------------------------*/

/*
** Add pcb to the interactive sheduling queue
*/
bool AddToShedulingQueueInt(int key, PCBPtr block);

/*
** Remove pcb from the front of the interactive sheduling queue
*/
PCBPtr RemoveFromShedulingQueueInt();

/*
** get copy of the front of the interactive sheduling queue
*/
bool FrontOfShedulingQueueInt(PCBPtr block);

int getSQIntCnt();


/* --------------------Batch-------------------------------------*/

/*
** Add pcb to the batch sheduling queue
*/
bool AddToShedulingQueueBat(int key, PCBPtr block);

/*
** Remove pcb from the front of the batch sheduling queue
*/
PCBPtr RemoveFromShedulingQueueBat();

/*
** get copy of the front of the batch sheduling queue
*/
bool FrontOfShedulingQueueBat(PCBPtr block);

int getSQBatCnt();

/*-----------------------------------------------------------------*/
/*
** Add a process to its correct sheduling queue
*/
bool AddProcessToShedulingQueue(PCBPtr block);

#endif

#ifndef LANDINGROAD_H
#define LANDINGROAD_H

#define _Mail_ "Mail"
#define _PassSmall_ "Passenger Small"
#define _PassLarge_ "Passenger Large"
#define _Govern_ "Government"
#define _VIP_ "VIP"

#include "airplanenode.h"
#include "utility.h"

class LandingRoad{
	private:
		string name;
		int maxSize;
		int currSize;
		string priorityType;
		AirPlaneNode * front;
		AirPlaneNode * back;
	public:
		LandingRoad(string = "", int = 0, int = 0, string = "");
		//LandingRoad(LandingRoad*); // copy constructor
		//setters
		void setName(string);
		void setMaxSize(int);
		void setCurrSize(int);
		void setPriorityType(string);
		void setFront(AirPlaneNode *);
		void setBack(AirPlaneNode *);
		//getters
		string getName() const;
		int getMaxSize() const;
		int getCurrSize() const;
		string getPriorityType() const;
		AirPlaneNode * getFront() const;
		AirPlaneNode * getBack() const;
		//utility functions
		bool enqueue(AirPlane *);
		AirPlane * dequeue();
		//Airplane * queueFront();
		//Airplane * queueBack();
		bool isEmpty();
		bool isFull();
};

#endif
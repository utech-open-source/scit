/**
* author: Mark Robinson
* date: 2/13/2014
**/

#include<cstdlib>
#include"search.h"
#include"utility.h"

using namespace std;

void main()
{

	int A[] = {1, 5, 7, 8, 2, 3, 10, 9};

	//question 2

	char Alphabeth[] = {'c', 'z', 'h', 'm', 'f', 'a', 'p', 'b', 'd', 
							'e', 'g', 'i', 'j', 'k', 'l', 'n', 'q', 's', 
									'o', 'r','t', 'v', 'w', 'x', 'u', 'y'};

	//question 3
	double effs[] = {1.2, 2.3, 3.4, 4.5, 5.6, 6.7, 
						7.8, 8.9, 9.10, 10.11, 11.12 };

	//question 4
	int nums[][3] = {{5, 7, 3}, 
						{2, 4, 1}, 
							{3, 0, 5} };


	int sizeAlphabeth = sizeof(Alphabeth)/sizeof(char);
	int sizeEffs = sizeof(effs)/sizeof(effs[0]);


	printArray<char>(Alphabeth, sizeAlphabeth);

	printArray<double>(effs, sizeEffs);

	//Search algorithms test
	Search<char>search(Alphabeth, sizeAlphabeth);

	//test the sorting
	Sort <char>sort(Alphabeth, sizeAlphabeth);

	sort.bubble();

	printArray<char>(sort.getArray(), sizeAlphabeth);

	cout<<"search.binary('d') index: "<<search.binary('d')<<endl;

	//Sort <int>sort(A, 8);

	//sort.insertion();

	//printArray<int>(sort.getArray(), 8);

	//question 11

	printArray<int>(&nums[0][0], 3, 3);

	//question 13
	printRand<double>(effs, sizeEffs, 2);

	system("pause");

}
package logging;

import gui.Resource;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Log {

	private static LogManager logManager = LogManager.getLogManager();
	private static String logName = "Intervention";
	
	public static PrintStream stdout = System.out;
	public static PrintStream stderr = System.err;
	
	private static Log log;
	private static boolean isChanged = false;
	
	private Log() throws SecurityException, IOException
	{
		//check if log file exist and remove
		File fp = new File("log");
		if(fp.exists())
			fp.delete();
		
        // initialize logging to go to rolling log file
        logManager.reset();

        // log file max size 10K, 1 rolling files, append-on-open
        Handler fileHandler = new FileHandler("log", 10000, 1, true);
        fileHandler.setFormatter(new SimpleFormatter());
        Logger.getLogger(logName).addHandler(fileHandler);
		
		// now rebind stdout/stderr to logger
		Logger logger;
		LoggingOutputStream los;
	
		logger = Logger.getLogger("stdout");
		los = new LoggingOutputStream(logger, StdOutErrLevel.STDOUT);
		System.setOut(new PrintStream(los, true));
		
		logger = Logger.getLogger("stderr");
		los= new LoggingOutputStream(logger, StdOutErrLevel.STDERR);
		System.setErr(new PrintStream(los, true));
	}
	
	//initialize logging here
	public static void Initialize() throws SecurityException, IOException
	{
		log = new Log();
	}
	
	private void LogMsg(String msg)
	{
		if(Resource.DEBUG)
		{	
			Logger logger;
			logger = Logger.getLogger(logName);
			logger.info(msg+"\n");
			
			isChanged = true;
		}
	}
	
	/**
	 * 
	 * @param msg
	 */
	public static void LogError(String msg)
	{
		log.LogMsg("Error"+" : "+msg);
	}
	
	/**
	 * 
	 * @param msg
	 */
	public static void LogInfo(String msg)
	{
		log.LogMsg(msg);
	}
	
	public static void reset()
	{
		Log.logManager.reset();
	}
	
	public static boolean changed()
	{
		return isChanged;
	}

	/**
	 * 
	 * @param b
	 */
	public static void IsChanged(boolean b) {
		isChanged = b;
	}
}

package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import logging.Log;
import intervention.Address;
import intervention.Animal;
import intervention.Client;
import intervention.ClientVisit;
import intervention.Date;
import intervention.Intervention;
import intervention.RemovalRequest;

public class InterventionDb {

	private SqlLite database;
	private boolean updateQuery;
	
	
	public InterventionDb()
	{
		setDatabase(new SqlLite("intervention", false));
		
		//connect to database
		getDatabase().connect();
		
		//check if database exits
		if(!getDatabase().exist())
		{
			Log.LogInfo("Attempting to create Database");
			create();
		}
	}


	/**
	 * @return the database
	 */
	public SqlLite getDatabase() {
		return database;
	}


	/**
	 * @param database the database to set
	 */
	public void setDatabase(SqlLite database) {
		this.database = database;
	}
	
	
	/**
	 * @return the updateQuery
	 */
	public boolean isUpdateQuery() {
		return updateQuery;
	}


	/**
	 * @param updateQuery the updateQuery to set
	 */
	public void setUpdateQuery(boolean updateQuery) {
		this.updateQuery = updateQuery;
	}
	
	
	private void create()
	{
	      String sql = 
                  "CREATE TABLE Address "+
                  "(addressId INTEGER PRIMARY KEY    AUTOINCREMENT," +
                  " lotNum        CHAR(50), " + 
                  " street        CHAR(50), " + 
                  " parish        CHAR(50) );"+
                  " "+
	    		  "CREATE TABLE Intervention " +
                  "(IdCode INTEGER PRIMARY KEY    AUTOINCREMENT," +
                  " time          CHAR(10), " +               
                  " date          DATE, " +
                  " fk_addressId	  INT	NOT NULL,"+
                  "FOREIGN KEY (fk_addressId) REFERENCES Address(addressId) );"+
                  " "+
                  "CREATE TABLE Client " +
                  "(clientId INTEGER PRIMARY KEY   AUTOINCREMENT," +
                  " firstName      CHAR(50)    NOT NULL, " + 
                  " lastName       CHAR(50)    NOT NULL, " + 
                  " contactNum     CHAR(10), "+ 
                  " payType        CHAR(50)    NOT NULL,"+
                  " fk_interventionId INT      NOT NULL,"+
                  " FOREIGN KEY (fk_interventionId) REFERENCES Intervention(IdCode));"+
                  " "+
                  "CREATE TABLE Animal " +
                  "(animalId INTEGER PRIMARY KEY     AUTOINCREMENT," +
                  " type       CHAR(50)    NOT NULL,"+
                  " breed      CHAR(50)    NOT NULL, " + 
                  " gender     CHAR(10)    NOT NULL," + 
                  " age 	   INT 		   NOT NULL );"+
                  " "+
                  "CREATE TABLE ClientVisit " +
                  "(clientVisitId INTEGER PRIMARY KEY     AUTOINCREMENT," +
                  " visitDate      DATE    NOT NULL, " + 
                  " reason         TEXT, " + 
                  " fk_interventionId INT     NOT NULL, " + 
                  " fk_animalId       INT     NOT NULL,"+
                  " FOREIGN KEY (fk_interventionId) REFERENCES Intervention(IdCode), "+
                  " FOREIGN KEY (fk_animalId) REFERENCES Animal(animalId) );"+
                  " "+
                  "CREATE TABLE RemovalRequest " +
                  "(removalRequestId INTEGER PRIMARY KEY     AUTOINCREMENT," +
                  " outcome        CHAR(50)     NOT NULL, " + 
                  " fk_interventionId INT     NOT NULL, " + 
                  " FOREIGN KEY (fk_interventionId) REFERENCES Intervention(IdCode)"+
                  ");";
	      
	      getDatabase().createTable(sql);
	}
	
	/**
	 * 
	 * @param address
	 * @return int
	 */
	private int writeAddress(Address address)
	{
		String sql = "INSERT INTO Address (lotNum, street, parish) "+
				  "VALUES("+
				  coats(address.getLotNumber())+","+
				  coats(address.getStreet())+","+					  
				  coats(address.getParish())+
				  " );";
	
		getDatabase().insert(sql);
	
		return getDatabase().getLastInsert("Address");
	}
	
	/**
	 * 
	 * @param address
	 * @param addressId
	 */
	private void updateAddress(Address address)
	{
		String sql = "UPDATE Address SET lotNum= "+coats(address.getLotNumber())+","+
						" street = "+coats(address.getStreet())+","+
						" parish = "+coats(address.getParish())+
						" WHERE addressId = "+address.getAddressId()+
						";";
		
		getDatabase().update(sql);
	}
	
	/**
	 * @param addressId
	 */
	private void deleteAddress(int addressId)
	{
		String sql = "DELETE FROM Address WHERE addressId = "+addressId+";";
		getDatabase().delete(sql);
	}
	
	
	/**
	 * 
	 * @param addressId
	 * @return
	 * @throws SQLException 
	 */
	private Address readAddress(ResultSet result) throws SQLException
	{
		Address address = new Address();
		
		address.setLotNumber(result.getString("lotNum"));
		address.setParish(result.getString("parish"));
		address.setStreet(result.getString("street"));
		address.setAddressId(result.getInt("addressId"));
		
		return address;
	}
	
	/**
	 * 
	 * @param animal
	 * @return
	 */
	private int writeAnimal(Animal animal)
	{
		String sql = "INSERT INTO Animal (type, breed, gender, age) "+
				  "VALUES("+
				  coats(animal.getType())+","+
				  coats(animal.getBreed())+","+					  
				  coats(animal.getGender())+","+
				  animal.getAge()+
				  " );";
	
		getDatabase().insert(sql);
		
		return getDatabase().getLastInsert("Animal");	
	}
	
	/**
	 * 
	 * @param animal
	 * @param animalId
	 */
	private void updateAnimal(Animal animal)
	{
		String sql = "UPDATE Animal SET type= "+coats(animal.getType())+","+
						" breed = "+coats(animal.getBreed())+","+
						" gender = "+coats(animal.getGender())+","+
						" age = "+animal.getAge()+
						" WHERE animalId = "+animal.getAnimalId()+
						";";
		
		System.out.println(animal.getAnimalId());
		
		getDatabase().update(sql);
	}
	
	/**
	 * 
	 * @param animalId
	 */
	private void deleteAnimal(int animalId)
	{
		String sql = "DELETE FROM Animal WHERE animalId = "+animalId+";";
		getDatabase().delete(sql);
	}
	
	/**
	 * 
	 * @param animalId
	 * @return
	 * @throws SQLException 
	 */
	private Animal readAnimal(ResultSet result) throws SQLException
	{
		Animal animal = new Animal();
		
		animal.setBreed(result.getString("breed"));
		animal.setGender(result.getString("gender"));
		animal.setType(result.getString("type"));
		animal.setAge(result.getInt("age"));
		animal.setAnimalId(result.getInt("animalId"));
		
		return animal;
	}
	
	
	/**
	 * 
	 * @param intervention
	 */
	private int writeIntervention(Intervention intervention)
	{
		int addressId = writeAddress(intervention.getAddress());
		
		String sql = "INSERT INTO Intervention (time, date, fk_addressId) "+
					  "VALUES("+
					    coats(intervention.getTime())+","+
					  	coats(intervention.getDate().toString())+","+
					  	addressId +
					  " );";
		
		getDatabase().insert(sql);
		
		return getDatabase().getLastInsert("Intervention");
	}
	
	/**
	 * 
	 * @param intervention
	 */
	private void deleteIntervention(int interventionId)
	{
		String sql = "DELETE FROM Intervention WHERE IdCode = "+interventionId+";";
		getDatabase().delete(sql);
	}
	
	/**
	 * 
	 * @param client
	 * @param interventionId
	 */
	private void writeClient(Client client, int interventionId)
	{
		String sql = "INSERT INTO Client (firstName, lastName, contactNum, payType, fk_interventionId) "+
					  "VALUES("+
					  coats(client.getFname()) +","+
					  coats(client.getLname()) +","+
					  coats(client.getContactNum()) +","+
					  coats(client.getPayType()) + ","+
					  interventionId +
					  ");";
		
		getDatabase().insert(sql);
	}
	
	/**
	 * 
	 * @param client
	 * @param clientId
	 */
	private void updateClient(Client client)
	{
		String sql = "UPDATE Client SET firstName= "+coats(client.getFname())+","+
						" lastName = "+coats(client.getLname())+","+
						" contactNum = "+coats(client.getContactNum())+","+
						" payType = "+coats(client.getPayType())+
						" WHERE clientId = "+client.getClientId()+
						";";
		
		getDatabase().update(sql);
	}
	
	/**
	 * 
	 * @param client
	 */
	private void deleteClient(int clientId)
	{
		String sql = "DELETE FROM Client WHERE clientId = "+clientId+";";
		getDatabase().delete(sql);
	}
	
	/**
	 * 
	 * @param clientId
	 * @return
	 * @throws SQLException 
	 */
	private Client readClient(ResultSet result) throws SQLException
	{
		Client client = new Client();
		
		client.setContactNum(result.getString("contactNum"));
		client.setFname(result.getString("firstName"));
		client.setLname(result.getString("lastName"));
		client.setPayType(result.getString("payType"));
		client.setClientId(result.getInt("clientId"));
		
		Log.LogInfo("Restrieving Client From ResultSet, In readClient");

		return client;
	}
	
	/**
	 * 
	 * @param clientVisit
	 */
	public void writeClientVisit(ClientVisit clientVisit)
	{
		int interventionId = writeIntervention(clientVisit);
		writeClient(clientVisit.getClient(), interventionId);
		
		int animalId = writeAnimal(clientVisit.getAnimal());
		
		String sql = "INSERT INTO ClientVisit (visitDate, reason, fk_interventionId, fk_animalId) "+
				  "VALUES("+
				  coats(clientVisit.getVisitDate().toString()) +","+
				  coats(clientVisit.getReason()) +","+
				  interventionId +","+
				  animalId +
				  ");";
	
		getDatabase().insert(sql);	
		
	}
	
	/**
	 * 
	 * @param clientVisit
	 */
	public void updateClientVisit(ClientVisit clientVisit)
	{
		ClientVisit prev = (ClientVisit)readClientVisit(clientVisit.getIdCode(), 1, 0)[0];
		//set Id for previous data set which may have being destroyed
		clientVisit.getAnimal().setAnimalId(prev.getAnimal().getAnimalId());
		clientVisit.getAddress().setAddressId(prev.getAddress().getAddressId());
		clientVisit.getClient().setClientId(prev.getClient().getClientId());
		
		updateAnimal(clientVisit.getAnimal());
		updateAddress(clientVisit.getAddress());
		updateClient(clientVisit.getClient());
		
		String sql = "UPDATE ClientVisit SET visitDate = "+coats(clientVisit.getVisitDate().toString())+","+
				" reason = "+coats(clientVisit.getReason())+
				" WHERE clientVisitId = "+prev.getClientVisitId()+
				";";

		getDatabase().update(sql);
		
		setUpdateQuery(true);
	}
	
	/**
	 * 
	 * @param clientVisit
	 */
	public void deleteClientVisit(String clientVisitId)
	{
		ClientVisit prev = (ClientVisit)readClientVisit(clientVisitId, 1, 0)[0];
		
		String sql = "DELETE FROM ClientVisit WHERE clientVisitId = "+prev.getClientVisitId()+";";
		getDatabase().delete(sql);
		
		deleteAnimal(prev.getAnimal().getAnimalId());
		deleteIntervention(Integer.parseInt(clientVisitId));
		deleteAddress(prev.getAddress().getAddressId());
		deleteClient(prev.getClient().getClientId());
	}
	
	/**
	 * 
	 * @param clientVisitId
	 * @param limit
	 * @param offset
	 * @return
	 */
	public Object[] readClientVisit(String clientVisitId, int limit, int offset)
	{
		ArrayList<ClientVisit> clientVisits = new ArrayList<ClientVisit>();
		
		String sql = "SELECT * FROM Intervention, Animal , Address, Client  JOIN  ClientVisit  "+
					"WHERE Client.fk_interventionId = ClientVisit.fk_interventionId  "+
					"AND ClientVisit.fk_animalId = Animal.animalId "+ 
					"AND Intervention.idCode = ClientVisit.fk_interventionId "+
					"AND Intervention.fk_addressId = Address.addressId ";
		
		//check if clientVisitId is present
		if(clientVisitId != null)
			sql+= "AND ClientVisit.fk_interventionId = "+clientVisitId+";";
		else
			sql+="LIMIT "+limit+" OFFSET "+offset+";";
		
		
		getDatabase().select(sql);
		
		ResultSet result = getDatabase().getResult();
		
		try 
		{			
			int i = 0;
							
			while(result.next())
			{
				ClientVisit clientVisit = new ClientVisit();
				
				addToClientVisit(clientVisit,result);
				
				clientVisits.add(clientVisit);
				
				i++;
			}
			
			Log.LogInfo("ClientVisit Result: "+ i);
		
			//close results
			result.close();
		
		} 
		catch (SQLException e) {
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );
		}
	
		//close statement
		getDatabase().closeStatement();
		
		setUpdateQuery(false);
		
		return clientVisits.toArray();
	}
	
	/**
	 * {@code} returns an array of objects of client visit once the database has being queried using the report form
	 * @param report
	 * @return Object
	 * @see searchRemovalRequest
	 */
	public Object[] searchClientVisit(Object [] report)
	{
		ArrayList<ClientVisit> clientVisits = new ArrayList<ClientVisit>();
		
		Address address = (Address)report[0];
		Animal animal = (Animal)report[1];
		Date visitDate = (Date)report[2];
		
		String sql = "SELECT * FROM Intervention, Animal , Address, Client  JOIN  ClientVisit  "+
				"WHERE Client.fk_interventionId = ClientVisit.fk_interventionId  "+
				"AND ClientVisit.fk_animalId = Animal.animalId "+ 
				"AND Intervention.idCode = ClientVisit.fk_interventionId "+
				"AND Intervention.fk_addressId = Address.addressId ";
		
		//build query
		if(address.getLotNumber().isEmpty() == false)
			sql += "AND Address.lotNum = "+coats(address.getLotNumber());
		if(address.getParish().isEmpty() == false)
			sql += "AND Address.parish = "+coats(address.getParish());
		if(address.getStreet().isEmpty() == false)
			sql += "AND Address.street = "+coats(address.getStreet());
		if(animal.getBreed().isEmpty() == false)
			sql += "AND Animal.breed = "+coats(animal.getBreed());
		if(animal.getType().isEmpty() == false)
			sql += "AND Animal.type = "+coats(animal.getType());
		if(animal.getAge() != 0)
			sql += "AND Animal.age = "+animal.getType();
		if(visitDate.toString().equalsIgnoreCase("0/0/0") == false)
			sql += "AND ClientVisit.dateVisit = "+coats(visitDate.toString());
		sql += ";";
		

		getDatabase().select(sql);
		
		ResultSet result = getDatabase().getResult();
		
		try 
		{			
			int i = 0;
							
			while(result.next())
			{
				ClientVisit clientVisit = new ClientVisit();
				
				addToClientVisit(clientVisit,result);
				
				clientVisits.add(clientVisit);
								
				i++;
			}
			
			Log.LogInfo("ClientVisit Result: "+ i);
		
			//close results
			result.close();
		
		} 
		catch (SQLException e) {
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );
		}		
		
		return clientVisits.toArray();
	}
	
	
	/**
	 * 
	 * @param clientVisit
	 * @param result
	 * @throws SQLException
	 */
	public void addToClientVisit(ClientVisit clientVisit, ResultSet result) throws SQLException
	{
		clientVisit.setIdCode(result.getInt("idCode")+"");
		clientVisit.setClientVisitId(result.getInt("clientVisitId"));
		clientVisit.setAddress(readAddress(result));
		clientVisit.setAnimal(readAnimal(result));
		clientVisit.setClient(readClient(result));
		clientVisit.setTime(result.getString("time"));
		clientVisit.setDate(Date.fromString(result.getString("date")));
		clientVisit.setVisitDate(Date.fromString(result.getString("visitDate")));
		clientVisit.setReason(result.getString("reason"));		
	}
	
	/**
	 * 
	 * @param removalRequest
	 */
	public void writeRemovalRequest(RemovalRequest removalRequest)
	{
		int interventionId = writeIntervention(removalRequest);
		writeClient(removalRequest.getClient(), interventionId);
		
		String sql = "INSERT INTO RemovalRequest (outcome, fk_interventionId) "+
				  "VALUES("+
				  coats(removalRequest.getOutcome()) +","+
				  interventionId +
				  ");";
	
		getDatabase().insert(sql);		
	}
	
	/**
	 * 
	 * @param removalRequest
	 */
	public void updateRemovalRequest(RemovalRequest removalRequest)
	{
		RemovalRequest prev = (RemovalRequest)readRemovalRequest(removalRequest.getIdCode(), 1, 0)[0];
		//set Id for previous data set which may have being destroyed
		removalRequest.getAddress().setAddressId(prev.getAddress().getAddressId());
		removalRequest.getClient().setClientId(prev.getClient().getClientId());

		System.out.println(removalRequest);
		
		updateAddress(removalRequest.getAddress());
		updateClient(removalRequest.getClient());
		
		String sql = "UPDATE RemovalRequest SET outcome = "+coats(removalRequest.getOutcome())+
				" WHERE removalRequestId = "+prev.getRemovalRequestId()+
				";";

		getDatabase().update(sql);
		
		setUpdateQuery(true);
	}
	
	
	public void deleteRemovalRequest(String removalRequestId)
	{
		RemovalRequest prev = (RemovalRequest)readRemovalRequest(removalRequestId, 1, 0)[0];
		
		String sql = "DELETE FROM RemovalRequest WHERE removalRequestId = "+prev.getRemovalRequestId()+";";
		getDatabase().delete(sql);
		
		deleteIntervention(Integer.parseInt(removalRequestId));
		deleteAddress(prev.getAddress().getAddressId());
		deleteClient(prev.getClient().getClientId());
	}
	
	/**
	 * 
	 * @param removalRequestId
	 * @param limit
	 * @param offset
	 * @return
	 */
	public Object[] readRemovalRequest(String removalRequestId, int limit, int offset)
	{
		ArrayList<RemovalRequest> removalRequests = new ArrayList<RemovalRequest>();
		
		String sql = "SELECT * FROM Intervention, Address, Client  JOIN  RemovalRequest  "+
				"WHERE Client.fk_interventionId = RemovalRequest.fk_interventionId  "+
				"AND Intervention.idCode = RemovalRequest.fk_interventionId "+
				"AND Intervention.fk_addressId = Address.addressId ";
	
		//check if clientVisitId is present
		if(removalRequestId != null)
			sql+= "AND RemovalRequest.fk_interventionId = "+removalRequestId+";";
		else
			sql+="LIMIT "+limit+" OFFSET "+offset+";";
		
		
		getDatabase().select(sql);
		
		ResultSet result = getDatabase().getResult();
		
		
		try 
		{			
			int i = 0;
							
			while(result.next())
			{
				RemovalRequest removalRequest = new RemovalRequest();
				
				addToRemovalRequest(removalRequest,result);
				
				removalRequests.add(removalRequest);
				
				i++;
			}
			
			Log.LogInfo("RemovalRequest Result: "+ i);
		
			//close results
			result.close();
		
		} 
		catch (SQLException e) {
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );
		}
	
		//close statement
		getDatabase().closeStatement();
		
		setUpdateQuery(false);
		
		return removalRequests.toArray();
	}
	
	
	/**
	 * {@code} returns an array of objects of removal request once the database has being queried using the report form
	 * @param report
	 * @return Object
	 * @see searchClientVisit
	 */
	public Object[] searchRemovalRequest(Object [] report)
	{
		ArrayList<RemovalRequest> removalRequests = new ArrayList<RemovalRequest>();
		
		Address address = (Address)report[0];
		String outcome = (String)report[1];
		Date date = (Date)report[2];
		
		String sql = "SELECT * FROM Intervention, Address, Client  JOIN  RemovalRequest  "+
				"WHERE Client.fk_interventionId = RemovalRequest.fk_interventionId  "+
				"AND Intervention.idCode = RemovalRequest.fk_interventionId "+
				"AND Intervention.fk_addressId = Address.addressId ";
		
		//build query
		if(address.getLotNumber().isEmpty() == false)
			sql += "AND Address.lotNum = "+coats(address.getLotNumber());
		if(address.getParish().isEmpty() == false)
			sql += "AND Address.parish = "+coats(address.getParish());
		if(address.getStreet().isEmpty() == false)
			sql += "AND Address.street = "+coats(address.getStreet());
		if(outcome.isEmpty() == false)
			sql += "AND RemovalRequest.outcome = "+coats(outcome);
		if(date.toString().equalsIgnoreCase("0/0/0") == false)
			sql += "AND Intervention.date = "+coats(date.toString());
		sql += ";";
		

		getDatabase().select(sql);
		
		ResultSet result = getDatabase().getResult();
		
		try 
		{			
			int i = 0;
							
			while(result.next())
			{
				RemovalRequest removalRequest = new RemovalRequest();

				addToRemovalRequest(removalRequest,result);
				
				removalRequests.add(removalRequest);
				
				i++;
			}
			
			Log.LogInfo("RemovalRequest Result: "+ i);
		
			//close results
			result.close();
		
		} 
		catch (SQLException e) {
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );
		}		
		
		return removalRequests.toArray();
	}
	
	
	/**
	 * @param removalRequest
	 * @param result
	 * @throws SQLException
	 */
	private void addToRemovalRequest(RemovalRequest removalRequest, ResultSet result) throws SQLException
	{
		removalRequest.setIdCode(result.getInt("idCode")+"");
		removalRequest.setRemovalRequestId(result.getInt("removalRequestId"));
		removalRequest.setAddress(readAddress(result));
		removalRequest.setClient(readClient(result));
		removalRequest.setTime(result.getString("time"));
		removalRequest.setDate(Date.fromString(result.getString("date")));
		removalRequest.setOutcome(result.getString("outcome"));	
	}
	
	/**
	 * 
	 * @param str
	 * @return
	 */
	private String coats(String str)
	{
		return "'"+str+"'";
	}

}

package gui.forms;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class ClientVisitReportForm extends ReportForm {

	private DateForm visitDateForm;
	private AnimalForm animalForm;
	
	public ClientVisitReportForm(String[] names, Object[][] obj) {
		super(names, obj);
	
		setAnimalForm(new AnimalForm());
		setVisitDateForm(new DateForm());
	}

	/**
	 * @return the visitDateForm
	 */
	public DateForm getVisitDateForm() {
		return visitDateForm;
	}

	/**
	 * @param visitDateForm the visitDateForm to set
	 */
	public void setVisitDateForm(DateForm visitDateForm) {
		this.visitDateForm = visitDateForm;
	}

	/**
	 * @return the animalForm
	 */
	public AnimalForm getAnimalForm() {
		return animalForm;
	}

	/**
	 * @param animalForm the animalForm to set
	 */
	public void setAnimalForm(AnimalForm animalForm) {
		this.animalForm = animalForm;
	}

	@Override
	public void addToPanel()
	{
		getAddressForm().isSlection = true;
		super.addToPanel();
		JPanel panel2 = new JPanel();
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.LINE_AXIS));
		//panel2.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel2.add(Box.createRigidArea(new Dimension(30, 0)));
		panel2.add(getAddressForm().render());
		panel2.add(getAnimalForm().render());
		
		getVisitDateForm().setTitle("Visit Date");
		panel2.add(getVisitDateForm().render());

		getPanel().add( space(panel2, 10), BorderLayout.SOUTH);
		
		getFormPanel().add(getPanel());
	}
}

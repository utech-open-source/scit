package toc;

public class Action<T> {
	private T read;
	private T write;
	private String tapeMove;
	private String moveToSate;
	
	public Action(T readObject, T writeObject, String tapeMoveTo, String moveToSateName){
		read = readObject;
		write = writeObject;
		setTapeMove(tapeMoveTo);
		moveToSate = moveToSateName;
	}
	
	public String getMoveToSateName() {
		return moveToSate;
	}

	public void setMoveToSateName(String moveToSate) {
		this.moveToSate = moveToSate;
	}

	public T getWrite() {
		return write;
	}

	public void setWrite(T write) {
		this.write = write;
	}

	public T getRead() {
		return read;
	}

	public void setRead(T read) {
		this.read = read;
	}

	public String getTapeMove() {
		return tapeMove;
	}

	public void setTapeMove(String tapeMove) {
		this.tapeMove = tapeMove;
	}
}

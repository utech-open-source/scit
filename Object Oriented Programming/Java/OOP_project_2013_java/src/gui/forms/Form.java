package gui.forms;

import gui.Resource;
import gui.components.TableModel;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

public abstract class Form {

	private JButton button;
	private JPanel formPanel;
	private String secTitle;
	
	public Form()
	{
		setFormPanel(new JPanel(new GridLayout(0,1)));
		setButton(Button("Save"));
		setSecTitle("");
	}

	/**
	 * @return the formPanel
	 */
	public JPanel getFormPanel() {
		return formPanel;
	}

	/**
	 * @param formPanel the formPanel to set
	 */
	public void setFormPanel(JPanel formPanel) {
		this.formPanel = formPanel;
	}
	
	/**
	 * @return the button
	 */
	public JButton getButton() {
		return button;
	}

	/**
	 * @return the secTitle
	 */
	public String getSecTitle() {
		return secTitle;
	}

	/**
	 * @param secTitle the secTitle to set
	 */
	public void setSecTitle(String secTitle) {
		this.secTitle = secTitle;
	}

	/**
	 * @param button the button to set
	 */
	public void setButton(JButton button) {
		this.button = button;
	}
	
	public abstract void addToPanel();
	
	/**
	 * 
	 * @return JPanel
	 */
	public JPanel render()
	{
		addToPanel();
		return getFormPanel();
	}
	

	public JTable Table(TableModel dm){
		JTable tmp = new JTable(dm);
		
        tmp.setPreferredScrollableViewportSize(new Dimension(400, 70));
        tmp.setFillsViewportHeight(true);
        
		
		return tmp;
	}
	
	
	public JButton Button(String name)
	{
		JButton tmp = new JButton(name);
		
		return tmp;
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public JLabel Label(String name)
	{
		JLabel tmp = new JLabel(name);
		
		tmp.setMinimumSize(new Dimension(Resource.labelWidth, Resource.labelheight));
		tmp.setMaximumSize(new Dimension(Resource.labelWidth, Resource.labelheight));
		
		return tmp;
	}
	
	/**
	 * 
	 * @param name
	 * @param width
	 * @param height
	 * @return JLabel
	 */
	public JLabel Label(String name, int width, int height)
	{
		JLabel tmp = new JLabel(name);
		
		tmp.setMinimumSize(new Dimension(width, height));
		tmp.setMaximumSize(new Dimension(height, height));
		
		return tmp;
	}
	
	/**
	 * 
	 * @param size
	 * @return
	 */
	public JTextField TextField(int size)
	{
		JTextField tmp = new JTextField(size);
		
		//style text area
		tmp.setBorder(BorderFactory.createBevelBorder(1));
		tmp.setBackground(Resource.textAreaColor);
		
		tmp.setMaximumSize(new Dimension(200, 20));
		
		//center textfield by default
		if(Resource.centerTextField)
			return center(tmp);
		
		return tmp;
	}
	
	public JTextField TextField(int size, int width, int height)
	{
		JTextField tmp = TextField(size);
		tmp.setMaximumSize(new Dimension(width, height));
		return tmp;
	}
	
	
	/**
	 * 
	 * @param size
	 * @param center
	 * @return
	 */
	
	public JTextField TextField(int size, boolean center)
	{
		JTextField tmp = TextField(size);
		
		if(center == true)
			return center(tmp);
		
		return tmp;
	}
	
	/**
	 * 
	 * @param text
	 * @return
	 */
	public JTextArea TextArea(String text)
	{
		return new JTextArea(text);
	}
	
	/**
	 * 
	 * @return
	 */
	public JTextArea TextArea(int rows, int columns)
	{
		JTextArea tmp = new JTextArea(rows, columns);

		//style text area
		
		//tmp.setBorder(BorderFactory.createBevelBorder(1));
		tmp.setLineWrap(Resource.lineWrap);
		tmp.setWrapStyleWord(Resource.wrapStyleWord);
		tmp.setMargin(new Insets(Resource.textAreaMargin, Resource.textAreaMargin, 
				Resource.textAreaMargin, Resource.textAreaMargin));
		
		tmp.setBackground(Resource.textAreaColor);
		//tmp.setMaximumSize(new Dimension(200, 90));
		
		return tmp;
	}
	
	/**
	 * 
	 * @param textArea
	 * @return JScrollPane
	 */
	public JScrollPane ScrollPane(JTextArea textArea)
	{
		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
		
		scroll.setMaximumSize(new Dimension(200, 90));
		
		return scroll;
	}
	
	/**
	 * 
	 * @param options
	 * @return
	 */
	public JComboBox<String> ComboBox(String [] options)
	{
		JComboBox<String> comboBox = new JComboBox<String>(options);
		comboBox.setSelectedIndex(0);
		comboBox.setMaximumSize(new Dimension(200, 90));
		
		return comboBox;
	}
	
	/**
	 * Center Text In TextFields
	 * @param textField
	 * @return
	 */
	public JTextField center(JTextField textField)
	{
		textField.setHorizontalAlignment(JTextField.CENTER);
		
		return textField;
	}
	
	/**
	 * 
	 * @param button
	 * @return
	 */
	public JPanel wrap(JButton button)
	{
		JPanel tmp = new JPanel();
		
		tmp.setLayout(new GridLayout(1,2));
		
		tmp.add(button);
		
		return tmp;
	}
	
	/**
	 * 
	 * @param label
	 * @param comboBox
	 * @return
	 */
	public JPanel wrap(JLabel label, JComboBox<String> comboBox)
	{
		JPanel tmp = new JPanel();
		
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.X_AXIS));
		
		tmp.add(label);
		tmp.add(comboBox);
				
		return finishWrap(tmp);		
	}
	
	/**
	 * 
	 * @param label
	 * @param textField
	 * @return
	 */
	public JPanel wrap(JLabel label, JTextField textField)
	{
		
		JPanel tmp = new JPanel();
		
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.X_AXIS));
		
		tmp.add(label);
		tmp.add(textField);
				
		return finishWrap(tmp);
	}
	
	public JPanel wrapY(JLabel label, JTextField textField)
	{
		
		JPanel tmp = new JPanel();
		
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.Y_AXIS));
		
		tmp.add(label);
		tmp.add(textField);
				
		return finishWrap(tmp);
	}
	
	
	/**
	 * 
	 * @param label
	 * @param textArea
	 * @return
	 */
	public JPanel wrap(JLabel label, JTextArea textArea)
	{
		
		JPanel tmp = new JPanel();
		
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.X_AXIS));
		
		tmp.add(label);
		tmp.add(textArea);
			
		return finishWrap(tmp);
	}
	
	/**
	 * 
	 * @param label
	 * @param panel
	 * @return
	 */

	public JPanel wrap(JLabel label, JPanel panel)
	{	
		JPanel tmp = new JPanel();
		
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.X_AXIS));
		
		tmp.add(label);
		tmp.add(panel);
		
		return finishWrap(tmp);
	}
	
	/**
	 * 
	 * @param label
	 * @param pane
	 * @return
	 */
	public JPanel wrap(JLabel label, JScrollPane pane)
	{	
		JPanel tmp = new JPanel();
		
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.X_AXIS));
		
		tmp.add(label);
		tmp.add(pane);
		
		return finishWrap(tmp);
	}
	
	/**
	 * 
	 * @param label
	 * @param panel
	 * @return
	 */
	public JPanel wrapY(JLabel label, JPanel panel)
	{	
		JPanel tmp = new JPanel();
		
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.Y_AXIS));
		
		tmp.add(label);
		tmp.add(panel);
		
		return finishWrap(tmp);
	}
	
	/**
	 * 
	 * @param panel
	 * @return
	 */
	public JPanel finishWrap(JPanel panel){
		JPanel tmp = new JPanel(new GridLayout(0,1));
		tmp.add(panel);
		
		return tmp;		
	}
	
	/**
	 * 
	 * @param panel
	 * @param amt
	 * @return
	 */
	public JPanel space(JPanel panel, int amt)
	{
		panel.setBorder(BorderFactory.createEmptyBorder(amt, 0, 0, 0));
		
		return panel;
	}
	
	/**
	 * 
	 * @param label
	 * @return
	 */
	public JPanel title(String label)
	{
		JPanel tmp = new JPanel(new GridLayout(1,1));

		tmp.add(Label(label));
		
		return tmp;
	}
	
}

#ifndef NODE_H
#define NODE_H

#include <iostream>

using namespace std;

class Node{
private:
	int data;
	Node * left;
	Node * right;
public:
	Node();
	Node(int);
	int getData() const;
	Node * getLeft() const;
	Node * getRight() const;
	void setData(int);
	void setLeft(Node *);
	void setRight(Node *);
};

#endif
/**
Assessment.c
Author: Mark Robinson
Date: October 26, 2012

Problem

 A company requires a game system that determines the amount of money a person wins directly based on the amount of money that the user bets.

**/

#include <stdio.h>
#include <conio.h>
#define LIMIT 10

//enumerate to get the exit, continue commands

enum EXITGAME{ NO=1, YES, GET};

void main()
{
	char Fname[30],Mname[20],Lname[30],Continue;
	float AmtWon, AmtLost, Winnings, AvgWinnings, Bet, Min, Max;
	float TotalAmtWon, TotalAmtLost, TotalWinnings, TotalBet, DiffFirstLast, WinningsArray[LIMIT];
	int CountryStatus, BetsMade, Exit;

	TotalAmtLost=TotalAmtWon=TotalWinnings=DiffFirstLast=BetsMade=CountryStatus=0;

	Exit = NO;

	printf("\n************************************************************\n");
	printf("\t\t\tTHE NUMBERS GAME");
	printf("\n************************************************************\n");

	//get the user first middle and last name
	printf("\nPlease enter your first, middle and last name: ");
	scanf("%s",&Fname);
	scanf("%s",&Mname);
	scanf("%s",&Lname);

	//set an infinite loop
	while(Exit == NO)
	{
		//get the user bet
		printf("\nPlease enter your Bet: ");
		scanf("%f",&Bet);

		// ensure that the bet entered is <=0
		while(Bet<=0)
		{
			printf("\nPlease enter your Bet: ");
			scanf("%f",&Bet);
		}

		if(CountryStatus == 0)
		{
			do
			{
				printf("\nPlease enter your Country Status (1-3): ");
				scanf("%s",&CountryStatus);

				// get correct input of country status if out or range string range 48-51
				CountryStatus = (int)CountryStatus - 48;

			}
			while(CountryStatus<1 || CountryStatus>3);
		}

		// selects the appropriate AmtWon and AmtLost based on the country status
		switch(CountryStatus)
		{
		case 1 :  AmtWon = ((Bet * Bet) / (Bet * Bet * Bet)) * 30500;
			AmtLost = 0.35 * Bet;
			break;
		case 2 :  AmtWon = (200 / Bet) * 200;
			AmtLost = 0.35*Bet;
			break;
		case 3 :  AmtWon = (100%(int)Bet) * 600;
			AmtLost = 500;
			break;

		}

		//calculate total winings
		Winnings = AmtWon - AmtLost;

		//Ensure max and min are assigned to winnings if only one is entered
		if(Max ==0 && Min==0)
		{
			Max = Min = Winnings;
		}

		if(BetsMade >= 1 && Winnings > WinningsArray[BetsMade-1])
		{
			Max = Winnings;
		}

		if(BetsMade >= 1 && Winnings < WinningsArray[BetsMade-1])
		{
			Min = Winnings;
		}

		//get overall totals

		TotalBet += Bet;
		TotalAmtLost += AmtLost;
		TotalAmtWon += AmtWon;
		TotalWinnings += Winnings;
		WinningsArray[BetsMade] = Winnings;

		//print the results
		printf("\nPlayer: %s %s %s\n",Fname,Mname,Lname);
		printf("Amount Won: JA$ %.2f\n",AmtWon);
		printf("Amount Lost: JA$ %.2f\n",AmtLost);
		printf("Winnings: JA$ %.2f\n",Winnings);

		BetsMade+=1;

		if(BetsMade == LIMIT)
		{
			Exit = YES;
		}
		else
		{

			do
			{
				fflush(stdin);
				printf("\nDo you want to play again, ENTER ( Y for yes ) or ( N for no ):");
				scanf("%c",&Continue);

				switch(Continue)
				{
				case 'N': Exit = YES;
					break;
				case 'n': Exit = YES;
					break;
				case 'Y': Exit = NO;
					break;
				case 'y': Exit = NO;
					break;
				default: Exit = GET;
					break;
				}

			}
			while(Exit == GET);

			//system("cls");
		}

	}

	AvgWinnings = TotalWinnings/BetsMade;
	DiffFirstLast = WinningsArray[0] - WinningsArray[BetsMade-1];

	//print the results
	printf("\n************************************************************\n");
	printf("\n** Highest Total Winnings: JA$%-30.2f **\n\n", Max);
	printf("** Lowest Total Winnings: JA$ %-31.2f **\n\n", Min);
	printf("** Average Total Winnings: JA$ %-26.2f **\n\n", AvgWinnings);
	printf("** Total Winnings: JA$ %-34.2f **\n\n",TotalWinnings);
	printf("** Difference between first and last: JA$ %-19.2f **\n\n", DiffFirstLast);
	printf("** Average Total Winnings: US$ %-26.2f **\n", AvgWinnings/85);
	printf("\n************************************************************\n");

	system("pause");

}
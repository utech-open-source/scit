package toc;

import java.awt.Toolkit;
import java.util.HashMap;

import toc.gui.View;

public class TuringMachine<T> {

	private int position;
	private Object [] tape;
	private int length;
	private static int offset = 5; 
	private HashMap<String, State<T>> states;
	private String acceptState;
	private String startState;
	private State<T> currentState;
	
	public static String Left = "L";
	public static String Right = "R";
	public static String Stay = "S";
	
	public View view;
	
	public TuringMachine(int size, String sState, String fState, View view){
		position = offset;
		length = size+offset;
		tape = new Object[length];
		acceptState = fState;
		startState = sState;
		currentState = null;
		states = new HashMap<String, State<T>>(0);
		initialize();
		this.view = view;
	}
	
	public void initialize(){
		for(int i = 0; i <length; i++ ){
			tape[i] = null;
		}
	}
	
	public void addValueToTape(T value, int pos){
		tape[pos + offset] = value;
	}
	
	public void write(T value){
		tape[position] = value;
	}
	
	@SuppressWarnings("unchecked")
	public T read(){
		return (T)tape[position];
	}
	
	public void moveLeft(){
		if(position > 0) position--;
	}
	
	public void moveRight(){
		if(position < length) position++;
	}
	
	public void addState(String name){
		states.put(name, new State<T>(name));
	}
	
	public void addState(String name, T read, T write, String tapeMoveTo, String gotoState){
		if(states.get(name) == null){
			addState(name);
		}
		addActionToState(name, read, write, tapeMoveTo, gotoState);
	}
	
	public void addState(String name, T read, String tapeMoveTo, String gotoState){
		if(states.get(name) == null){
			addState(name);
		}
		addActionToState(name, read, read, tapeMoveTo, gotoState);
	}

	public void addState(String name, T read, T write, String tapeMoveTo){
		if(states.get(name) == null){
			addState(name);
		}
		addActionToState(name, read, write, tapeMoveTo, name);
	}
	
	public void addState(String name, T read, String tapeMoveTo){
		if(states.get(name) == null){
			addState(name);
		}
		addActionToState(name, read, read, tapeMoveTo, name);
	}
	
	private void addActionToState(String stateName, T read, T write, String tapeMoveTo, String gotoState){
		State <T> state = states.get(stateName);
		if(state != null){
			if( (read == null && !state.actionInMap("null") ) || 
					( read != null &&!state.actionInMap(read.toString()))  ){
				state.addAction(new Action<T>(read, write, tapeMoveTo, gotoState));
			}
		}
	}

	public String getAcceptState() {
		return acceptState;
	}

	public void setAcceptState(String acceptState) {
		this.acceptState = acceptState;
	}

	public String getStartState() {
		return startState;
	}

	public void setStartState(String startState) {
		this.startState = startState;
	}
	
	public void run(int len){
		int i = 0;
		T currentVal = read();
		currentState = states.get(startState);
		
		while( ( len != 0 && i < len ) || !currentState.equals( states.get(acceptState)) ){
			Action<T> action = null;
			if(currentVal != null)
				action = currentState.action(currentVal.toString());
			else
				action = currentState.action("null");
			
			if(action != null){ 
				write(action.getWrite());
				if(action.getTapeMove().equals(Left)){
					moveLeft();
				}
				else if(action.getTapeMove().equals(Right)){
					moveRight();
				}
				currentState = states.get(action.getMoveToSateName());
			}
			currentVal = read();
			printTape();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i++;
		}
		//System.out.println(currentVal);
	}

	public void printTape() {
		//System.out.print(currentState.getName()+ " ");
		int len = tape.length;
		
		for(int i =0; i < len; i++){
			String val = tape[i] + "";
			if(val.equalsIgnoreCase("null")){
				val = "[ ]";
			}
			view.updateTape(i, val);
		}	
		view.setPositonColor(position);
		Toolkit.getDefaultToolkit().beep();
		//for(Object obj: tape){
			//System.out.print(obj+" ");
		//}
		//System.out.println();
	}
	
	public String getResult()
	{
		String val=" ";
		
		for(int x=0;x<tape.length;x++)
		{
			if(tape[x] != null && (tape[x].equals('1') || tape[x].equals('0') ) )
			{
				val+=tape[x];
			}
		}
		return val;
	}
	
}

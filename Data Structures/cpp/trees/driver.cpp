#include "Tree.h"

void main()
{
	Tree * T = new Tree();

	T->insertNode(31);
	T->insertNode(68);
	T->insertNode(7);
	T->insertNode(21);
	T->insertNode(47);
	T->insertNode(25);
	T->insertNode(43);
	T->insertNode(5);
	T->insertNode(54);

	cout<<"Pre Order:"<<endl;
	T->preOrderTraversal(T->getRoot());
	cout<<"In Order:"<<endl;
	T->inOrderTraversal(T->getRoot());
	cout<<"Post Order:"<<endl;
	T->postOrderTraversal(T->getRoot());

	system("pause");
}
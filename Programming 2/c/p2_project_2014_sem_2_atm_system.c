#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

#define true 1
#define false 0
#define charge 30 //fee for transactions

struct Account
{
	int ac_num;
	char pin[4];
	double ac_bal;
};

typedef struct Account AC;

struct Transaction
{
	char time_stamp[30];
	char type[10];
	double amount;
	int other_account;
};

typedef struct Transaction TRANS;

//function prototypes
void system_menu();
void user_menu();
void create_new_files();
void write_random_file(AC);
void read_random_file(AC *);
int check_random_file(AC);
int login();
AC account_login();
void deposit(AC);
void withdraw(AC);
void pay_fee(AC);
void check_bal(AC);
void change_pwrd(AC);
void write_log(AC, TRANS);
void read_all_log(AC*);
void swap(char *, char, char);
int get_size(char *);

//bonnus
void get_hidden_pass(char *, char *);

int main(){
	system_menu();
	return 0;
}
//first main menu
void system_menu(){
	char ch;
	int chk=false;

	while(1){
		system("cls");
		printf("\t\t\t\tMain Menu\n\n\n"
			"\t\t  WELCOME! PLEASE SELECT YOUR OPTION!\n\n\n"
			"\t\tINITIALIZE SYSTEM ---------------------- 1\n\n\n"
			"\t\tDISPLAY ATM TRANSACTIONS LOG ----------- 2\n\n\n"
			"\t\tUSER TRANSACTIONS MENU ----------------- 3\n\n\n"
			"\t\tEXIT ----------------------------------- 4\n\n\n");

		fflush(stdin);
		printf(" Enter option: ");
		scanf("%c",&ch);

		system("cls");
		switch(ch){
		case '1':
			//initialize system
			chk=login();
			if(chk==true)
				create_new_files();
			break;
		case '2':
			//display log
			chk=login();
			if(chk==true)
				read_all_log(NULL);
			break;
		case '3':
			user_menu();
			break;
		case '4':
			exit(0);
		default:
			printf("invalid option.");
		}

		system("pause");
	}
}

//sub menu of option 3
void user_menu(){
	char ch;
	AC a;
	a = account_login();

	if(a.ac_num!=false)
	{
		while(1){
			system("cls");
			printf("\t\t\tUser Transactions Menu\n\n\n"
				"\t\tWELCOME! PLEASE SELECT YOUR OPTION!\n\n\n"
				"\tDeposit Funds ---------------------------------- 1\n\n\n"
				"\tWithdraw Funds --------------------------------- 2\n\n\n"
				"\tPay School Fee --------------------------------- 3\n\n\n"
				"\tCheck Account Balance -------------------------- 4\n\n\n"
				"\tDisplay Account Transactions Log --------------- 5\n\n\n"
				"\tChange Password -------------------------------- 6\n\n\n"
				"\tExit ------------------------------------------- 7\n\n");

			fflush(stdin);
			printf(" Enter option: ");
			scanf("%c",&ch);

			system("cls");
			switch(ch){
			case '1':
				//deposit funds
				deposit(a);
				break;
			case '2':
				//withdraw funds
				withdraw(a);
				break;
			case '3':
				//pay school fee
				pay_fee(a);
				break;
			case '4':
				//check balance
				check_bal(a);
				break;
			case '5':
				//dis trans
				read_all_log(&a);
				break;
			case '6':
				//change password
				change_pwrd(a);
				break;
			case '7':
				//return to main
				return;
				break;
			}

			system("pause");
		}
	}
}

//create and initialize the ramdom file
void create_new_files(){
	FILE * sfp;
	FILE * fp;
	int i;

	fp = fopen("account.dat","wb");

	if((sfp = fopen("logs.txt", "w")) == NULL) //create or open file
	{
		printf(" FILE COULD NOT BE OPENED\n");  //if file can't be opened
		return;
	}
	else //if the condition is false
	{
		fclose(sfp);
	}

	//initialize random file
	if(fp!=NULL)
	{
		for(i=0;i<1000;++i){
			AC a = {i,"0000",1000.0};
			fwrite(&a, sizeof(AC),1,fp);
		}
		fclose(fp);
		printf("\n SYSTEM REST\n");
	}
	else
	{
		printf("\n\n FILE COULD NOT BE OPENED\n\n");
	}
}
//write the acount structure to the random file
void write_random_file(AC b){

	FILE * fp;

	fp=fopen("account.dat","r+b");

	if(fp!=NULL){
		b.ac_num=b.ac_num%1000;

		fseek(fp,sizeof(AC)*((b.ac_num%1000)),SEEK_SET);
		fwrite(&b,sizeof(b),1,fp);
		fclose(fp);
		//	printf("\n\nDATA WAS WRITTEN\n\n");
	}else{
		printf("\n\n ERROR WRITNG DATA\n\n");
	}
}

//read the account structure from the ramdom file
void read_random_file(AC * b){
	FILE * fp;

	fp=fopen("account.dat","r+b");

	if(fp!=NULL){

		fseek(fp,sizeof(AC)*( b->ac_num % 1000),SEEK_SET);
		fread(b,sizeof(AC),1,fp);
		fclose(fp);

		if(b->ac_num!=0){
			b->ac_num+=1000; ///iiiiiiiiiiiiiiiiiiiiiiiiiiiii

		}else{
			printf("\n\n DATA IS EMPTY\n\n");
		}
	}else{
		printf("\n\n ERROR READING DATA\n\n");
	}
}

//check to ensure account is present before going to sub menu
int check_random_file(AC a){

	AC b={0000,"0000",0.0};

	b.ac_num = a.ac_num;

	read_random_file(&b);

	if( !strcmp(b.pin,a.pin) ){
		return true;
	}else{
		return false;
	}
}

//accept login info then call check_random_file
AC account_login(){
	AC a;
	int chk;

	printf("\n ACCOUNT NUMBER: ");
	scanf("%d",&a.ac_num);

	if(a.ac_num>1000 && a.ac_num<2001){

		get_hidden_pass(" PIN: ", a.pin);

		chk=check_random_file(a);
		if(chk==false){
			printf("\n\n INVALID USER ACCOUNT\n\n");
			a.ac_num = false;
		}
	}else{
		printf("\n\n INVALID USER ACCOUNT\n\n");
	}

	return a;
}

//login for option 1 and  2 in main menu
int login()
{
	char * unames[] = {"user", "guest"};
	char * passwords[] = {"pass", "pass"};

	char password[25], uname[25], ch, cont;
	int i=0, e = 0;
	int trial=0;
	while(trial<3)
	{
		printf("\n LOGIN\n\n");
		printf(" Enter the Username: ");
		scanf("%s", uname);

		get_hidden_pass(" Enter the Password: ", password);

		if( (!strcmp(uname,unames[0])&&!strcmp(password,passwords[0]) ) ||
			( !strcmp(uname,unames[1])&&!strcmp(password,passwords[0])) )
		{
			return true;
		}
		else
		{
			printf("\n\n%d of 3",(trial+1));
			printf("\n Enter Valid Username and Password\n\n");
			i=0;
		}
		trial++;
	}
	printf("\n Too many login attempts!  \n");
	return false;
}

void get_hidden_pass(char * msg, char * password)
{
	char ch;
	int i = 0, e = 0;

	printf(msg);

	while(1)
	{
		fflush(stdin);
		ch = getch();
		if( (int)ch==13)
			break;

		if( (int) ch == 8){ //backspace pressed
			i--;
			printf("\r%s",msg);
			for(e = 0; e < i ; e++)
				printf("*");
		}
		else
		{
			printf("*");
			password[i]=ch;
			i++;
		}
	}
	password[i]='\0';
}

//change the password
void change_pwrd(AC a){
	int chk, cnt = 0;

	printf("\n CHANGE PASSWORD\n\n");
	read_random_file(&a);
	get_hidden_pass(" CURRENT PIN: ", a.pin);

	chk=check_random_file(a);

	while(chk==false)
	{
		if(cnt == 3)
		{
			printf("\n TOO MANY ATTEMPS");
			break;
		}
		printf("\n INCORRECT PIN TRY AGAIN! ");
		chk=check_random_file(a);
		cnt++;
	}

	get_hidden_pass("\n NEW PIN: ", a.pin);

	while(get_size(a.pin) != 4)
	{
		printf("\n INCORRECT PIN TRY AGAIN! ");
		get_hidden_pass("\n NEW PIN: ", a.pin);
	}

	write_random_file(a);

	printf("\n PIN UPDATED\n\n");
}


int get_size(char * pin)
{
	int i = 0;
	while(pin[i] != '\0')
	{
		i++;
	}
	return i;
}

//deposit money
void deposit(AC a){
	double dep_amt;

	//AC b={0000,0000,0.0};
	TRANS tr={"time","deposit", 0.0,0};

	//printf(" \nACCOUNT NUMBER: ");
	//scanf("%d", &b.ac_num);

	//if(b.ac_num>1000 && b.ac_num<2001){
	printf("\n DEPOSIT AMT: $");
	scanf("%lf", &dep_amt);

	//ensure that deposite amount is a multiple of ten
	while( ((int)dep_amt%100) != 0)
	{
		system("cls");
		printf("\n DEPOSIT AMOUNT MUST BE MULTIPLE OF $100. PLEASE TRY AGAIN!\n");
		printf(" DEPOSIT AMT: $");
		scanf("%lf", &dep_amt);
	}

	printf("\n DEPOSIT APPROVED\n");

	read_random_file(&a);
	//subtract cost of deposit from current account
	a.ac_bal += (dep_amt - charge);
	write_random_file(a);
	//set transaction logs
	tr.other_account=a.ac_num;  //set id between 1000 and 2000
	tr.amount=dep_amt;
	write_log(a,tr);
	//}
	//check balance of current user account
	check_bal(a);
}

//withdraw money
void withdraw(AC a){
	double withd_amt;

	TRANS tr={"time","withdraw", 0.0,0};

	printf("\n WITHDRAW AMT: $");
	scanf("%lf", &withd_amt);

	read_random_file(&a);
	//check if account can withdraw money
	if(a.ac_bal > (withd_amt+charge) && a.ac_bal > 1000){
		a.ac_bal -= (withd_amt+charge);
		write_random_file(a);
		tr.other_account=a.ac_num;
		tr.amount=withd_amt;
		write_log(a,tr);
		printf("\n WITHDRAW APPROVED\n");
	}else{
		printf("\n\n NOT ENOUGH FUNDS TO COMPLETE REQUEST\n\n");
	}
	check_bal(a);
}

//pay school fee
void pay_fee(AC a){

	TRANS tr={"time","School_Fee", 0.0,0};

	int school_ac;
	int stud_id;
	double fee;

	printf("\n\n STUDENT ID#: ");
	scanf("%d", &stud_id);

	printf(" SCHOOL A/C NUMBER: ");
	scanf("%d", &school_ac);

	printf(" AMOUNT TO BE TRANSFERED: $");
	scanf("%lf", &fee);

	read_random_file(&a);

	a.ac_bal -=(fee+charge);
	if(a.ac_bal>1000){
		write_random_file(a);
		printf("\n TRANSFER APPROVED\n");

		tr.other_account=school_ac;  //set id between 1000 and 2000
		tr.amount=fee;
		write_log(a,tr);
	}else{
		printf("\n\n NOT ENOUGH FUNDS TO COMPLETE REQUEST\n\n");
	}
	check_bal(a);
}

//check account balance
void check_bal(AC a){

	read_random_file(&a);
	printf("\n ==========================\n"
		" YOUR ACCOUNT BALANCE: $%.2f\n"
		" ==========================\n\n",a.ac_bal);
}

//modify time_date array to write to log
void swap(char * arr, char find, char replace)
{
	char * tmp = arr;

	if(tmp == NULL)
		return;

	while(*tmp != '\0' )
	{
		if(*tmp == find)
		{
			*tmp = replace;
		}

		tmp++;
	}
}

//write to log
void write_log(AC a, TRANS tr){
	FILE * sfp;

	time_t T= time(0);
	char*dt= ctime(&T);
	swap(dt, ' ', '_');
	strcpy(tr.time_stamp,dt);

	if((sfp = fopen("logs.txt", "a+")) == NULL) //create or open file
	{
		printf("File could not be opened\n");  //if file can't be opened
		return;
	}
	else //if the condition is false
	{
		fprintf(sfp,"%d %s %d %lf %s", a.ac_num, tr.type,tr.other_account,tr.amount,tr.time_stamp);
		fclose(sfp);
	}
}

void read_all_log(AC * a){
	AC b;
	TRANS tr;
	FILE * sfp;

	if((sfp = fopen("logs.txt", "r+")) == NULL) //create or open file
	{
		printf("File could not be opened\n");  //if file can't be opened
		return;
	}
	else //if the condition is false
	{
		system("cls");
		rewind(sfp);
		printf("\n\nLOGS\n----\n\n");
		printf("A/C\t  TYPE\t\tA/C\t AMT\t TIME\n");
		printf("------------------------------------------------------------------------\n");
		while(1)
		{
			fscanf(sfp,"%d %s %d %lf %s", &b.ac_num, tr.type,&tr.other_account,&tr.amount,tr.time_stamp);
			//unswap chars first placed into time stamp
			swap(tr.time_stamp, '_', ' ');
			if(feof(sfp))
				break;
			if(a == NULL ||
				(a->ac_num == b.ac_num) )
				printf("%d\t %s\t %d\t %2.lf\t %s\n", b.ac_num, tr.type,tr.other_account,tr.amount,tr.time_stamp);
		}
		fclose(sfp);
	}
}

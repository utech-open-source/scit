package gui;

import gui.components.Location;
import gui.components.Utilities;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class Frame extends JFrame implements Utilities {
	
	//add room for one child frame
	private Frame childFrame = null;
	
	private String frameName;
	
	public Frame()
	{
		super("Form");
		
		setFrameName("Frame");
	}
	
	public Frame(String frameName)
	{
		super(frameName);
		setFrameName(frameName);
		
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
            	super.windowClosed(e);
            	if(getChildFrame() != null)
            	{
					WindowEvent closingEvent = new WindowEvent(getChildFrame(), WindowEvent.WINDOW_CLOSING);
					Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(closingEvent);
            	}

            }

        });
	}

	public String getFrameName() {
		return frameName;
	}

	public void setFrameName(String frameName) {
		this.frameName = frameName;
	}
	
	/**
	 * @return the childFrame
	 */
	public Frame getChildFrame() {
		return childFrame;
	}

	/**
	 * @param childFrame the childFrame to set
	 */
	public void setChildFrame(Frame childFrame) {
		this.childFrame = childFrame;
	}

	public void display()
	{
      setVisible(true);
	}
	
	public void setSizeAndLocation(Location loc)
	{
		float x = 0, y = 0;
	    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();		
		
		float widthP = loc.widthP/100;
		float heightP = loc.heightP/100;
		
		if(loc.center == true) 
		{
			if(widthP < 1)
				x = (1 - widthP)*screenSize.width/2;
	
			if(heightP < 1)
				y = (1 - heightP)*screenSize.height/2;
		}
		
		widthP = widthP*screenSize.width;
		heightP = heightP*screenSize.height;
	      
	    setBounds((int)x, (int)y, (int)widthP, (int)heightP);
	}
	
	public void hideActiveDialog()
	{
		Window active = getSelectedWindow(getWindows());
		
		active.setVisible(false);
	}
	
	
	public Window getActiveWindow()
	{
		return getSelectedWindow(getOwnedWindows());
	}
	
	public Window getSelectedWindow(Window[] windows) {
	    Window result = null;
	    for (int i = 0; i < windows.length; i++) {
	        Window window = windows[i];
	        if (window.isActive()) {
	            result = window;
	        } else {
	            Window[] ownedWindows = window.getOwnedWindows();
	            if (ownedWindows != null) {
	                result = getSelectedWindow(ownedWindows);
	            }
	        }
	    }
	    return result;
	}
	
}

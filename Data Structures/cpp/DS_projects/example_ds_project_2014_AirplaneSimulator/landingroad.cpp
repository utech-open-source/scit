#include "landingroad.h"

//default constructor
LandingRoad::LandingRoad(string name, int maxSize, int currSize, string priorityType)
{
	setName(name);
	setMaxSize(maxSize);
	setCurrSize(currSize);
	setPriorityType(priorityType);
	setFront(NULL);
	setBack(NULL);

	SYSTEM->display(IDC_TEXT_DISPLAY_PLANE, " 'Road-"+ string(this->name)+"' Is Ready For AirPlanes.", 1);
}


//setters
void LandingRoad::setName(string name)
{
	this->name = name;
}

void LandingRoad::setMaxSize(int maxSize)
{
	this->maxSize = maxSize;
}

void LandingRoad::setCurrSize(int currSize)
{
	this->currSize = currSize;
}

void LandingRoad::setPriorityType(string priorityType)
{
	this->priorityType = priorityType;
}

void LandingRoad::setFront(AirPlaneNode * front)
{
	this->front = front;
}

void LandingRoad::setBack(AirPlaneNode * back)
{
	this->back = back;
}

//getters
string LandingRoad::getName() const
{
	return name;
}

int LandingRoad::getMaxSize()const
{
	return maxSize;
}

int LandingRoad::getCurrSize()const
{
	return currSize;
}

string LandingRoad::getPriorityType()const
{
	return priorityType;
}

AirPlaneNode * LandingRoad::getFront()const
{
	return front;
}

AirPlaneNode * LandingRoad::getBack()const
{
	return back;
}

//utility functions

/*
* function efficency O(1)
*/
bool LandingRoad::enqueue(AirPlane * airPlane)
{
	if(!isFull())
	{
		AirPlaneNode * temp = new AirPlaneNode(airPlane);

		if(isEmpty())
		{
			front = temp;
			back = temp;
		}
		else
		{
			back->setNext(temp);
			temp->setPrev(back);
			back = temp;
		}
		//increment current size
		setCurrSize(getCurrSize() + 1);

		SYSTEM->display(IDC_TEXT_DISPLAY_PLANE, " 'Road-"+ this->getName()+"' Accepted AirPlane#" + SYSTEM->toString<long>(airPlane->getIdnum()) + ".", 1);

		return true;
	}

	return false;

}

/*
* function efficency O(1)
*/
AirPlane * LandingRoad::dequeue()
{

	if(!isEmpty())
	{
		//check if back  is the same as front change back
		//to null for dequeuing process
		if(back == front)
			back = NULL;

		AirPlaneNode * temp = front;
		front = front->getNext();

		//set airplane from temp to plane object
		AirPlane * plane = new AirPlane(temp->getAirPlane());

		if(front != NULL)
			front->setPrev(NULL);

		delete temp;

		//increment current size
		setCurrSize(getCurrSize() - 1);

		SYSTEM->display(IDC_TEXT_DISPLAY_PLANE, " 'Road-"+ this->getName() +"' Removed AirPlane#" + SYSTEM->toString<long>(plane->getIdnum()) + ".", 1);

		return plane;
	}
	else
		return NULL;
}

/*
* function efficency O(1)
*/
bool LandingRoad::isFull()
{
	//first check if is full based on max size
	if(getCurrSize() < getMaxSize())
	{
		AirPlaneNode * temp = new AirPlaneNode(new AirPlane());

		if(temp != NULL)
		{
			delete temp;
			return false;
		}
	}
	else
		return true;
}

/*
* function efficency O(1)
*/
bool LandingRoad::isEmpty()
{
	if(front == NULL)
		return true;
	else
		return false;
}
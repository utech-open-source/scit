package gui.forms;

import gui.Resource;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AnimalForm extends Form{

	/*
	 * Labels For Attributes in the Animal class
	 */
	private JLabel breedLb;
	private JLabel genderLb;
	private JLabel ageLb;
	private JLabel typeLb;
	
	/*
	 * TextField For Attributes in the Animal class
	 */
	private JTextField breedTf;
	private JComboBox<String> genderTf;
	private JTextField ageTf;
	private JComboBox<String> typeTf;
	
	public AnimalForm()
	{
		super();
		
		//set section title
		setSecTitle(Resource.secAnimalTitle);
		
		setBreedLb(Label(Resource.breedLb));
		setGenderLb(Label(Resource.genderLb));
		setAgeLb(Label(Resource.ageLb));
		setTypeLb(Label(Resource.typeLb));
		
		setBreedTf(TextField(Resource.breedTf));
		setGenderTf(ComboBox(Resource.genderTf));
		setAgeTf(TextField(Resource.ageTf));
		setTypeTf(ComboBox(Resource.typeTf));
		
	}

	/**
	 * @return the breedLb
	 */
	public JLabel getBreedLb() {
		return breedLb;
	}

	/**
	 * @param breedLb the breedLb to set
	 */
	public void setBreedLb(JLabel breedLb) {
		this.breedLb = breedLb;
	}

	/**
	 * @return the genderLb
	 */
	public JLabel getGenderLb() {
		return genderLb;
	}

	/**
	 * @param genderLb the genderLb to set
	 */
	public void setGenderLb(JLabel genderLb) {
		this.genderLb = genderLb;
	}

	/**
	 * @return the ageLb
	 */
	public JLabel getAgeLb() {
		return ageLb;
	}

	/**
	 * @param ageLb the ageLb to set
	 */
	public void setAgeLb(JLabel ageLb) {
		this.ageLb = ageLb;
	}

	/**
	 * @return the breedTf
	 */
	public JTextField getBreedTf() {
		return breedTf;
	}

	/**
	 * @param breedTf the breedTf to set
	 */
	public void setBreedTf(JTextField breedTf) {
		this.breedTf = breedTf;
	}

	/**
	 * @return the genderTf
	 */
	public JComboBox<String> getGenderTf() {
		return genderTf;
	}

	/**
	 * @param genderTf the genderTf to set
	 */
	public void setGenderTf(JComboBox<String> genderTf) {
		this.genderTf = genderTf;
	}

	/**
	 * @return the ageTf
	 */
	public JTextField getAgeTf() {
		return ageTf;
	}

	/**
	 * @param ageTf the ageTf to set
	 */
	public void setAgeTf(JTextField ageTf) {
		this.ageTf = ageTf;
	}
	
	
	/**
	 * @return the typeTf
	 */
	public JComboBox<String> getTypeTf() {
		return typeTf;
	}

	/**
	 * @param typeTf the typeTf to set
	 */
	public void setTypeTf(JComboBox<String> typeTf) {
		this.typeTf = typeTf;
	}

	/**
	 * @return the type
	 */
	public JLabel getTypeLb() {
		return typeLb;
	}

	/**
	 * @param type the type to set
	 */
	public void setTypeLb(JLabel typeLb) {
		this.typeLb = typeLb;
	}
	
	/**
	 * Override abstract form addToPanel
	 */
	public void addToPanel()
	{
		getFormPanel().add(wrap(getTypeLb(), getTypeTf()));
		
		getFormPanel().add(wrap(getBreedLb(), getBreedTf()));
		
		getFormPanel().add(wrap(getGenderLb(), getGenderTf()));
		
		getFormPanel().add(wrap(getAgeLb(), getAgeTf()));
	}
}

#include<stdio.h>
#include<stdlib.h>

void main()
{
    int array[10];

    int size,i,large, small;
    size = sizeof(array)/sizeof(int);

    for(i=0; i<size; i++)
    {
        printf("Enter a number:");
        scanf("%d", & array[i]);
    }

    printf("Numbers Entered:\n");
    large = small = array[0];

    for(i=0; i<size; i++)
    {
        printf("%d\t", array[i]);
        if(large < array[i])
            large = array[i];

        if(small > array[i])
            small = array[i];
    }

    printf("\nLargest: %d\nSmallest: %d\n", large, small);
    system("pause");
}

#ifndef STACK_H
#define STACK_H

#include "node.h"

class Stack{

private:
	Node * top;

public:
	Stack();
	void createStack();
	void deleteStack();
	Node * getTop();
	void pop();
	void push(Node&);
	void push(int);
	bool checkEmpty() const;
	bool checkFull();

	~Stack();

};

#endif
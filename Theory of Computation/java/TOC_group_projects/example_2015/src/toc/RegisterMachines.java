package toc;

public class RegisterMachines {
	
	public enum Label{L0, L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12}

	/**
	* Register machine that multiply two numbers R1, R2 returns result R0
	*
	* L0: R1- -> L1,L2
	* L1: R2- -> L3,L6
	* L2: Halt
	* L3: R0+ -> L4
	* L4: R3+ -> L5
	* L5: L1
	* L6: R3- -> L7,L0
	* L7: R2+ -> L8
	* L8: L6
	*
	**/
	int multiplyRegister(int R0, int R1, int R2, int R3, Label label){
		while(true){
			switch(label){
			case L0:
				if(R1 > 0) {
					R1--;
					label = Label.L1;
				} else {
					label = Label.L2;
				}
				break;
			case L1:
				if(R2 > 0){
					R2--;
					label = Label.L3;
				} else {
					label = Label.L6;
				}
				break;
			case L2:
					return R0;
			case L3:
				R0++;
				label = Label.L4;
				break;
			case L4:
				R3++;
				label = Label.L5;
				break;
			case L5:
				label = Label.L1;
				break;
			case L6:
				if(R3 > 0) {
					R3--;
					label = Label.L7;
				} else {
					label = Label.L0;
				}
				break;
			case L7:
				R2++;
				label = Label.L8;
				break;
			case L8:
				label = Label.L6;
				break;
			default:
				break;
			}
		}
	}

	/**
	* Register machine that divides two numbers R1, R2 returns result R0
	*
	* L0: R2- ->L2,L1
	* L1: Halt
	* L2: R2+ ->L4
	* L3: R2- ->L4,L8
	* L4: R3+ ->L5
	* L5: R1- ->L6,L7
	* L6: L3
	* L7: L1
	* L8: R0+ ->L9
	* L9: R3- ->L10,L11
	* L10: R2+ ->L11
	* L11: L9
	* L12: L3
	*
	**/
	int dividerRegister(int R0, int R1, int R2, int R3, Label label){
		while(true){
			switch(label){
			case L0:
				if(R2 > 0){
					R2--;
					label = Label.L2;
				}else{
					label = Label.L1;
				}
				break;
			case L1:
					return R0;
			case L2:
				R2++;
				label = Label.L3;
				break;
			case L3:
				if(R2 > 0) {
					R2--;
					label = Label.L4;
				} else {
					label = Label.L8;
				}
				break;
			case L4:
				R3++;
				label = Label.L5;
				break;
			case L5:
				if(R1 > 0){
					R1--;
					label = Label.L6;
				} else {
					label = Label.L7;
				}
				break;
			case L6:
				label = Label.L3;
				break;
			case L7:
				label = Label.L1;
				break;
			case L8:
				R0++;
				label = Label.L9;
				break;
			case L9:
				if(R3 > 0) {
					R3--;
					label = Label.L10;
				} else {
					label = Label.L12;
				}
				break;
			case L10:
				R2++;
				label = Label.L11;
				break;
			case L11:
				label = Label.L9;
				break;
			case L12:
				label = Label.L3;
				break;
			}
		}
		
	}

	/**
	* Register machine that adds two numbers R1, R2 returns result R0
	* L0: R1- -> L2,L3
	* L1: L0
	* L2: R0+ -> L1
	* L3: R2- -> L5,L6
	* L4: L3
	* L5: R0+ -> L4
	* L6: Halt
	*
	**/
	int addderRegister(int R0, int R1, int R2, Label label){
		while (true)
		{
		switch(label){
			case L0:
				if(R1 > 0) {
					R1--;
					label = Label.L2;
				} else {
					label = Label.L3;
				}
				break;
			case L1:
					label = Label.L0;
				break;
			case L2:
				R0++;
				label = Label.L1;
				break;
			case L3:
				if(R2 > 0) { 
					R2--;
					label = Label.L5;
				} else {
					label = Label.L6;
				}
				break;
			case L4:
				label = Label.L3;
			case L5:
				R0++;
				label = Label.L4;
				break;
			case L6:
				return R0;
		default:
			break;
			}

		}
	}

	/**
	* Register Machine to subtract two values in register R0 and R1
	* R1 - R0 : R1 > R0
	*
	* L0: R5- -> L2,L1
	* L1: Halt
	* L2: R7- -> L3,L1
	* L3: R5- -> L2,L1
	*/
	int subtractRegister(int R0, int R1, Label label){
		while(true){
			switch(label){
			case L0:
				if(R0 > 0) {
					label = Label.L2;
					R0--;
				} else {
					label = Label.L1;
				}
				break;
			case L1:
					return R1;
			case L2:
				if(R1 > 0) {
					R1--;
					label = Label.L3;
				} else {
					label = Label.L1;
				}
				break;
			case L3:
				if(R0 > 0) {
					R0--; 
					label = Label.L2;
				} else {
					label = Label.L1;
				}
				break;
			default:
				break;
			}
		}
	}

	/**
	*  uses dividerRegister, followed by multiplyRegister and then subtractRegister to 
	*  find x mod y
	*  x - y(x / y)
	**/
	int mod(int x, int y){
		int div = dividerRegister(0, x, y, 0, Label.L0);
		int mul = multiplyRegister(0, y, div, 0, Label.L0);
		return subtractRegister(mul,x,Label.L0);
	}
	String binary(int x, Label label){
		String binaryString = "";
		while(true) {
			switch(label){
			case L0:
				if(x > 0) {
					binaryString = mod(x, 2) + binaryString;
					x = dividerRegister(0, x, 2, 0, Label.L0);
					label = Label.L2;
				}
				else {
					label = Label.L1;
				}
				break;
			case L1:
				return binaryString;
			case L2: 
				label = Label.L0;
				break;
			default:
				break;
			}
		}
	}	
	/**
	public static void main(String [] args){

		//int x = 4, y = 2;
		//int ans = subtractRegister(y,x,L0);
		//int ans = addderRegister(0, x, y, L0);
		//int ans = dividerRegister(0, x, y, 0, L0);
		//int ans = multiplyRegister(0, x, y, 0, L0);
		//int ans = mod(x, y);
		//printf("%d\n", ans);

		System.out.println(new RegisterMachines().binary(18, Label.L0));
	}
	**/

}

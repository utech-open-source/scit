#include "node.h"

Node::Node(int data, Node * next)
{
	setData(data);
	setNext(next);
}

Node::Node(const Node & node)
{
	setData(node.getData());
	setNext(node.getNext());
}

int Node::getData() const
{
	return data;
}

Node * Node::getNext() const
{
	return next;

}

void Node::setData(int data)
{
	this->data = data;
}

void Node::setNext(Node * next)
{
	this->next = next;
}
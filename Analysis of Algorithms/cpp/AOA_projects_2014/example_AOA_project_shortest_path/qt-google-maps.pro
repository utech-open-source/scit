#-------------------------------------------------
#
# Project created by QtCreator 2012-01-14T12:08:12
#
#-------------------------------------------------

QT       += core gui webkit network

TARGET = qtmaps
TEMPLATE = app

INCLUDEPATH += qjson/include/qjson/

win32-g++ {
    LIBS += "libqjson.dll"
}


SOURCES += main.cpp\
        mainwindow.cpp \
    form.cpp \
    geocode_data_manager.cpp \
    ShortestPath/ShortestPath.cpp

HEADERS  += mainwindow.h \
    form.h \
    geocode_data_manager.h \
    sleeper.h \

FORMS    += mainwindow.ui \
    form.ui




OTHER_FILES += \
    qjson/bin/libqjson.dll

RESOURCES += \
    resource.qrc



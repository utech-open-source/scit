package gui.components;

import gui.Event;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTable;

@SuppressWarnings("serial")
public class ButtonEditor extends DefaultCellEditor {
	  protected JButton button;
	  private String label;
	  private String actionClass;
	  private int row;
	  private boolean isPushed;
	  private JTable table;


	  /**
	   * 
	   * @param evt
	   * @param checkBox
	   * @param actionC
	   */
	  public ButtonEditor(JCheckBox checkBox, String actionC) {
	    super(checkBox);
	    actionClass = actionC;
	    button = new JButton();
	    button.setOpaque(true);
	    button.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	        fireEditingStopped();
	      }
	    });
	  }
	  
	  
	  /**
	   * 
	   */
	  public Component getTableCellEditorComponent(JTable table, Object value,
	      boolean isSelected, int row, int column) {
		  
	    this.row = row;
	    this.table = table;
	    
	    if (isSelected) {
	      button.setForeground(table.getSelectionForeground());
	      button.setBackground(table.getSelectionBackground());
	    } else {
	      button.setForeground(table.getForeground());
	      button.setBackground(table.getBackground());
	    }
	    label = (value == null) ? "" : value.toString();
	    button.setText(label);
	    isPushed = true;
	    return button;
	  }

	  public Object getCellEditorValue() {
	    if (isPushed) {
	      //JOptionPane.showMessageDialog(button, label);
	    
	      if(actionClass.compareToIgnoreCase("viewClientVisit") == 0) {
		      	Event.viewClientVisit((String)table.getValueAt(row, 0));
		      }
		      else
		      if(actionClass.compareToIgnoreCase("viewRemovalRequest") == 0){
		    	  	Event.viewRemovalRequest((String)table.getValueAt(row, 0));
		      }
		      else
		      if(actionClass.compareToIgnoreCase("deleteClientVisit") == 0
		    		  || actionClass.compareToIgnoreCase("deleteRemovalRequest") == 0)
		    	  Event.deleteIntervention((String)table.getValueAt(row, 0), actionClass);
		   
	    }
	    isPushed = false;
	    return new String(label);
	  }

	  public boolean stopCellEditing() {
	    isPushed = false;
	    return super.stopCellEditing();
	  }


	  protected void fireEditingStopped() {
	    super.fireEditingStopped();
	  }
}

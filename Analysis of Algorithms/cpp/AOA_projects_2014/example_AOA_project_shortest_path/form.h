#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include "geocode_data_manager.h"
#include "sleeper.h"
#include "ShortestPath/ShortestPath.h"

namespace Ui {
    class Form;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = 0);
    ~Form();



private slots:
    void goClicked();
    void initializeMap();
    void showCoordinates(double east, double north, QString address);
    //set marker to map and save marker in markers list
    QString setMarker(Vertice * vertice, bool engage);
    QString createMarker(QString latlng, QString html, bool engage);
    void errorOccured(const QString&);

    void on_lwMarkers_currentRowChanged(int currentRow);

    void on_pbRemoveMarker_clicked();

    void on_zoomSpinBox_valueChanged(int arg1);

private:
    void getCoordinates(const QString& address);
    void displayShortestPath(int type);
    QString showRoutes(Edge * edge);
    void resizeEvent(QResizeEvent* event);
    void clearMarkers();


private:
    Ui::Form *ui;
    GeocodeDataManager m_geocodeDataManager;
    //markers list
    QList <Vertice*> m_markers;
    Vertice * vertices[verticeSize];
};

#endif // FORM_H

package gui;

import gui.components.ButtonEditor;
import gui.components.ButtonRenderer;
import gui.components.ChildDialog;
import gui.components.DeleteDialog;
import gui.components.Location;
import gui.components.TableModel;
import gui.forms.ClientVisitForm;
import gui.forms.ClientVisitReportForm;
import gui.forms.FormData;
import gui.forms.RemovalRequestForm;
import gui.forms.RemovalRequestReportForm;
import gui.forms.ReportForm;
import intervention.ClientVisit;
import intervention.InterventionProgram;
import intervention.RemovalRequest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

public class Event {
	
	public static InterventionProgram IP;//new InterventionProgram();
	public static Frame frame;
	
	/**
	 * 
	 * @param report
	 * @return
	 */
	public static ActionListener reportForm(final ClientVisitReportForm report){
		ActionListener action = new ActionListener(){
			
			public void actionPerformed(ActionEvent event)
			{
				Object[] obj = FormData.getReportFormData(report);
				Object[][] objs = clientVisitObjs(IP.searchClientVisit(obj));

				resultCheck(objs, report);
			}			
			
		};
		
		return action;
				
	}
	
	/**
	 * 
	 * @param report
	 * @return
	 */
	public static ActionListener reportForm(final RemovalRequestReportForm report){
		ActionListener action = new ActionListener(){
			
			public void actionPerformed(ActionEvent event)
			{
				Object[] obj = FormData.getReportFormData(report);
				Object[][] objs = removalRequestObjs(IP.searchRemovalRequest(obj));

				resultCheck(objs, report);
			}			
			
		};
		
		return action;
				
	}
	
	public static void noResult()
	{
    	JOptionPane.showMessageDialog(null, 
				"No Results Found!", 
				"Message", 
				JOptionPane.INFORMATION_MESSAGE
				);
	}
	
	
	/**
	 * 
	 * @param objs
	 * @param report
	 */
	public static void resultCheck(Object[][] objs, final ReportForm report)
	{
        if(objs.length == 0){
        	noResult();	
        }
        else
        {
			//update table model based on search from form
            TableModel tableModel = report.getDm();
            tableModel.setData(objs);
            tableModel.fireTableDataChanged();              	
        }
        
        //call to get rid of pervious errors
        FormData.errorToString();		
	}
	
	/**
	 * 
	 * @param form
	 * @return
	 */
	public static ActionListener clientVisitSave(final ClientVisitForm form){
		
		ActionListener action = new ActionListener(){
			
			public void actionPerformed(ActionEvent event)
			{

				ClientVisit clientVisit = FormData.getClientVisitData(form);

				if(FormData.getErrors().length > 0)
				{					
					JOptionPane.showMessageDialog(null, FormData.errorToString(), "Message", JOptionPane.ERROR_MESSAGE);	
				}
				else
				{
					//save clientVisit
					IP.addClientVisit(clientVisit);
					//trigger update
					IP.getDb().setUpdateQuery(true);
					
					JOptionPane.showMessageDialog(null, "Client Visit Saved!", "Message", JOptionPane.INFORMATION_MESSAGE);
					
					//empty the current form
					FormData.setClientVisitData(form, new ClientVisit());
					//modify values in base form
					FormData.modifyForm(form);
				}
			}			
			
		};
		
		return action;
		
	}
	
	/**
	 * 
	 * @param form
	 * @return
	 */
	public static ActionListener clientVisitUpdate(final ClientVisitForm form){
		
		ActionListener action = new ActionListener(){
			
			public void actionPerformed(ActionEvent event)
			{

				ClientVisit clientVisit = FormData.getClientVisitData(form);
				
				if(FormData.getErrors().length > 0)
				{					
					JOptionPane.showMessageDialog(null, FormData.errorToString(), "Message", JOptionPane.ERROR_MESSAGE);	
				}
				else
				{
					IP.updateClientVisit(clientVisit);
					JOptionPane.showMessageDialog(null, "Client Visit Updated!", "Message", JOptionPane.INFORMATION_MESSAGE);
				}
				
			}			
			
		};
		
		return action;
		
	}
	
	/**
	 * 
	 * @return
	 */
	public static void clientVisitDelete(String interventionId){
		
		IP.deleteClientVisit(interventionId);
		
	}
	
	/**
	 * 
	 * @param interventionId
	 */
	public static void removalRequestDelete(String interventionId){
		
		IP.deleteRemovalRequest(interventionId);
		
	}
	
	/**
	 * 
	 * @param form
	 * @return
	 */
	public static ActionListener removalRequestUpdate(final RemovalRequestForm form){
		
		ActionListener action = new ActionListener(){
			
			public void actionPerformed(ActionEvent event)
			{

				RemovalRequest removalRequest = FormData.getRemovalRequestData(form);
				
				if(FormData.getErrors().length > 0)
				{					
					JOptionPane.showMessageDialog(null, FormData.errorToString(), "Message", JOptionPane.ERROR_MESSAGE);	
				}
				else
				{
					IP.updateRemovalRequest(removalRequest);
					JOptionPane.showMessageDialog(null, "Removal Request Updated!", "Message", JOptionPane.INFORMATION_MESSAGE);
				}
			}			
			
		};
		
		return action;
		
	}
	
	/**
	 * 
	 * @param form
	 * @return
	 */
	public static ActionListener removalRequestSave(final RemovalRequestForm form){
		
		ActionListener action = new ActionListener(){
			
			public void actionPerformed(ActionEvent event)
			{
				RemovalRequest removalRequest = FormData.getRemovalRequestData(form);
				
				if(FormData.getErrors().length > 0)
				{					
					JOptionPane.showMessageDialog(null, FormData.errorToString(), "Message", JOptionPane.ERROR_MESSAGE);	
				}
				else{
					
					IP.addRemovalRequest(removalRequest);
					//trigger update
					IP.getDb().setUpdateQuery(true);
					//show saved dialog
					JOptionPane.showMessageDialog(null, "Removal Request Saved!", "Message", JOptionPane.INFORMATION_MESSAGE);
					//reset the removal request form
					FormData.setRemovalRequestData(form, new RemovalRequest());
					//modify values in base form
					FormData.modifyForm(form);
				}
			}			
			
		};
		
		return action;
		
	}
	
	/**
	 * 
	 * @param frame
	 * @param interventionId
	 */
	public static void deleteIntervention(String interventionId, String interven)
	{
		DeleteDialog deleteDialog = new DeleteDialog(frame, frame.getActiveWindow());
		
		
		int value = ((Integer)deleteDialog.getOptionPane().getValue()).intValue();
		
		if (value == JOptionPane.YES_OPTION) {
			//delete clientVisit
			if(interven.equalsIgnoreCase("deleteClientVisit")) {
				clientVisitDelete(interventionId);
			}
			else{
				removalRequestDelete(interventionId);
			}
			//set update query to true
			IP.getDb().setUpdateQuery(true);
		}
		else
		//hide delete dialog
		deleteDialog.setVisible(false);
		
		System.out.println(interven);
		
	}
	
	
	
	/**
	 * 
	 * @param frame
	 * @param interventionId
	 */
	public static void viewClientVisit(String interventionId)
	{
		ClientVisit clientVisit = (ClientVisit)IP.viewClientVisit(interventionId)[0];
		
		ClientVisitForm form = new ClientVisitForm();
		
		form.getFormPanel().setBorder(BorderFactory.createEmptyBorder(20, 30, 20, 20));
		form.getFormPanel().setAlignmentX(JComponent.CENTER_ALIGNMENT);
		form.getButton().setText("Update");
		
		FormData.setClientVisitData(form, clientVisit);
		
		ActionListener action = clientVisitUpdate(form);
		
		Location loc = new Location(Resource.clientVisitFormWidth,Resource.clientVisitFormHeight, true);
		
		ChildDialog dialog = ChildDialog.showDialog(frame, form, null, action, loc, "Client Visit Edit Form","close");
        
        dialog.setVisible(true);
	}
	
	/**
	 * 
	 * @param frame
	 * @param interventionId
	 */
	public static void viewRemovalRequest(String interventionId)
	{

		RemovalRequest removalRequest = (RemovalRequest)IP.viewRemovalRequest(interventionId)[0];
		
		RemovalRequestForm form = new RemovalRequestForm();
		
		form.getFormPanel().setBorder(BorderFactory.createEmptyBorder(20, 30, 20, 20));
		form.getFormPanel().setAlignmentX(JComponent.CENTER_ALIGNMENT);
		form.getButton().setText("Update");
		
		FormData.setRemovalRequestData(form, removalRequest);
		
		ActionListener action = removalRequestUpdate(form);
		
		Location loc = new Location(Resource.removalRequestFormWidth,Resource.removalRequestFormHeight, true);
		
		ChildDialog dialog = ChildDialog.showDialog(frame, form, null, action, loc, "Removal Request Edit Form","close");
		
		dialog.setVisible(true);

	}
	
	/**
	 * 
	 * @return
	 */
	public static Object [][] clientVisitObjs(Object[] clientVisits)
	{

		Object[][] obj = new Object[clientVisits.length][];
		
		for(int cnt = 0; cnt < clientVisits.length; cnt++)
		{
				obj[cnt] = new Object[]{
										((ClientVisit)clientVisits[cnt]).getIdCode(),
										((ClientVisit)clientVisits[cnt]).getDate().toString(),
										((ClientVisit)clientVisits[cnt]).getAddress().getStreet() + ", " +
										((ClientVisit)clientVisits[cnt]).getAddress().getParish(),
										((ClientVisit)clientVisits[cnt]).getClient().getFname(),
										((ClientVisit)clientVisits[cnt]).getClient().getLname(),
										((ClientVisit)clientVisits[cnt]).getVisitDate().toString(),
										((ClientVisit)clientVisits[cnt]).getAnimal().getType(),
										((ClientVisit)clientVisits[cnt]).getAnimal().getBreed(),
										"edit",
										"delete"
										};
				
				
				
		}
		
		return obj;
	}
	
	/**
	 * 
	 * @param removalRequests
	 * @return
	 */
	public static Object [][] removalRequestObjs(Object[] removalRequests)
	{
		Object[][] obj = new Object[removalRequests.length][];
		
		for(int cnt = 0; cnt < removalRequests.length; cnt++)
		{
				obj[cnt] = new Object[]{
										((RemovalRequest)removalRequests[cnt]).getIdCode(),
										((RemovalRequest)removalRequests[cnt]).getDate().toString(),
										((RemovalRequest)removalRequests[cnt]).getAddress().getLotNumber(),
										((RemovalRequest)removalRequests[cnt]).getAddress().getStreet(),
										((RemovalRequest)removalRequests[cnt]).getAddress().getParish(),
										((RemovalRequest)removalRequests[cnt]).getClient().getFname(),
										((RemovalRequest)removalRequests[cnt]).getClient().getLname(),
										((RemovalRequest)removalRequests[cnt]).getOutcome(),
										"edit",
										"delete"
										};
		}
		
		return obj;
	}
	
	/**
	 * 
	 * @param report
	 * @param btn
	 * @param displayText
	 * @param type
	 * @return
	 */
	public static ChildDialog newReport(ReportForm report, JButton btn, String displayText, String type)
	{
		
		ButtonEditor edit = new ButtonEditor(new JCheckBox(),"view"+type);
		ButtonEditor delete = new ButtonEditor(new JCheckBox(), "delete"+type);
		
		
		report.getTable().getColumn("View").setCellRenderer(new ButtonRenderer());
		report.getTable().getColumn("View").setCellEditor(edit);
		
		report.getTable().getColumn("Delete").setCellRenderer(new ButtonRenderer());
		report.getTable().getColumn("Delete").setCellEditor(delete);
		
		report.getButton().setText("Search");
		
		Location loc = new Location(80, 80);
		
		ChildDialog dialog = null;
		
		if(type.equalsIgnoreCase("RemovalRequest"))
			dialog = ChildDialog.showDialog(frame, 
						report, btn, reportForm((RemovalRequestReportForm)report), loc, displayText,"close");
		else
			dialog = ChildDialog.showDialog(frame, 
						report, btn, reportForm( (ClientVisitReportForm)report), loc, displayText,"close");
		
		
		return dialog;
	}
	

}

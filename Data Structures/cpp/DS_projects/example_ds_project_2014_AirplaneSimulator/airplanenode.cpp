#include "airplaneNode.h"

AirPlaneNode::AirPlaneNode()
{
	next = NULL;
	prev = NULL;
}

AirPlaneNode::AirPlaneNode(AirPlane* airPlane, AirPlaneNode* next, AirPlaneNode* prev)
{
	this->airPlane = airPlane;
	this->next = next;
	this->prev = prev;
}

//mutators
void AirPlaneNode::setNext(AirPlaneNode * node)
{
	next = node;
}

void AirPlaneNode::setPrev(AirPlaneNode * node)
{
	prev = node;
}

void AirPlaneNode::setAirPlane(AirPlane * airPlane)
{
	this->airPlane = airPlane;
}

//accessors
AirPlaneNode * AirPlaneNode::getNext() const
{
	return next;
}

AirPlaneNode * AirPlaneNode::getPrev() const
{
	return prev;
}

AirPlane * AirPlaneNode::getAirPlane() const
{
	return airPlane;
}
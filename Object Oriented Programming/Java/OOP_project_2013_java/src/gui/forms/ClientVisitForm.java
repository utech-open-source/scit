package gui.forms;

import gui.Resource;

import javax.swing.JLabel;
import javax.swing.JTextArea;

public class ClientVisitForm extends BaseForm{
	
	private DateForm visitDateForm;
	private AnimalForm animalForm;
	
	/*
	 * Labels For Attributes in the ClientVisit
	 */
	private JLabel reasonLb;
	
	/*
	 * TextField For Attributes in the ClientVisit
	 */	
	private JTextArea reasonTf;
	
	public ClientVisitForm()
	{
		super();
		
		setAnimalForm(new AnimalForm());
		
		setVisitDateForm(new DateForm());
		
		setReasonLb(Label(Resource.reasonLb));
		
		/*
		 * TextField For Attributes in the ClientVisit
		 */
	
		setReasonTf(TextArea(5,20));
		
	}

	/**
	 * @return the reasonLb
	 */
	public JLabel getReasonLb() {
		return reasonLb;
	}

	/**
	 * @param reasonLb the reasonLb to set
	 */
	public void setReasonLb(JLabel reasonLb) {
		this.reasonLb = reasonLb;
	}

	/**
	 * @return the reasonTf
	 */
	public JTextArea getReasonTf() {
		return reasonTf;
	}

	/**
	 * @param reasonTf the reasonTf to set
	 */
	public void setReasonTf(JTextArea reasonTf) {
		this.reasonTf = reasonTf;
	}

	/**
	 * @return the visitDateForm
	 */
	public DateForm getVisitDateForm() {
		return visitDateForm;
	}

	/**
	 * @param visitDateForm the visitDateForm to set
	 */
	public void setVisitDateForm(DateForm visitDateForm) {
		this.visitDateForm = visitDateForm;
	}

	/**
	 * @return the animalForm
	 */
	public AnimalForm getAnimalForm() {
		return animalForm;
	}

	/**
	 * @param animalForm the animalForm to set
	 */
	public void setAnimalForm(AnimalForm animalForm) {
		this.animalForm = animalForm;
	}
	
	/**
	 * Override addToPannel
	 */
	
	public void addToPanel()
	{
		//set address selection to true
		super.getAddressForm().isSlection = true;
		super.getAddressForm().setSecTitle("The Location Of The Clinic");
		
		super.addToPanel();
		
		getFormPanel().add(space(wrap(getReasonLb(),ScrollPane(getReasonTf())), 10));
		
		getVisitDateForm().setTitle("Visit Date");
		
		getFormPanel().add(space(title(getAnimalForm().getSecTitle()), 10));
		
		getFormPanel().add(space(getAnimalForm().render(), 10));
		
		getFormPanel().add(space(getVisitDateForm().render(), 10));
		
		//getFormPanel().add(space(wrap(getButton()), 10));
	}
}

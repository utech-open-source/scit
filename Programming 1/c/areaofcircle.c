//Author: Mark Robinson
//Program to calculate the area of a circle

#include <stdio.h>
#include <conio.h>
#define PI 3.14

int area(int radius)
{
   if(radius)
   return(PI * radius*radius); else return 0;
}

void main(void)
{

   int areaofsquare;
   int radius;

   printf("\ninput radius of circle: ");
   scanf("%d", &radius);

   areaofsquare = area(radius);

   printf("areaofsquare = %d", areaofsquare);

   getch();

}
/**
* Generic Node
* Date : November 1, 2014 
**/

#ifndef NODE_H
#define NODE_H

template <typename TYPE> class Heap;

template <typename TYPE>
class Node{

	friend class Heap<TYPE>;

private:
	TYPE data;
	Node<TYPE> * left;
	Node<TYPE> * right;
	Node<TYPE> * parent;

public:
	Node(const TYPE &);
	TYPE getData()const;
	void setData(const TYPE &);
};

template <typename TYPE>
Node<TYPE>::Node(const TYPE & node):
	data(node), left(0), right(0), parent(0)
{
}

template <typename TYPE>
TYPE Node<TYPE>::getData()const
{
	return this->data;
}

template <typename TYPE>
void Node<TYPE>::setData(const TYPE & data)
{
	this->data = data;
}

#endif
#----------------------------------------------------------------
# Generated CMake target import file for configuration "".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "qjson" for configuration ""
set_property(TARGET qjson APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(qjson PROPERTIES
  IMPORTED_IMPLIB_NOCONFIG "C:/Program Files (x86)/qjson/lib/libqjson.dll.a"
  IMPORTED_LINK_INTERFACE_LIBRARIES_NOCONFIG "Qt4::QtGui;Qt4::QtCore"
  IMPORTED_LOCATION_NOCONFIG "C:/Program Files (x86)/qjson/bin/libqjson.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS qjson )
list(APPEND _IMPORT_CHECK_FILES_FOR_qjson "C:/Program Files (x86)/qjson/lib/libqjson.dll.a" "C:/Program Files (x86)/qjson/bin/libqjson.dll" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)

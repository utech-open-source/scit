package toc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;

import toc.gui.View;

public class TMADriver {

	private View view;

	TMADriver(View view) {
		this.view = view;

		final View tmp = view;
		view.getConvert().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Runnable runnable = new Runnable() {
					@Override
					public void run() {
						execute(Integer.parseInt(tmp.userInput()));
					}
				};
				
				new Thread(runnable).start();
			}

		});
	}

	public static void main(String arg[]) {

		new TMADriver(new View(20));

	}

	public void execute(int input) {

		int length = 20;

		TuringMachine<Character> A = new TuringMachine<Character>(length, "q0",
				"q23", view);

		String test = new RegisterMachines().binary(input,
				RegisterMachines.Label.L0);

		for (int i = 0; i < test.length(); i++) {
			A.addValueToTape(test.charAt(i), i);
		}

		A.addState("q0", '0', TuringMachine.Right);
		A.addState("q0", '1', TuringMachine.Right);
		A.addState("q0", null, '#', TuringMachine.Left, "q1");

		A.addState("q1", '1', 'A', TuringMachine.Right, "q2");
		A.addState("q1", '0', 'B', TuringMachine.Right, "q3");

		A.addState("q2", '1', TuringMachine.Right);
		A.addState("q2", '0', TuringMachine.Right);
		A.addState("q2", 'A', TuringMachine.Right);
		A.addState("q2", 'B', TuringMachine.Right);
		A.addState("q2", '#', TuringMachine.Right);
		A.addState("q2", null, '1', TuringMachine.Left, "q4");

		A.addState("q3", '1', TuringMachine.Right);
		A.addState("q3", '0', TuringMachine.Right);
		A.addState("q3", 'A', TuringMachine.Right);
		A.addState("q3", 'B', TuringMachine.Right);
		A.addState("q3", '#', TuringMachine.Right);
		A.addState("q3", null, '0', TuringMachine.Left, "q4");

		A.addState("q4", '1', TuringMachine.Left);
		A.addState("q4", '0', TuringMachine.Left);
		A.addState("q4", '#', TuringMachine.Left, "q5");

		A.addState("q5", 'A', TuringMachine.Left);
		A.addState("q5", 'B', TuringMachine.Left);
		A.addState("q5", '1', TuringMachine.Stay, "q1");
		A.addState("q5", '0', TuringMachine.Stay, "q1");
		A.addState("q5", null, TuringMachine.Right, "q6");

		A.addState("q6", 'B', '0', TuringMachine.Right);
		A.addState("q6", 'A', '1', TuringMachine.Right);
		A.addState("q6", '#', TuringMachine.Right, "q7");

		A.addState("q7", '0', TuringMachine.Right);
		A.addState("q7", '1', TuringMachine.Right);
		A.addState("q7", null, TuringMachine.Left, "q8");

		A.addState("q8", '0', TuringMachine.Left, "q9");
		A.addState("q8", '1', TuringMachine.Left, "q9");

		A.addState("q9", '0', TuringMachine.Left, "q10");
		A.addState("q9", '1', TuringMachine.Left, "q10");
		A.addState("q9", '#', TuringMachine.Stay, "q13");

		A.addState("q10", '0', TuringMachine.Left, "q11");
		A.addState("q10", '1', TuringMachine.Left, "q11");
		A.addState("q10", '#', TuringMachine.Stay, "q13");

		A.addState("q11", '0', TuringMachine.Left, "q12");
		A.addState("q11", '1', TuringMachine.Left, "q12");
		A.addState("q11", '#', TuringMachine.Stay, "q13");

		A.addState("q12", '0', 'x', TuringMachine.Left);
		A.addState("q12", '1', 'x', TuringMachine.Left);
		A.addState("q12", '#', TuringMachine.Stay, "q13");

		// Subtraction of the binary numbers
		A.addState("q13", '#', TuringMachine.Right);
		A.addState("q13", 'x', TuringMachine.Right);
		A.addState("q13", '0', TuringMachine.Right);
		A.addState("q13", '1', TuringMachine.Right);
		A.addState("q13", null, TuringMachine.Left, "q14");

		A.addState("q14", 'x', TuringMachine.Stay, "q23");
		A.addState("q14", '#', TuringMachine.Stay, "q23");
		A.addState("q14", '1', 'p', TuringMachine.Left, "q15");
		A.addState("q14", '0', 'p', TuringMachine.Left, "q16");

		A.addState("q15", 'x', TuringMachine.Left);
		A.addState("q15", '0', TuringMachine.Left);
		A.addState("q15", '1', TuringMachine.Left);
		A.addState("q15", '#', TuringMachine.Left, "q17");

		A.addState("q16", 'x', TuringMachine.Left);
		A.addState("q16", '0', TuringMachine.Left);
		A.addState("q16", '1', TuringMachine.Left);
		A.addState("q16", '#', TuringMachine.Left, "q18");

		A.addState("q17", 'x', TuringMachine.Left);
		A.addState("q17", '0', 'x', TuringMachine.Left, "q22");
		A.addState("q17", '1', 'x', TuringMachine.Right, "q19");

		A.addState("q18", 'x', TuringMachine.Left);
		A.addState("q18", '1', 'x', TuringMachine.Right, "q21");
		A.addState("q18", '0', 'x', TuringMachine.Right, "q19");

		A.addState("q19", 'x', TuringMachine.Right);
		A.addState("q19", '0', TuringMachine.Right);
		A.addState("q19", '1', TuringMachine.Right);
		A.addState("q19", '#', TuringMachine.Right);
		A.addState("q19", 'p', '0', TuringMachine.Left, "q14");

		A.addState("q20", 'x', TuringMachine.Right, "q21");
		A.addState("q20", '#', TuringMachine.Right, "q21");
		A.addState("q20", '0', '1', TuringMachine.Right);

		A.addState("q21", 'x', TuringMachine.Right);
		A.addState("q21", '0', TuringMachine.Right);
		A.addState("q21", '1', TuringMachine.Right);
		A.addState("q21", '#', TuringMachine.Right);
		A.addState("q21", 'p', '1', TuringMachine.Left, "q14");

		// borrow
		A.addState("q22", '0', TuringMachine.Left);
		A.addState("q22", '1', '0', TuringMachine.Right, "q20");

		A.addState("q23");

		A.run(0);
		
		view.setText(A.getResult());
	}
}

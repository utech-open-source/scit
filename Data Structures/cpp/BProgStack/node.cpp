#include "node.h"

Node::Node():
data(' '), next(NULL)
{
}

Node::Node(char data):
data(data), next(NULL)
{

}

char Node::getData()const
{
	return data;
}

Node * Node::getNext() const
{
	return next;
}

void Node::setData(char data)
{
	this->data = data;
}

void Node::setNext(Node * next)
{
	this->next = next;
}
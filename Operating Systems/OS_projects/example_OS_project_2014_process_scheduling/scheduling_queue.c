#include "scheduling_queue.h"

//PCBNodePtr SQFrontSys = NULL, SQFrontInt = NULL, SQFrontBat = NULL;
//PCBNodePtr SQBackSys = NULL, SQBackInt =  NULL, SQBackBat = NULL;

PQueue SQSys, SQInt, SQBat;

int SQSysCnt = 0, SQIntCnt = 0, SQBatCnt = 0;

/*
** creates the sheduling queues
*/
void createShedulingQueues()
{
  createPQueue(&SQSys);
  createPQueue(&SQInt);
  createPQueue(&SQBat);
}

/*
** Add pcb to the sheduling queue
*/
bool AddToShedulingQueue(int key, PCBPtr block, PQueuePtr SQptr)
{
    return insert(key, block, SQptr);
}

/*
** Remove pcb from the front of the sheduling queue
*/
PCBPtr RemoveFromShedulingQueue(PQueuePtr SQptr)
{
	return removeMin(SQptr);
}

/*
** get copy of the front of the sheduling queue
*/
bool FrontOfShedulingQueue(PCBPtr block, PQueuePtr SQptr)
{
    return min(block, SQptr);
}

/* --------------------SYSTEM--------------------------------*/

/*
** Add pcb to the system sheduling queue
*/
bool AddToShedulingQueueSys(int key, PCBPtr block)
{
	if( AddToShedulingQueue(key, block, &SQSys) ){
		SQSysCnt++;
		return true;
	}
	return false;
}

/*
** Remove pcb from the front of the system sheduling queue
*/
PCBPtr RemoveFromShedulingQueueSys()
{
    PCBPtr temp = RemoveFromShedulingQueue(&SQSys);
	if( temp != NULL){
		SQSysCnt--;
	}
	return temp;
}

/*
** get copy of the front of the system sheduling queue
*/
bool FrontOfShedulingQueueSys(PCBPtr block)
{
	return FrontOfShedulingQueue(block, &SQSys);
}

int getSQSysCnt()
{
	return SQSysCnt;
}

/* --------------------INTERACTIVE--------------------------------*/

/*
** Add pcb to the interactive sheduling queue
*/
bool AddToShedulingQueueInt(int key, PCBPtr block)
{
	if(AddToShedulingQueue(key, block, &SQInt)){
		SQIntCnt++;
		return true;
	}
	return false;
}

/*
** Remove pcb from the front of the interactive sheduling queue
*/
PCBPtr RemoveFromShedulingQueueInt()
{
    PCBPtr temp = RemoveFromShedulingQueue(&SQInt);
	if( temp != NULL){
		SQIntCnt--;
	}
	return temp;
}

/*
** get copy of the front of the interactive sheduling queue
*/
bool FrontOfShedulingQueueInt(PCBPtr block)
{
	return FrontOfShedulingQueue(block, &SQInt);
}

int getSQIntCnt()
{
	return SQIntCnt;
}

/* --------------------Batch--------------------------------*/

/*
** Add pcb to the batch sheduling queue
*/
bool AddToShedulingQueueBat(int key, PCBPtr block)
{
	if(AddToShedulingQueue(key, block, &SQBat)){
		SQBatCnt++;
		return true;
	}
	return false;
}

/*
** Remove pcb from the front of the batch sheduling queue
*/
PCBPtr RemoveFromShedulingQueueBat()
{
    PCBPtr temp = RemoveFromShedulingQueue(&SQBat);
	if( temp != NULL ){
		SQBatCnt--;
	}
	return temp;
}

/*
** get copy of the front of the batch sheduling queue
*/
bool FrontOfShedulingQueueBat(PCBPtr block)
{
	return FrontOfShedulingQueue(block, &SQBat);
}

int getSQBatCnt()
{
	return SQBatCnt;
}

/*-----------------------------------------------------------------------*/

/*
** Add a process to its correct sheduling queue
*/
bool AddProcessToShedulingQueue(PCBPtr block)
{
	//system
	if(block->process_type == SYSTEM)
		return AddToShedulingQueueSys(SYSTEM_ALGO, block);
	else //interactive
		if(block->process_type == INTERACTIVE)
			return AddToShedulingQueueInt(INTERACTIVE_ALGO, block);
		else //batch
			if(block->process_type == BATCH)
				return AddToShedulingQueueBat(BATCH_ALGO, block);
			else
				return false;
}

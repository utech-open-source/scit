package gui.forms;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;

import gui.components.TableModel;

public class ReportForm extends Form{

	private JTable table;
	private TableModel dm;
	private JPanel panel;
	
	private AddressForm addressForm;
	
	public ReportForm(String [] names, Object [][] obj)
	{
		super();
		setDm(new TableModel(names, obj));
		setTable(Table(dm));
		
		setPanel(new JPanel());
		setAddressForm(new AddressForm());
	}
	
	/**
	 * @return the table
	 */
	public JTable getTable() {
		return table;
	}

	/**
	 * @param table the table to set
	 */
	public void setTable(JTable table) {
		this.table = table;
	}

	/**
	 * @return the dm
	 */
	public TableModel getDm() {
		return dm;
	}

	/**
	 * @param tableModel the dm to set
	 */
	public void setDm(TableModel tableModel) {
		this.dm = tableModel;
	}
	
	
	/**
	 * @return the addressForm
	 */
	public AddressForm getAddressForm() {
		return addressForm;
	}

	/**
	 * @param addressForm the addressForm to set
	 */
	public void setAddressForm(AddressForm addressForm) {
		this.addressForm = addressForm;
	}

	/**
	 * @return the panel
	 */
	public JPanel getPanel() {
		return panel;
	}

	/**
	 * @param panel the panel to set
	 */
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public void addToPanel() {
		JTableHeader header = getTable().getTableHeader();

		getPanel().setLayout(new BorderLayout());
		getPanel().add(header, BorderLayout.NORTH);
		getPanel().add(getTable(), BorderLayout.CENTER);

	}

}

#include "ShortestPath.h"

void shortestPath(Heap<Vertice *> * heap, int size, int type)
{	
	while(!heap->isEmpty())
	{
		Vertice * start = NULL;
		heap->removeMin(start);

		//visit all edges in the vertices
		for(size_t i = 0; i < start->getEdges().size(); i++)
		{
			double prev = start->getWeight();
			double nextWeight = prev;

			if(type == DISTANCE)
			{
				nextWeight += start->getEdges().at(i).getDistance();
			}
			if(type == TIME)
			{
				nextWeight += start->getEdges().at(i).getTime();
			}

			if( nextWeight < start->getEdges().at(i).getRight()->getWeight())
			{
				//start->getEdges().at(i).getRight()->setWeight(nextWeight);
				start->getEdges().at(i).getRight()->setPred(start);
				heap->decreaseKey<double>(start->getEdges().at(i).getRight()->getWrapper(), start->getEdges().at(i).getRight(), nextWeight);
			}
		}
	}
}

void showShortestPath(Vertice * vertices[], int size, int type)
{
	Heap<Vertice *> heap;

	//add all the vertices to the heap
	for(int i = 0; i < size; i++)
	{
		if(i > 0) vertices[i]->setWeight(INFINITY);
		heap.insert(vertices[i]);
	}

	shortestPath(&heap, size, type);
}

void getVertices(Vertice * vertices[])
{
    vertices[0] = new Vertice("Kingston",17.9833, -76.8);
    vertices[1] = new Vertice("Port Antonio", 18.175701, -76.450253);
    vertices[2] = new Vertice("Port Royal", 17.9367669, -76.8411038);
    vertices[3] = new Vertice("Spanish Town", 17.9959, -76.9551);
    vertices[4] = new Vertice("Ewarton", 18.1833, -77.0833);
    vertices[5] = new Vertice("Annoto Bay", 18.2703, -76.7706);
    vertices[6] = new Vertice("Port Maria", 18.3702, -76.8903);
    vertices[7] = new Vertice("St.Ann's Bay", 18.4329, -77.1974);
    vertices[8] = new Vertice("Ocho Rios", 18.4057, -77.0967);
    vertices[9] = new Vertice("Christiana", 18.172, -77.489);
    vertices[10] = new Vertice("Mandeville", 18.0333, -77.5);
    vertices[11] = new Vertice("May Pen", 17.9646, -77.2434);
    vertices[12] = new Vertice("Falmouth", 18.49, -77.661);
    vertices[13] = new Vertice("Santa Cruz", 18.05, -77.7);
    vertices[14] = new Vertice("Black River", 18.0335, -77.8567);
    vertices[15] = new Vertice("Savanna la Mar", 18.2167, -78.1333);
    vertices[16] = new Vertice("Montego Bay", 18.4667, -77.9167);
    vertices[17] = new Vertice("Lucea", 18.4504186, -78.17639369);
    vertices[18] = new Vertice("Morant Point", 17.9149, -76.1882);
    vertices[19] = new Vertice("Alley", 17.789, -77.2693);
    vertices[20] = new Vertice("Southfield", 18.406451, -77.296264);
    vertices[21] = new Vertice("Negril", 18.2683058, -78.3472424);

    //kingston
    vertices[0]->addEdge(Edge(87.4, 105, vertices[0], vertices[18]));
    vertices[0]->addEdge(Edge(92.2, 124, vertices[0], vertices[1]));
    vertices[0]->addEdge(Edge(46.82, 73, vertices[0], vertices[5]));
    vertices[0]->addEdge(Edge(20.4, 28, vertices[0], vertices[3]));
    vertices[0]->addEdge(Edge(26.3, 32, vertices[0], vertices[2]));

    //added
    //vertices[2]->addEdge(Edge(24, vertices[2], vertices[3]));

    vertices[18]->addEdge(Edge(67.7, 83, vertices[18], vertices[1]));
    vertices[1]->addEdge(Edge(45.5, 52, vertices[1], vertices[5]));
    vertices[5]->addEdge(Edge(25.2, 27, vertices[5], vertices[6]));
    vertices[6]->addEdge(Edge(31.4, 32, vertices[6], vertices[8]));
    vertices[8]->addEdge(Edge(12.1, 14, vertices[8], vertices[7]));

    vertices[7]->addEdge(Edge(57.1, 57, vertices[7], vertices[12]));
    vertices[12]->addEdge(Edge(34.9, 36, vertices[12], vertices[16]));
    vertices[16]->addEdge(Edge(36.5, 37, vertices[16], vertices[17]));
    vertices[17]->addEdge(Edge(39.6, 37, vertices[17], vertices[21]));
    vertices[3]->addEdge(Edge(54.7, 68, vertices[3], vertices[19]));

    vertices[19]->addEdge(Edge(93.1, 106, vertices[19], vertices[13]));
    vertices[13]->addEdge(Edge(28.9, 28, vertices[13], vertices[14]));
    vertices[14]->addEdge(Edge(47.3, 47, vertices[14], vertices[15]));
    vertices[15]->addEdge(Edge(28.1, 29, vertices[15], vertices[21]));
    vertices[3]->addEdge(Edge(30.5, 43, vertices[3], vertices[4]));

    vertices[4]->addEdge(Edge(46.8, 48, vertices[4], vertices[8]));
    vertices[4]->addEdge(Edge(77.6, 117, vertices[4], vertices[9]));
    vertices[3]->addEdge(Edge(34.6, 37, vertices[3], vertices[11]));
    vertices[11]->addEdge(Edge(24.5, 45, vertices[11], vertices[19]));
    vertices[11]->addEdge(Edge(39.4, 44, vertices[11], vertices[10]));

    vertices[10]->addEdge(Edge(20.9, 32, vertices[10], vertices[9]));
    vertices[9]->addEdge(Edge(70.2, 93, vertices[9], vertices[10]));
    vertices[9]->addEdge(Edge(51.2, 54, vertices[9], vertices[13]));
    vertices[15]->addEdge(Edge(49.5, 59, vertices[15], vertices[16]));
}

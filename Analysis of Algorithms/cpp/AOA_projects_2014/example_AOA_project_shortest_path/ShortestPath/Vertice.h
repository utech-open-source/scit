#ifndef VERTICE_H
#define VERTICE_H

#include <string>
#include <limits>
#include <vector>
#include "Edge.h"

#define INFINITY std::numeric_limits<double>::infinity()

class Vertice{
private:
	std::string name;
	//location variables
    double lat;
    double lng;
	std::vector<Edge> edges;
	double weight;
	bool visited;
	Vertice * pred;
	void * wrapper;

public:
	Vertice(){
		this->name = " ";
		this->weight = 0;//INFINITY;
		edges = std::vector<Edge>(0);
		visited = false;
		pred = NULL;
		wrapper = NULL;
	}

	Vertice(std::string name, double lat, double lng){
		this->name = name;
		this->lat = lat;
		this->lng = lng;
		this->weight = 0;//INFINITY;
		edges = std::vector<Edge>(0);
		visited = false;
		pred = NULL;
		wrapper = NULL;
	}

	void setName(std::string name){
		this->name = name;
	}

	std::string getName() const{
		return this->name;
	}

	double getLng() const{
		return this->lng;
	}

	double getLat() const{
		return this->lat;
	}

	void addEdge(Edge edge){
		edges.insert(edges.end(), edge);
	}

	std::vector<Edge> getEdges(){
		return edges;
	}

	double getWeight(){
		return weight;
	}

	void setWeight(double weight){
		this->weight = weight;
	}

	void setVisited(bool visited){
		this->visited = visited;
	}

	bool getVisited()const{
		return this->visited;
	}

	void setPred(Vertice * pred){
		this->pred = pred;
	}

	Vertice * getPred()const{
		return this->pred;
	}

	void setWrapper(void * wrapper){
		this->wrapper = wrapper;
	}

	void * getWrapper()const{
		return this->wrapper;
	}

	//overload operators for comparison between vertices
	bool operator<(Vertice * vertice){
		return (this->getWeight() < vertice->getWeight());
	}

	bool operator<=(Vertice * vertice){
		return (this->getWeight() <= vertice->getWeight());
	}

	bool operator>(Vertice * vertice){
		return (this->getWeight() > vertice->getWeight());
	}

	bool operator>=(Vertice * vertice){
		return (this->getWeight() >= vertice->getWeight());
	}

	//overload operator for setting weight
	void operator=(const double & weight){
		this->setWeight(weight);
	}
	 
	friend ostream &operator<<(ostream & output, Vertice * vertice){
		output<<vertice->getName()<<" "<<vertice->getWeight();
		return output;
	}
};

#endif
#include "virtual_processor.h"

bool interrupt = false;
bool stop_virtual_processor = false;
PROCESSOR_ONE * p = NULL;

/*
** virtual processor
** add items back to the process block queues
** runs the additional operations which includes the kernel
**  needed to add new pcbs to processor to execute
*/
void virtualProcessor(size_t(*AdditionOperations)(void))
{
    //return if process to execute is null
	if(p == NULL) return;

	for(;;)
	{
	    string data = malloc(50);
	    //sprintf(data, "CYCLE %d, STATE %d, #Terminated: %d \n", p->cycle, p->state, p->num_of_process_executed );
		//output(data);
		AdditionOperations();
		if( p->cpu_bound_process != NULL)
		{
            //printf("CPU BOUND PROCESS: %d\n", p->cpu_bound_process->process_id);
			//set processor to runing state
			p->state = RUNNING;

			if( p->cpu_bound_process->start_time == -1)
			{
				p->cpu_bound_process->start_time = p->cycle;
			}
			//decrement the remaining burst time
			if(interrupt == false)
				p->cpu_bound_process->remaining_burst_time--;
			//change to remaining burst time
			if( p->cpu_bound_process->remaining_burst_time == 0)
			{
				p->cpu_bound_process->end_time = p->cycle + 1;
				p->num_of_process_executed += 1;
				//output("\n------Executing PCB-----\n");
				displayProcessControlBlock(*p->cpu_bound_process);
				//output("\n----------END----------\n\n");
				//remove cpu process
				p->cpu_bound_process = NULL;
				//set state to waiting after execution ends
				p->state = WAITING;
			}
			//if pcb is still active accept interupt
			if( p->cpu_bound_process != NULL && interrupt == true)
			{
				p->cpu_bound_process->end_time = p->cycle;// + 1;
				//output("\n------Execution Interrupt PCB-----\n");
				displayProcessControlBlock(*p->cpu_bound_process);
				if(p->cpu_bound_process->remaining_burst_time > 0)
				{
					//output("\n---- Adding Back To Queue----\n");
					p->cpu_bound_process->start_time = -1;
					AddProcessToShedulingQueue(p->cpu_bound_process);
				}
				//output("\n----------END----------\n\n");
				//remove cpu process
				p->cpu_bound_process = NULL;
				//reset the interrupt to false
				interrupt = false;
				//set state to waiting after execution ends
				p->state = WAITING;
				p->cycle--;
			}
		}
		//check if virtual processor interrupt was engaged
		if(stop_virtual_processor == true)
		{
			break;
		}
		p->cycle++;
	}

    #ifdef _WINDOWS_VER
        _endthread();
	#else
        pthread_exit(NULL);
	#endif
}

void startVirtualProcessor(PROCESSOR_ONE * p1, void * AdditionOperations)
{
	#ifdef _WINDOWS_VER
        p = p1;
        _beginthread(virtualProcessor, 0, AdditionOperations );
	#else
        int return_code;
        pthread_t thread_id;
        p = p1;
        return_code = pthread_create(&thread_id, NULL, virtualProcessor, AdditionOperations);
        if(return_code)
        {
            printf("ERROR; return code from pthread_create() is %d\n", return_code);
            exit(EXIT_FAILURE);
        }
        //Wait till threads are complete before main continues. Unless we
        //wait we run the risk of executing an exit which will terminate
        //the process and all threads before the threads have completed.
        pthread_join( thread_id, NULL);
	#endif
}

void stopVirtualProcessor()
{
	stop_virtual_processor = true;
}

bool executeProcess(PCBPtr process)
{
	if(p->cpu_bound_process == NULL)
	{
		p->cpu_bound_process = process;
		return true;
	}
	return false;
}

bool interuptRuningProcess()
{
	//check if process is cpu_bound_process
	if(p->cpu_bound_process != NULL)
	{
		interrupt = true;
		return true;
	}
	//no process to interupt
	return false;
}

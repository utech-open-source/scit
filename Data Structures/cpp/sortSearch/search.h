/**
* author: Mark Robinson
* date: 2/13/2014
**/

#ifndef SEARCH_H
#define SEARCH_H

#include"sort.h"

template <class S>
class Search{

private:
	S * original;
	int size;

public:
	Search(S *, int);
	int linear(S);
	int binary(S);
};


template <class S>
Search<S>::Search(S * A, int s)
{
	original = A;
	size = s;
}

/*
* linear - search for a value in array using linear search algorithm
* val value - the value to search for in array
*/
template <class S>
int Search<S>::linear(S value)
{
    int cnt = 0;
    while(value != original[cnt] && cnt < size)
    {
       cnt++;
    }

    if(cnt == size)
        return -1;
    return cnt;
}


/*
* binary - search for a value in array using binary search algorithm
* val value - the value to search for in array
* condition - array must be sorted first
*/
template <class S>
int Search<S>::binary(S value)
{
	int mid, start, end;
	start = 0;
	end  = size-1;

	//order the list before search using binary
	Sort<S>sort(original, size);

	//sort.insertion();
	sort.bubble();

	S * tmp = sort.getArray();

	//check if array is null
	if(tmp != NULL)
		while(start <= end)
		{
			mid = (start+end)/2;

			if(tmp[mid] == value)
			{
			return mid;
			}

			if(value < tmp[mid])
			{
				end = mid - 1;
			}

			if(value > tmp[mid])
			{
				start = mid + 1;
			}
		}

	return -1;
}

#endif
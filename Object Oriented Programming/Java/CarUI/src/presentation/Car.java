package presentation;

public class Car {
	
	private String plateNumber;
	private String make;
	private String model;
	private int year;
	private Engine eng;
	
	Car()//default Constructor
	{
		plateNumber = "";
		make = "";
		model = "";
		year = 1900;
		eng = new Engine();
	}

	Car(String plateNumber, String make, String model, int year, Engine eng)
	{
		this.plateNumber = plateNumber;
		this.make = make;
		this.model = model;
		this.year = year;
		this.eng = eng;
	}
	
	
	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	
	public String getDisplayString() {
		return "PlateNumber:"+this.getPlateNumber()+"\n"
				+ "Model:"+this.getModel()+"\n"
				+ "Make: "+this.getMake()+"\n"
				+ "Year: "+this.getYear()+"\n";
		
	}
	

}

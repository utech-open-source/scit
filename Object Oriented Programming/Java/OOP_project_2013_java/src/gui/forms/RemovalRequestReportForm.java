package gui.forms;

import gui.Resource;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class RemovalRequestReportForm extends ReportForm{

	
	private DateForm dateForm;
	
	/*
	 * Labels For Attributes in the RemovalRequest class
	 */
	private JLabel outcomeLb;
	
	/*
	 * TextField For Attributes in the RemovalRequest class
	 */
	private JComboBox<String> outcomeTf;
	
	public RemovalRequestReportForm(String[] names, Object[][] obj) {
		super(names, obj);
		
		setDateForm(new DateForm());
		
		setOutcomeLb(Label(Resource.outcomeLb));
		setOutcomeTf(ComboBox(Resource.outcomeTf));
		
	}

	/**
	 * @return the dateForm
	 */
	public DateForm getDateForm() {
		return dateForm;
	}

	/**
	 * @param dateForm the dateForm to set
	 */
	public void setDateForm(DateForm dateForm) {
		this.dateForm = dateForm;
	}
	
	/**
	 * @return the outcomeLb
	 */
	public JLabel getOutcomeLb() {
		return outcomeLb;
	}

	/**
	 * @param outcomeLb the outcomeLb to set
	 */
	public void setOutcomeLb(JLabel outcomeLb) {
		this.outcomeLb = outcomeLb;
	}

	/**
	 * @return the outcomeTf
	 */
	public JComboBox<String> getOutcomeTf() {
		return outcomeTf;
	}

	/**
	 * @param outcomeTf the outcomeTf to set
	 */
	public void setOutcomeTf(JComboBox<String> outcomeTf) {
		this.outcomeTf = outcomeTf;
	}

	@Override
	public void addToPanel()
	{
		super.addToPanel();
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.LINE_AXIS));
		panel2.add(Box.createRigidArea(new Dimension(30, 0)));
		panel2.add(getAddressForm().render());
		
		JPanel outcomePanel = new JPanel(new GridLayout(0,1));
		outcomePanel.add(wrap(getOutcomeLb(), getOutcomeTf()));
		outcomePanel.add(Box.createRigidArea(new Dimension(Resource.labelWidth, Resource.labelheight)));
		outcomePanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 30, 50));
		
		panel2.add(outcomePanel);
		panel2.add(getDateForm().render());
		getPanel().add( space(panel2, 10), BorderLayout.SOUTH);
		
		getFormPanel().add(getPanel());
	}
	

}

package gui.forms;

import gui.Resource;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ClientForm extends Form{
	/*
	 * Labels For Attributes in the Client and ClientVisit
	 */
	private JLabel firstNameLb;
	private JLabel lastNameLb;
	private JLabel contactNumLb; 
	private JLabel payTypeLb;
	
	/*
	 * TextField For Attributes in the Client
	 */
	private JTextField firstNameTf;
	private JTextField lastNameTf;
	private JTextField contactNumTf;
	private JComboBox<String> payTypeTf;
	
	public ClientForm()
	{
		super();
		
		setSecTitle(Resource.secClientTitle);
		
		setFirstNameLb(Label(Resource.firstNameLb));
		setLastNameLb(Label(Resource.lastNameLb));
		setContactNumLb(Label(Resource.contactNumLb)); 
		setPayTypeLb(Label(Resource.payTypeLb)); 
		
		/*
		 * TextField For Attributes in the Client
		 */
		setFirstNameTf(TextField(Resource.firstNameTf));
		setLastNameTf(TextField(Resource.lastNameTf));
		setContactNumTf(TextField(Resource.contactNumTf));
		setPayTypeTf(ComboBox(Resource.payTypeTf));
	}

	/**
	 * @return the firstNameLb
	 */
	public JLabel getFirstNameLb() {
		return firstNameLb;
	}

	/**
	 * @param firstNameLb the firstNameLb to set
	 */
	public void setFirstNameLb(JLabel firstNameLb) {
		this.firstNameLb = firstNameLb;
	}

	/**
	 * @return the lastNameLb
	 */
	public JLabel getLastNameLb() {
		return lastNameLb;
	}

	/**
	 * @param lastNameLb the lastNameLb to set
	 */
	public void setLastNameLb(JLabel lastNameLb) {
		this.lastNameLb = lastNameLb;
	}

	/**
	 * @return the contactNumLb
	 */
	public JLabel getContactNumLb() {
		return contactNumLb;
	}

	/**
	 * @param contactNumLb the contactNumLb to set
	 */
	public void setContactNumLb(JLabel contactNumLb) {
		this.contactNumLb = contactNumLb;
	}

	/**
	 * @return the payTypeLb
	 */
	public JLabel getPayTypeLb() {
		return payTypeLb;
	}

	/**
	 * @param payTypeLb the payTypeLb to set
	 */
	public void setPayTypeLb(JLabel payTypeLb) {
		this.payTypeLb = payTypeLb;
	}

	/**
	 * @return the firstNameTf
	 */
	public JTextField getFirstNameTf() {
		return firstNameTf;
	}

	/**
	 * @param firstNameTf the firstNameTf to set
	 */
	public void setFirstNameTf(JTextField firstNameTf) {
		this.firstNameTf = firstNameTf;
	}

	/**
	 * @return the lastNameTf
	 */
	public JTextField getLastNameTf() {
		return lastNameTf;
	}

	/**
	 * @param lastNameTf the lastNameTf to set
	 */
	public void setLastNameTf(JTextField lastNameTf) {
		this.lastNameTf = lastNameTf;
	}

	/**
	 * @return the contactNumTf
	 */
	public JTextField getContactNumTf() {
		return contactNumTf;
	}

	/**
	 * @param contactNumTf the contactNumTf to set
	 */
	public void setContactNumTf(JTextField contactNumTf) {
		this.contactNumTf = contactNumTf;
	}

	/**
	 * @return the payTypeTf
	 */
	public JComboBox<String> getPayTypeTf() {
		return payTypeTf;
	}

	/**
	 * @param payTypeTf the payTypeTf to set
	 */
	public void setPayTypeTf(JComboBox<String> payTypeTf) {
		this.payTypeTf = payTypeTf;
	}

	
	public void addToPanel()
	{
		getFormPanel().add(wrap(getFirstNameLb(),getFirstNameTf()));
		
		getFormPanel().add(wrap(getLastNameLb(),getLastNameTf()));
		
		getFormPanel().add(wrap(getContactNumLb(),getContactNumTf()));
		
		getFormPanel().add(wrap(getPayTypeLb(), getPayTypeTf()));
	}

}

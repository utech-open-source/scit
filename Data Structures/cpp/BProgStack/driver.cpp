#include "stack.h"

#include <iostream>

using namespace std;

void main()
{
	Stack s;

	char B;
	
	cout << "Enter B program eg ()()(()) :" << endl;

	bool failed = false;

	while(true)
	{
		cin >> B;

		if(B == '(')
		{
			s.push(B);
		}
		else
			if(B == ')')
			{
				if(!s.isEmpty())
					s.pop()->getData();
				else{
					cout << "Compilation Error!"<<endl;
					failed = true;
					break;
				}
			}
			else
				if(B == ';')
					break;
	}

	if(s.isEmpty() && failed == false)
		cout << "Program Compiled" << endl;


	system("pause");


}
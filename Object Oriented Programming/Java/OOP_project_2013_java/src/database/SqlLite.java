package database;

import java.sql.*;

import logging.Log;

public class SqlLite {

	private Connection connection;
	private Statement statement;
	private String dbName;
	private boolean commit;
	private boolean active;
	private ResultSet result;
	
	public SqlLite(String dbName, boolean commit)
	{
		this.setDbName(dbName);
		this.setCommit(commit);
		this.setStatement(null);
		this.setActive(false);
		this.setResult(null);
	}
	
	/**
	 * @return the dbName
	 */
	public String getDbName() {
		return dbName;
	}

	/**
	 * @param dbName the dbName to set
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	/**
	 * @return the statement
	 */
	public Statement getStatement() {
		return statement;
	}

	/**
	 * @param statement the statement to set
	 */
	public void setStatement(Statement statement) {
		this.statement = statement;
	}

	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * @param connection the connection to set
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	/**
	 * @return the commit
	 */
	public boolean isCommit() {
		return commit;
	}

	/**
	 * @param commit the commit to set
	 */
	public void setCommit(boolean commit) {
		this.commit = commit;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	
	/**
	 * @return the result
	 */
	public ResultSet getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(ResultSet result) {
		this.result = result;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean connect()
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
			
			connection = DriverManager.getConnection("jdbc:sqlite:"+getDbName()+".db");
			//Database auto commit
			connection.setAutoCommit(isCommit());
			setActive(true);
		}
		catch(Exception e)
		{
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );
		    setActive(false);
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean exist()
	{			
		try{
			select("SELECT * FROM sqlite_sequence ;");
			ResultSet res = getResult();
			res.next();
			res.close();
			closeStatement();
		}
		catch(Exception e)
		{
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param sql
	 * @return
	 */
	public boolean createTable(String sql)
	{
		try {
			setStatement(connection.createStatement());
			getStatement().executeUpdate(sql);
			getStatement().close();
			commit();
		} catch (SQLException e) {
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );
			return false;
		}
		
		//Log.LogInfo(sql);
		
		return true;
	}
	
	/**
	 * 
	 * @param sql
	 * @return
	 */
	public boolean insert(String sql)
	{
		try {
			setStatement(connection.createStatement());
			getStatement().executeUpdate(sql);			
			getStatement().close();
			commit();
			
		} 
		catch (SQLException e) {
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );
			return false;
		}
		Log.LogInfo(sql);
		
		return true;
	}
	
	/**
	 * 
	 * @param sql
	 * @return
	 */
	public void select(String sql)
	{
		try {
			
			setStatement(connection.createStatement());
			
			ResultSet result = getStatement().executeQuery(sql);
			
			setResult(result);
			
		}
		catch(SQLException e)
		{
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );			
		}
		Log.LogInfo(sql);		
	}
	
	/**
	 * 
	 * @param sql
	 * @return
	 */
	public boolean update(String sql)
	{
		try {
			setStatement(connection.createStatement());
			getStatement().executeUpdate(sql);
			getStatement().close();
			commit();
		}
		catch (SQLException e) {
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param sql
	 * @return
	 */
	public boolean delete(String sql)
	{
		return update(sql);
	}
	
	public void commit()
	{
		try {
			//check if auto commit is set to false before running commit
			if(!isCommit())
				getConnection().commit();
		} 
		catch (SQLException e) {
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );			
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean close()
	{
		try {
			//check if connection is active
			if(isActive())
			{
				getConnection().close();
			}
			
		} catch (SQLException e) {
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );			
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param table
	 * @return
	 */
	public int getLastInsert(String table)
	{
		int id = 0;
		
		select("SELECT * FROM sqlite_sequence where name='"+table+"';");
		
		ResultSet res = getResult();
		
		try{
			
			res.next();
			id = res.getInt("seq");
			
			res.close();
			closeStatement();
		}
		catch(SQLException e)
		{
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );			
		}
		
		return id;
	}
	
	/**
	 * use after every select query to close statement
	 */
	public void closeStatement()
	{
		try {
			getStatement().close();
		} catch (SQLException e) {
			Log.LogError( e.getClass().getName() + ": " + e.getMessage() );
		}
	}
	
	
}

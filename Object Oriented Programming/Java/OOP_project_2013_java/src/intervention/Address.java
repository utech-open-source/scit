package intervention;

public class Address {
	
	private String lotNumber;
	private String street;
	private String parish;
	//added for database use
	private int addressId;
	
	
	public Address()
	{
		setLotNumber("");
		setStreet("");
		setParish("");
		setAddressId(0);
	}
	
	public Address(String lotNum, String stre, String paris, int id)
	{
		lotNumber = lotNum;
		street = stre;
		parish = paris;
		addressId = id;
		
	}

	public String getLotNumber() {
		return lotNumber;
	}

	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getParish() {
		return parish;
	}

	public void setParish(String parish) {
		this.parish = parish;
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

}

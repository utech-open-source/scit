#ifndef UTILITY_H
#define UTILITY_H

/**
* author: Mark Robinson
* date: 2/13/2014
**/

#include<iostream>


/*
* printArray - prints a 1d array
* Args
* ref A - array pointer
* val size - the size of the 1d array
*/
template<typename P>
void printArray(P * A, int size)
{
	int cnt = 0;
	while(cnt < size)
	{
		std::cout << A[cnt];
		
		if(cnt < size-1)
			std::cout<<",";
		else
			std::cout<<endl;
		cnt++;
	}
}

/*
* printArray - prints a 2d array
* Args
* ref A - array pointer
* val row - the number of rows in 2d array
* val col - the number of col in 2d array
*/
template<typename P>
void printArray(P * A, int row, int col)
{
	int cnt = 0, cnt2 = 0;
	while(cnt < row)
	{
		printArray<P>(A + cnt, col);

		cnt++;
	}
}

/*
* printRand - prints a random value in array using hashing algorithm
* Args
* ref A - array pointer of type P 
* val size - the size of the array
* val num - the num to calculate value in array
*/
template<typename P>
void printRand(P * A, int size, int num)
{
	std::cout << A[size % num] << std::endl;
}

#endif
package gui.forms;

import gui.Resource;

import javax.swing.JComboBox;
import javax.swing.JLabel;

public class RemovalRequestForm extends BaseForm{
	
	/*
	 * Labels For Attributes in the RemovalRequest class
	 */
	private JLabel outcomeLb;
	
	/*
	 * TextField For Attributes in the RemovalRequest class
	 */
	private JComboBox<String> outcomeTf;
	
	public RemovalRequestForm()
	{
		super();
		
		setOutcomeLb(Label(Resource.outcomeLb));
		setOutcomeTf(ComboBox(Resource.outcomeTf));
	}

	/**
	 * @return the outcomeLb
	 */
	public JLabel getOutcomeLb() {
		return outcomeLb;
	}

	/**
	 * @param outcomeLb the outcomeLb to set
	 */
	public void setOutcomeLb(JLabel outcomeLb) {
		this.outcomeLb = outcomeLb;
	}

	/**
	 * @return the outcomeTf
	 */
	public JComboBox<String> getOutcomeTf() {
		return outcomeTf;
	}

	/**
	 * @param outcomeTf the outcomeTf to set
	 */
	public void setOutcomeTf(JComboBox<String> outcomeTf) {
		this.outcomeTf = outcomeTf;
	}
	
	/**
	 * Override addToPannel
	 */
	
	public void addToPanel()
	{
		super.addToPanel();
		getFormPanel().add(wrap(getOutcomeLb(), getOutcomeTf()));
		//getFormPanel().add(space(wrap(getButton()), 10));
	}

}

/*This program will display the addition of three numbers entered by the user */
//Author: Mark Robinson

#include <stdio.h>
#include <conio.h>

void main (void)
{
  // variable declaration
  int number1, number2, number3;
  int total;
  
  // input from user
  printf ("\nPlease Enter Number 1:\t");
  scanf("%d", &number1);

  printf ("\nPlease Enter Number 2:\t");
  scanf("%d", &number2);

  printf ("\nPlease Enter Number 3:\t");
  scanf("%d", &number3);
  
  // processing the input
  total = number1 + number2 + number3;
  
  // output to user
  printf ("\n\nTotal Is = %d.\n", total);
  
  // wait on the computer to end the program
  getch();
}
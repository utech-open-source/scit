#ifndef UTILITY_H
#define UTILITY_H

#include <string>
#include <windows.h>
#include <stdlib.h>
#include <sstream>
#include "Resource.h"

using namespace std;

#ifndef UNICODE  
  typedef string String;
#else
  typedef wstring String;
#endif




#define SYSTEM Utility::instance()

class Utility{

private:
	static Utility sys;
	HWND hwndMain;
	bool activeOperation;

public:

	Utility()
	{
		hwndMain = NULL;
		activeOperation = false;
	}

	void flush(int r_id)
	{
		SetText(r_id, "");
	}


	void display(int r_id, string msg, int level)
	{
		//TCHAR buff[20 * 1024];

		if(hwndMain != NULL)
		{

			//GetWindowText(hwndStatic, buff, 1024);

			//String initial = buff;

			//msg = string(initial.begin(), initial.end()) + msg + "\n";

			msg = "\r\n"+msg+ "\r\n";

			//SetTextColor((HDC)hwndStatic, RGB(255,0,0)); //set text color

			//HDC hdc = GetDC(hwndStatic);

			//SelectObject(hdc, GetStockObject(DC_PEN));
			//SetDCPenColor(hdc, RGB(250, 20, 0));
			//Rectangle(hdc, 10, 10, 200, 200);

			HWND hwnd = GetDlgItem(hwndMain, r_id);

			AppendText(hwnd, toTchar(msg) );

			//SetWindowText(hwndStatic, toTchar(msg) );
		}
	}

	void setHwndMain(HWND hwnd)
	{
		this->hwndMain = hwnd;
	}

	void setActiveOperation(bool active)
	{
		activeOperation = active;
	}

	bool getActiveOperation()
	{
		return activeOperation;
	}

	//utility functions

	void AppendText(HWND hEditWnd, LPCTSTR Text)
	{
		int idx = GetWindowTextLength(hEditWnd);
		SendMessage(hEditWnd, EM_SETSEL, (WPARAM)idx, (LPARAM)idx);
		SendMessage(hEditWnd, EM_REPLACESEL, 0, (LPARAM)Text);
	}

	//overloaded functions for string and wstring
	template <typename T>
	TCHAR * _toTchar(T data)
	{
		TCHAR * final = new TCHAR[data.size()+1];
		final[data.size()] = 0;
		copy(data.begin(),data.end(),final);

		return final;
	}

	TCHAR * toTchar(string data)
	{
		return _toTchar<string>(data);
	}

	TCHAR * toTchar(wstring data)
	{
		return _toTchar<wstring>(data);
	}

	template <class T>
	string toString (const T& t)
	{
		std::stringstream ss;
		ss << t;
		return ss.str();
	}

	static Utility * instance() 
	{	
		return &sys; 
	}

	//todo: fix
	void MakeTrans(HWND hwnd)
	{
		HDC hdc = GetDC(hwnd);
		SetTextColor(hdc, RGB(0, 0, 0));
		SetBkMode(hdc, TRANSPARENT);
	}

	String GetText(int r_id)
	{
		TCHAR buff[1024];

		HWND hwnd = GetDlgItem(hwndMain, r_id);

		GetWindowText(hwnd, buff, 1024);

		return String(buff);
	}

	//overloaded functions for string and wstring
	template <typename T>
	void _SetText(int r_id, T data)
	{
		HWND hwnd = GetDlgItem(hwndMain, r_id);

		SetWindowText(hwnd, toTchar(data) );
	}

	void SetText(int r_id, string data)
	{
		_SetText<string>(r_id, data);
	}

	void SetText(int r_id, wstring data)
	{
		_SetText<wstring>(r_id, data);
	}
};

#endif
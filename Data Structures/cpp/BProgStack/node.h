#ifndef NODE_H
#define NODE_H

#include <iostream>

using namespace std;

class Node{
private:
	char data;
	Node * next;

public:
	Node();
	Node(char);
	char getData() const;
	Node * getNext() const;
	void setData(char);
	void setNext(Node*);
};

#endif
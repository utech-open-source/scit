/**
* Generic ListNode
* Author : Mark Robinson
* Date : April 4, 2014 
**/

#ifndef LISTNODE_H
#define LISTNODE_H

template <typename LISTTYPE> class List;

template <typename TYPE>
class ListNode{

	friend class List<TYPE>;

private:
	TYPE data;
	ListNode<TYPE> * nextPtr;
	ListNode<TYPE> * prevPtr;

public:
	ListNode(const TYPE &);
	TYPE getData()const;
	void setData(const TYPE &);
};

template <typename TYPE>
ListNode<TYPE>::ListNode(const TYPE & node):
data(node), nextPtr(0), prevPtr(0)
{
}

template <typename TYPE>
TYPE ListNode<TYPE>::getData()const
{
	return this->data;
}

template <typename TYPE>
void ListNode<TYPE>::setData(const TYPE & data)
{
	this->data = data;
}

#endif
#ifndef PROCESS_CONTROL_BLOCK_H
#define PROCESS_CONTROL_BLOCK_H

#include "output.h"
#include <time.h>

#define MAX_PROCESS_ID_AVAILABLE 30

/*
* Process control block(PCB)
*/
typedef struct process_control_block PCB, * PCBPtr, ** PCBPtrPtr;

struct process_control_block{
	int process_id; // Unique identifer for process
	int process_type; //type of process: 1-system, 2-interative, 3-Batch
	int priority; //importance of process 1-High,2-Medium, 3-Low
	int burst_time; //cpu time needed to complete task
	int arrival_time; //time the process was created
	int start_time; //time the process first gets the CPU
	int end_time; //time the process ends
	int remaining_burst_time; //the remaining burst time
};

/*
** generate random number
** offset: used to specify where the rand number starts
**		   gerating from
** limit : max a random number generates to
*/
int generateRand(int offset, int limit);

/*
** generate process static control block
** initialize the values of control block with parameters
*/
PCB generateProcessControlBlockStatic(int id, int type, int priority, int burst_time, int arrival_time);

/*
** generate process control block
** initialize the values of control block randomly
** return a PCB type
*/
PCB generateProcessControlBlock();

/*
** generate array of process control blocks PCB(s)
** num - the number of process control blocks to generate
*/
PCB * generateProcessControlBlocks(int num);

/*
** display a process control block
*/
void displayProcessControlBlock(PCB block);

/*
** display a list of process control blocks
** blocks - the array of process control blocks
** num - the number of process control blocks to display
*/
void displayProcessControlBlocks(PCBPtr blocks, int num);

/*
** Swap two array items
*/
void swap(PCBPtr item1, PCBPtr item2);

/*
** Sorts process control block array
*/
void sortProcessControlBlock(PCBPtr A, int pass);

#endif


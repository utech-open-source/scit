package presentation;
	
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CarUI extends JFrame
{
	//Car attributes
	private Car car;
	
	//labels which shows information for the GUI
	private JLabel plateNumberLb1 = new JLabel("Plate Number:");
	private JLabel makeLb1 = new JLabel("Make:");
	private JLabel modelLb1 = new JLabel("Model");
	private JLabel yearLb1 = new JLabel("Year:");
	
	//text fields which accepts input
	private JTextField plateNumberF1d = new JTextField(20);
	private JTextField makeF1d = new JTextField(30);
	private JTextField modelF1d = new JTextField(30);
	private JTextField yearF1d = new JTextField(20);
	
	//BUttons  (executes an action upon clicking)
	private JButton saveBtn = new JButton("Save");
	private JButton clearBtn = new JButton("Clear");
	
	//JPannels
	private JPanel panel1;
	private JPanel panel2;
	
	public CarUI() //Default Constructor
	{		
		//calls super class to display title on window
		super("Car Information");
		
		//initialize the car object
		this.car = new Car();
		
		//layout of the AddBookUI look & feel below
		
		//labels & text fields are added in the grids
		//from left to right, a 4 row by 2 column grid
		panel1 = new JPanel(new GridLayout(4,2)); //panel one
		
		//flowlayout adds stuff like buttons from
		//left to right side by side
		panel2 = new JPanel(new GridLayout());//pannel 2
		
		//panel one setup with test boxes and labels
		
		//adds label row 1, column 1
		//adds text field row 1, column 2
		panel1.add(plateNumberLb1);
		panel1.add(plateNumberF1d);
		
		//adds label row 2, column1
		//adds text field row 2, column 2
		panel1.add(makeLb1);
		panel1.add(makeF1d);
		
		//adds label row 3, column 2
		//adds text field row 3, column 2
		panel1.add(modelLb1);
		panel1.add(modelF1d);
		
		//adds label row 4, column 1
		//adds text field row 4, column 2
		panel1.add(yearLb1);
		panel1.add(yearF1d);
		
		
		//panel 2 set up with buttons
		panel2.add(saveBtn);
		panel2.add(clearBtn);
		
		//set up of main panel layout
		//the content pane is the main container for layout
		this.getContentPane().add(panel1, BorderLayout.CENTER);
		this.getContentPane().add(panel2, BorderLayout.SOUTH);
		//end of layout set up
		
		
		//set default values for text input fields
		makeF1d.setText(car.getMake());
		modelF1d.setText(car.getModel());
		yearF1d.setText(""+car.getYear());
		
		//Event handlers (for enabling buttons to respond to a click)
		saveBtn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				
				//get the input field attributes
				car.setMake(makeF1d.getText());
				car.setModel(modelF1d.getText());
				car.setPlateNumber(plateNumberF1d.getText());
				car.setYear(Integer.parseInt(yearF1d.getText()));
				
				//get the form inputs
				JOptionPane.showMessageDialog(null, "Clicked Save Button: \n you Entered: \n"+car.getDisplayString(), "Message", JOptionPane.INFORMATION_MESSAGE);
				
				//example
				//JOptionPane.showMessageDialog(null, "Clicked Save Button", "Message", JOptionPane.INFORMATION_MESSAGE);
				
				
			}//end cancel button class
		});
		
		setVisible(true);
		
	} // end constructor
}
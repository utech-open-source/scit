# How to contribute

Here are some important resources:

  * Mailing list: Join our [developer list](http://#)


## Submitting changes

Please send a [GitHub Pull Request to utech-open-source](https://gitlab.com/utech-open-source/scit/pull/new/master) with a clear list of what you've done. Please follow our coding conventions (below) and make sure all of your commits are atomic (one change per commit).

Always write a clear log message for your commits. One-line messages are fine for small changes, but bigger changes should look like this:

    $ git commit -m "A brief summary of the commit
    > 
    > A paragraph describing what changed and its impact."

## Coding conventions

Start reading our code and you'll get the hang of it. We optimize for readability:

  * We indent using two spaces (soft tabs)

Thanks,
Mark Robinson
//CastleAmusement
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


#define Action_s "Action"
#define Comedy_s "Comedy"
#define Romance_s "Romance"
#define Active_s "Active"
#define Inactive_s "Inactive"
#define PName "CastleAmusement"
#define MovieRatings_s {"13", "16", "MA"}
#define Worker_s "Worker"
#define Customer_s "Customer"

//define for files
#define LoginFile "login.dat"
#define CustomerFile "customer.dat"
#define MovieFile "movie.dat"
#define ScheduleFile "movieschedule � dd-mm-yyyy.dat"
#define RecordsFile "records.dat"
#define TicketsFile "tickets.dat"

//date structure
typedef struct Date Date;
struct Date
{
	char month[3]; //remember null character
	char day[3]; //remember null character
	char year[5]; //remember null character
};

//full name, date of birth, age [calculated using date of birth], email
//address, contact number, password, and date registered.
typedef struct User User;
struct User{
	int user_id; //sequential user id
	char first_name[30];
	char last_name[30];
	Date date_of_birth;
	int age;
	char email_address[30];
	char contact_number[10];
	Date date_registered;
};

enum Category{Worker=0, Customer};
//login structure
typedef struct UserLogin UserLogin;
struct UserLogin{
	int user_id; //sequential user id
	char password[30];
	int category;
};

//movie code, title, PG rating [13, 16, MA], duration in minutes,director of the movie, 
//type of movie [action, comedy, romance], status [active, inactive].
enum MovieType{Action=1, Comedy, Romance};
enum MovieStatus{Active=1, Inactive};
typedef struct Movie Movie;
struct Movie{
	int id;
	char movie_code[30];
	char title[30];
	int pg_rating;
	int duration;
	char director[30];
	int type;
	int status;
};

//the scheduling struct
typedef struct Schedule Schedule;
struct Schedule{
	int id;
	int movie_id;
	int time;
	Date date;
	double price;
};

typedef struct Ticket Ticket;
struct Ticket{
	int id;
	int schedule_id;
	double payment;
};

//records structure to store the size of the files
enum Records{USER=0, USER_LOGIN, MOVIE, SCHEDULE, TICKET};
typedef struct Record Record;
struct Record{
	int id;
	int size;
	int last;
};

void CustomerMenu(User user);
void WorkerMenu(User user);
void Login();
int Register();
void AddMovie();
void UpdateMovie();
void MovieSchedule();
void BuyTicket();
void GenerateMovieTicket(Schedule schedule, Movie movie, double payment);
//File Operations
//initialize
void InitializeRecord();
//save
int SaveUser(User user);
void SaveUserLogin(UserLogin userLogin);
void SaveMovie(Movie movie);
void SaveSchedule(Schedule schedule);
void SaveTicket(Ticket ticket);
//update file functions
void UpdateUser(User user);
void UpdateUserLogin(UserLogin userLogin);
void UpdateMovieFile(Movie movie);
void UpdateSchedule(Schedule schedule);
void UpdateTicket(Ticket ticket);
void UpdateRecord(Record record);
//read file functions
User ReadUser(int user_id);
UserLogin ReadUserLogin(int user_id);
Movie ReadMovie(int movie_code);
Ticket ReadTicket(int ticket_id);
Schedule ReadSchedule(int id);
//Schedule ReadTicket(int id);
Record ReadRecord(int id);
//clear file functions
void ClearRecord(int type, char * fileName);
//get the current date
Date CurrentDate();
//Authenticate a user
int AuthUser(UserLogin userLogin);
//display functions
//void DisplayDate(Date date);
void DisplayCategory(int cat);
void DisplayUser(User user);
void DisplayMovie(Movie movie);
void DisplaySchedule(Schedule schedule, Movie movie);
void DisplayTicket(Ticket ticket);
void Art();

void main()
{
	srand(time(NULL));
	InitializeRecord();
	while(1)
	{
		Login();
	}
	system("pause");
}

void CustomerMenu(User user)
{
	char choice;

	do
	{
		system("cls");
		Art();
		printf("Welcome %s %s Register Date %s\n\n", user.first_name, user.last_name, user.date_registered.year);
		printf("Customer Menu\n");
		printf("1. Buy Ticket\n");
		printf("2. Exit\n");

		fflush(stdin);
		scanf("%c", &choice);

		switch(choice)
		{
		case '1':
			BuyTicket();
			system("pause");
			break;
		case '2': 
			system("pause");
			system("cls");
			Login();
			break;
		}
		system("cls");
	}
	while(1);


}

void WorkerMenu(User user)
{
	char choice;

	do
	{
		system("cls");
		Art();
		printf("Welcome %s %s Register Date %s\n\n", user.first_name, user.last_name, user.date_registered.year);
		printf("Worker Menu\n");
		printf("1. Add Movie\n");
		printf("2. Update Movie\n");
		printf("3. Movie Schedule\n");
		printf("4. Buy Ticket\n");
		printf("5. Exit\n");

		fflush(stdin);
		scanf("%c", &choice);

		switch(choice)
		{
		case '1': 
			AddMovie();
			system("pause");
			break;
		case '2': 
			UpdateMovie();
			system("pause");
			break;
		case '3': 
			MovieSchedule();
			system("pause");
			break;
		case '4':
			BuyTicket();
			break;
		case '5': 
			system("pause");
			system("cls");
			Login();
			break;
		}
	}
	while(1);
}

void Login()
{
	char choice;
	UserLogin userLogin;
	system("cls");
	Art();
	printf("\nLogin Screen\n");
	printf("\nEnter User-id[number]: ");
	scanf("%d", &userLogin.user_id);
	userLogin = ReadUserLogin(userLogin.user_id-1);
	if(userLogin.user_id != 0)
	{
		if(!AuthUser(userLogin))
		{
			printf("failed to authenticate user\n");
			system("pause");
			exit(0);
		}

		switch(userLogin.category)
		{
		case Customer:
			CustomerMenu(ReadUser(userLogin.user_id-1));
			break;
		case Worker:
			WorkerMenu(ReadUser(userLogin.user_id-1));
			break;
		}
	}
	else
	{
		printf("No account found. Would you like to register (Y/N)?\n");
		fflush(stdin);
		scanf("%c", &choice);
		if( choice == 'Y' || choice == 'y')
		{
			Register();
			system("pause");
		}
	}
}

int Register()
{
	User user = {0, "", "", {"", "", ""} , 0, "", "", {"", "", ""}};
	UserLogin userLogin = {0, "", 0};

	printf("\nREGISTRATION\n");

	printf("First Name: ");
	scanf("%s", user.first_name);

	printf("Last Name: ");
	scanf("%s", user.last_name);

	printf("Date of Birth\n");
	do
	{
		printf("Enter a valid Day dd[1-31]:");
		scanf("%s", user.date_of_birth.day);
	}
	while(atoi(user.date_of_birth.day) < 1 || atoi(user.date_of_birth.day) > 31);

	do
	{
		printf("Enter a valid Month mm[1-12]:");
		scanf("%s", user.date_of_birth.month);
	}
	while(atoi(user.date_of_birth.month) < 1 || atoi(user.date_of_birth.month) > 12);

	//get the current date and sets the user registered date
	user.date_registered = CurrentDate();

	do
	{
		printf("Enter a valid Year yyyy[<now]:");
		scanf("%s", user.date_of_birth.year);
		user.age = atoi(user.date_registered.year) - atoi(user.date_of_birth.year);
	}
	while(atoi(user.date_of_birth.year) > atoi(user.date_registered.year));

	printf("Email:");
	scanf("%s", user.email_address);

	printf("Phone Number:");
	scanf("%s", user.contact_number);

	userLogin.category = rand()%2;

	//get user password
	printf("\nPassword:");
	scanf("%s", userLogin.password);

	//save user and get id
	userLogin.user_id = SaveUser(user);

	//save user login information
	SaveUserLogin(userLogin);

	printf("Your User_id: %d\n", userLogin.user_id);

	DisplayCategory(userLogin.category);

	return 1;
}

void AddMovie()
{
	Movie movie = {0, "", "", 0, 0, "", 0, 0};

	printf("ADD MOVIE\n");

	printf("Movie Code: ");
	scanf("%s", movie.movie_code);	 

	printf("Movie Title[no spaces in title]: ");
	scanf("%s", movie.title);

	printf("Movie Rating: \n");
	do
	{
		fflush(stdin);
		printf("Press 1 for PG 13\nPress 2 for PG 16\nPress 3 for MA\n:");
		scanf("%d", &movie.pg_rating);
	}
	while(movie.pg_rating < 1 || movie.pg_rating > 3);

	printf("Duration of Movie in minutes: ");

	scanf("%d", &movie.duration);

	printf("Director of Movie[no spaces in title]: ");
	scanf("%s", movie.director);

	do
	{
		fflush(stdin);
		printf("\nMovie Status\n Press 1 for Active\n Press 2 for Inactive\n: ");
		scanf("%d", &movie.status );
	}
	while(movie.status < 1 || movie.status > 2);

	do
	{
		fflush(stdin);
		printf("\nMovie type\n Press 1 for Romance\n Press 2 for Action\n Press 3 for Comedy\n:");
		scanf("%d", &movie.type);
	}
	while(movie.type < 1 || movie.type > 3);

	DisplayMovie(movie);
	SaveMovie(movie);
}

void UpdateMovie()
{
	int i = 0;
	Record record = ReadRecord(MOVIE);
	Movie movie;
	char code[30];

	printf("Enter movie code: ");
	scanf("%s", code);

	for(i = 0; i < record.size; i++)
	{
		movie = ReadMovie(i);
		if( strcmp(movie.movie_code, code) == 0)
		{
			DisplayMovie(movie);
			break;
		}
	}

	if( strcmp(movie.movie_code,"") == 0)
	{
		printf("Movie with code %s not found!\n", code);
		return;
	}

	do
	{
		fflush(stdin);
		printf("\nMovie Status\n Press 1 for Active\n Press 2 for Inactive\n: ");
		scanf("%d", &movie.status );
	}
	while(movie.status < 1 || movie.status > 2);

	UpdateMovieFile(movie);
}

void MovieSchedule()
{
	int i = 0, e = 0;
	Record record = ReadRecord(MOVIE), sch_record = ReadRecord(SCHEDULE);
	Schedule schedule;

	if(record.size == 0)
		printf("No Movie Record Available!\n");

	//clear the schedule file
	ClearRecord(SCHEDULE, ScheduleFile);
	//clear the ticket file
	ClearRecord(TICKET, TicketsFile);

	while(i < record.size)
	{
		Movie movie = ReadMovie(i);
		if(movie.status == Active)
		{
			//DisplayMovie(movie);
			schedule.movie_id = movie.id;
			schedule.time = 11 + (e * 3);
			//add the current date
			schedule.date = CurrentDate();
			//add a random price
			schedule.price = 500+rand()%1000;
			SaveSchedule(schedule);
			//end when schedule time reaches 12am midnight or 23:00 hr
			if(schedule.time == 23)
				break;

			e++;
		}
		i++;
	}
	printf("Movie Schedule Generated!\n");
}

void BuyTicket()
{
	Schedule schedule;
	Record record = ReadRecord(SCHEDULE), ticRecrod = ReadRecord(TICKET);
	Movie movie;
	int i; 
	double payment;
	char choice;
	char code[30];

	system("cls");
	Art();
	printf("\nMovie Schedule\n");

	for(i = 0; i < record.size; i++)
	{
		schedule = ReadSchedule(i);
		movie = ReadMovie(schedule.movie_id-1);
		DisplaySchedule(schedule, movie);
	}

	if(record.size == 0)
	{
		printf("Unable to sell tickets at this time try again later.\n");
		system("pause");
		return;
	}

	if(ticRecrod.size == 100)
	{
		printf("Unable to sell tickets at this time try again later.\n");
		system("pause");
		return;
	}

	printf("Enter movie code to buy ticket: ");
	scanf("%s", code);

	for(i = 0; i < record.size; i++)
	{
		movie = ReadMovie(i);
		if( strcmp(movie.movie_code, code) == 0)
		{
			break;
		}
	}

	printf("Would you like to buy ticket (Y/N)?: ");
	fflush(stdin);
	scanf("%c", &choice);
	if(choice == 'n' || choice == 'N')
		return;

	do
	{
		printf("Enter valid payment: ");
		scanf("%lf", &payment);
	}
	while(payment < schedule.price);

	printf("\nReciept\n");
	printf("\n-----------------------------------\n");
	printf("Price: %lf\n", schedule.price);
	printf("Total Payment: %lf\n", payment);
	printf("Change: %lf\n", payment - schedule.price );
	printf("\n-----------------------------------\n");

	GenerateMovieTicket(schedule, movie, payment );

	system("pause");
}

void GenerateMovieTicket(Schedule schedule, Movie movie, double payment)
{
	Ticket ticket;
	ticket.payment = payment;
	ticket.schedule_id = schedule.id;
	DisplaySchedule(schedule, movie);
	SaveTicket(ticket);
}

//File Operations
//initialize
void InitializeRecord()
{
	int i;
	const int size = 5;
	Record record = {0, 0, 0};
	char * files[] = {MovieFile, CustomerFile, LoginFile, TicketsFile, ScheduleFile};
	FILE * file = fopen(RecordsFile, "r+b");
	if(file == NULL && (file = fopen(RecordsFile, "w+b")) != NULL )
	{
		for(i = 0; i < size; i++)
		{
			record.id = i;
			fseek(file, sizeof(Record) * record.id, SEEK_SET);
			fwrite(&record, sizeof(Record), 1, file);
		}
		fclose(file);

		for(i = 0; i < size; i++)
		{
			file = fopen(files[i], "w+b");
			if(file == NULL)
			{
				printf("Major Error Occured!\n");
				system("pause");
				exit(0);
			}
			fclose(file);
		}
	}
}
//save
int SaveUser(User user)
{
	Record record = ReadRecord(USER);
	user.user_id = record.size + 1;
	//increase record size
	record.size++;
	UpdateUser(user);
	UpdateRecord(record);
	return user.user_id;
}
void SaveUserLogin(UserLogin userLogin)
{
	Record record = ReadRecord(USER_LOGIN);
	//increase record size
	record.size ++;
	UpdateUserLogin(userLogin);
	UpdateRecord(record);
}
void SaveMovie(Movie movie)
{
	Record record = ReadRecord(MOVIE);
	movie.id = record.size + 1;
	record.size++;
	UpdateMovieFile(movie);
	UpdateRecord(record);
}
void SaveTicket(Ticket ticket)
{
	Record record = ReadRecord(TICKET);
	ticket.id = record.size + 1;
	record.size++;
	UpdateTicket(ticket);
	UpdateRecord(record);
}
void SaveSchedule(Schedule schedule)
{
	Record record = ReadRecord(SCHEDULE);
	schedule.id = record.size+1;
	//increase record size
	record.size++;
	UpdateSchedule(schedule);
	UpdateRecord(record);
}

//update
void UpdateUser(User user)
{
	FILE * file = fopen(CustomerFile, "r+b");
	if(file != NULL)
	{
		fseek(file, sizeof(User)*(user.user_id-1), SEEK_SET);
		if(!feof(file))
		{
			fwrite(&user, sizeof(User), 1, file);
		}
		fclose(file);
	}
	else
		printf("file failed to open!\n");
}

void UpdateUserLogin(UserLogin userLogin)
{
	FILE * file = fopen(LoginFile, "r+b");
	if(file != NULL)
	{
		fseek(file, sizeof(UserLogin)*(userLogin.user_id-1), SEEK_SET);
		if(!feof(file))
		{
			fwrite(&userLogin, sizeof(UserLogin), 1, file);
		}
		fclose(file);
	}
	else
		printf("file failed to open!\n");
}

void UpdateMovieFile(Movie movie)
{
	FILE * file = fopen(MovieFile, "r+b");
	if(file != NULL)
	{
		fseek(file, sizeof(Movie)*(movie.id-1), SEEK_SET);
		if(!feof(file))
		{
			fwrite(&movie, sizeof(Movie), 1, file);
		}
		fclose(file);
	}
	else
		printf("file failed to open!\n");
}

void UpdateSchedule(Schedule schedule)
{
	FILE * file = fopen(ScheduleFile, "r+b");
	if(file != NULL)
	{
		fseek(file, sizeof(Schedule)*(schedule.id-1), SEEK_SET);
		if(!feof(file))
		{
			fwrite(&schedule, sizeof(Schedule), 1, file);
		}
		fclose(file);
	}
	else
		printf("file failed to open!\n");
}

void UpdateTicket(Ticket ticket)
{
	FILE * file = fopen(TicketsFile, "r+b");
	if(file != NULL)
	{
		fseek(file, sizeof(Schedule)*(ticket.id-1), SEEK_SET);
		if(!feof(file))
		{
			fwrite(&ticket, sizeof(Schedule), 1, file);
		}
		fclose(file);
	}
	else
		printf("file failed to open!\n");
}

void UpdateRecord(Record record)
{
	FILE * file = fopen(RecordsFile, "r+b");
	if(file != NULL)
	{
		fseek(file, sizeof(Record)*record.id, SEEK_SET);
		if(!feof(file))
		{
			fwrite(&record, sizeof(Record), 1, file);
		}
		fclose(file);
	}
	else
		printf("file failed to open!\n");
}

//read
User ReadUser(int user_id)
{
	User user = {0, "", "", {"", "", ""} , 0, "", "", {"", "", ""}};
	FILE * file = fopen(CustomerFile, "r+b");
	if(file != NULL)
	{
		fseek(file, sizeof(User)*user_id, SEEK_SET);
		if(!feof(file))
		{
			fread(&user, sizeof(User), 1, file);
		}
		fclose(file);
	}
	else
		printf("file failed to open!\n");

	return user;
}

UserLogin ReadUserLogin(int user_id)
{
	UserLogin user = {0, "", 0};
	FILE * file = fopen(LoginFile, "r+b");
	if(file != NULL)
	{
		fseek(file, sizeof(UserLogin)*user_id, SEEK_SET);
		if(!feof(file))
		{
			fread(&user, sizeof(UserLogin), 1, file);
		}
		fclose(file);
	}
	else
		printf("file failed to open!\n");

	return user;
}

Movie ReadMovie(int id)
{
	Movie movie = {0, "", "", 0, 0, "", 0, 0};
	FILE * file = fopen(MovieFile, "r+b");
	if(file != NULL)
	{
		fseek(file, sizeof(Movie)*id, SEEK_SET);
		if(!feof(file))
		{
			fread(&movie, sizeof(Movie), 1, file);
		}
		fclose(file);
	}
	else
		printf("file failed to open!\n");

	return movie;
}

Schedule ReadSchedule(int id)
{
	Schedule schedule = {0, 0, 0, {"", "", ""},  0};
	FILE * file = fopen(ScheduleFile, "r+b");
	if(file != NULL)
	{
		fseek(file, sizeof(Schedule)*id, SEEK_SET);
		if(!feof(file))
		{
			fread(&schedule, sizeof(Schedule), 1, file);
		}
		fclose(file);
	}
	else
		printf("file failed to open!\n");

	return schedule;
}

Record ReadRecord(int id)
{
	Record record = {0, 0, 0};
	FILE * file = fopen(RecordsFile, "r+b");
	if(file != NULL)
	{
		fseek(file, sizeof(Record)*id, SEEK_SET);
		if(!feof(file))
		{
			fread(&record, sizeof(Record), 1, file);
		}
		fclose(file);
	}
	else
		printf("file failed to open!\n");

	return record;
}

Ticket ReadTicket(int ticket_id)
{
	Ticket ticket = {0, 0, 0};
	FILE * file = fopen(TicketsFile, "r+b");
	if(file != NULL)
	{
		fseek(file, sizeof(Ticket)*ticket_id, SEEK_SET);
		if(!feof(file))
		{
			fread(&ticket, sizeof(Ticket), 1, file);
		}
		fclose(file);
	}
	else
		printf("file failed to open!\n");

	return ticket;
}

//clear the schedule file
void ClearRecord(int type, char * fileName)
{
	Record record = ReadRecord(type);
	FILE * file = fopen(fileName, "w+b");
	if(file != NULL)
	{
		fclose(file);
	}
	record.size = 0;
	UpdateRecord(record);
}

//Authenticate User
int AuthUser(UserLogin userLogin)
{
	char password[30];
	int i = 0;
	do{
		printf("\nPassword: ");
		scanf("%s", password);
		if(strcmp(password, userLogin.password ) == 0)
		{
			return 1;
		}
		else
		{
			printf("Incorrect attemp %d of 3 try again!\n", i+1);
		}
		i++;
	}
	while(i < 3);
	return 0;
}

//gets current date
Date CurrentDate()
{
	Date date = {"", "", ""};

	time_t now = time(NULL);

	struct tm *t = localtime(&now);

	sprintf(date.month, "%d", t->tm_mon+1);
	sprintf(date.day, "%d", t->tm_mday);
	sprintf(date.year, "%d", t->tm_year+1900);

	return date;
}

//display functions
//void DisplayDate(Date date){}

void DisplayCategory(int cat)
{
	if(cat == Worker)
		printf("User Category: Worker\n");
	else
		printf("User Category: Customer\n");
}

void DisplayUser(User user){}

void DisplayMovie(Movie movie)
{
	char * ratings[] = MovieRatings_s;
	//printf("Movie %d\n", movie.id);
	printf("Code: %s\n", movie.movie_code);
	printf("Title: %s\n", movie.title);
	printf("Type: ");
	switch(movie.type)
	{
	case Action:
		printf("%s\n", Action_s);
		break;
	case Comedy:
		printf("%s\n", Comedy_s);
		break;
	case Romance:
		printf("%s\n", Romance_s);
		break;
	default:
		printf("not available\n");
		break;
	}
	printf("Director: %s\n", movie.director);
	printf("PG Rating: ");
	switch(movie.pg_rating)
	{
	case 1:
		printf("%s\n", ratings[1]);
		break;
	case 2:
		printf("%s\n", ratings[2]);
		break;
	case 3:
		printf("%s\n", ratings[3]);
		break;
	default:
		printf("not available\n");
		break;
	}
	printf("Status: ");
	if(movie.status == Active)
		printf("%s\n", Active_s);
	else
		printf("%s\n", Inactive_s);
}

void DisplaySchedule(Schedule schedule, Movie movie)
{
	printf("----------------------------\n");
	DisplayMovie(movie);
	printf("Date: %s %s %s\n", schedule.date.month, schedule.date.day, schedule.date.year);
	printf("Time: %d:00\n", schedule.time);
	printf("Price: %lf\n", schedule.price);
	printf("----------------------------\n");
}

void Art()
{
	printf("/----      //\\       //----\\    ---||---      //\\             \n");
	printf("||        //  \\      ||            ||        //  \\            \n");
	printf("||       //----\\     ||----\\       ||       //----\\           \n");
	printf("||      //------\\          ||      ||  LE  //------\\  MUSMENT \n");
	printf("\----   //        \\   ------//      ||     //        \\         \n");
}
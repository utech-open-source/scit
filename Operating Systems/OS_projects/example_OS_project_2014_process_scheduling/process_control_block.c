#include "process_control_block.h"

//Process Id array to ensure it is unique
int processIds[MAX_PROCESS_ID_AVAILABLE] = {0};

/*
** generate process static control block
** initialize the values of control block with parameters
*/
PCB generateProcessControlBlockStatic(int id, int type, int priority, int burst_time, int arrival_time)
{
	PCB pcb;
	//check if processid generated is available
	pcb.process_id = id;
	pcb.process_type = type;
	pcb.priority = priority;
	pcb.burst_time = burst_time;
	pcb.arrival_time = arrival_time;
	pcb.start_time = -1;
	pcb.end_time = 0;
	pcb.remaining_burst_time = pcb.burst_time;
	return pcb;
}

/*
** generate process control block
** initialize the values of control block randomly
*/
PCB generateProcessControlBlock()
{
	PCB pcb;
	//check if processid generated is available
	do
	{
		pcb.process_id = generateRand(0, 29);
	}while( processIds[pcb.process_id] != 0);
	processIds[pcb.process_id] = 1; //set processIds to 1 after use
	pcb.process_type = generateRand(1, 3);
	pcb.priority = generateRand(1,3);
	pcb.burst_time = generateRand(1,10);
	pcb.arrival_time = generateRand(0,29);
	pcb.start_time = -1;
	pcb.end_time = 0;
	pcb.remaining_burst_time = pcb.burst_time;
	return pcb;
}

/*
** generate array of process control blocks PCB(s)
** num - the number of process control blocks to generate
*/
PCB * generateProcessControlBlocks(int num)
{
	int i;
	PCB * blocks = (PCB*)malloc(sizeof(PCB) * num);
	//seed random number
	srand(time(NULL));
	for(i=0; i<num; i++) *(blocks + i) = generateProcessControlBlock();
	return blocks;
}

/*
** display a process control block
*/
void displayProcessControlBlock(PCB block)
{
    string data = malloc(32);

    sprintf(data, "%.3d\t", block.process_id);
    output(data);

	sprintf(data, "%.3d\t", block.process_type);
    output(data);

	sprintf(data, "%.3d\t", block.priority);
    output(data);

	sprintf(data, "%.3d\t", block.burst_time);
    output(data);

	sprintf(data, "%.3d\t", block.arrival_time);
    output(data);

	sprintf(data, "%.3d\t", block.start_time);
    output(data);

	sprintf(data, "%.3d\t", block.end_time);
	output(data);

	sprintf(data, "%.3d\n", block.remaining_burst_time);
    output(data);

    free(data);
}

/*
** display a list of process control blocks
** blocks - the array of process control blocks
** num - the number of process control blocks to display
*/
void displayProcessControlBlocks(PCB * blocks, int num)
{
	int i;
    output("Id\tType\tPrior\tBTime\tATime\tSTime\tETime\tRBTime\n");
	for(i=0; i<num; i++)
	{
		displayProcessControlBlock(*(blocks + i));
	}
}

/*
** Swap two array items
*/
void swap(PCB * item1, PCB * item2)
{
    PCB temp = *item1;
    *item1 = *item2;
    *item2 = temp;
}

/*
** Bubble Sorts process control block array
*/
void sortProcessControlBlock(PCB * A, int pass)
{
    int chkSwap = 0;
    int i;
    for(i=0; i < pass - 1; i++)
    {
		if(A[i].arrival_time > A[i + 1].arrival_time)
        {
			swap(&A[i],&A[i + 1]);
			chkSwap = 1;
        }
    }
    if(chkSwap)
    {
        sortProcessControlBlock(A, (pass - 1));
    }
}

/*
** generate random number
** offset: used to specify where the rand number starts
**		   gerating from
** limit : max a random number generates to
*/
int generateRand(int offset, int limit)
{
	int r;
	int n_limit = limit + 1;
	r = offset+rand()%(n_limit-offset);
	return r;
}

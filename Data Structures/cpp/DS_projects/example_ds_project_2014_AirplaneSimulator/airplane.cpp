#include "airplane.h"
long AirPlane::idinc = 0L;
// -- Constructors --
// default constructor
AirPlane::AirPlane():
   type(1), idnum(AirPlane::idinc++) {
	// initialize the array
	this->init();
}
// primary constructor
AirPlane::AirPlane(int t):
   type(t), idnum(AirPlane::idinc++) {
	// initialize the array
	this->init();
}
// copy constructor
AirPlane::AirPlane(AirPlane* p):
	 type(p->getType()), idnum(p->getIdnum()) {
	// initialize the array
	this->init();
}
// -- Accessors --
long AirPlane::getIdnum() const { return this->idnum; }
int AirPlane::getType() const { return this->type; }
// -- Mutators --
void AirPlane::setType(int t) { this->type = t; }
// -- Other Interfaces --
// displays the info for the current task
void AirPlane::display() const {
	SYSTEM->display(IDC_TEXT_DISPLAY_PLANE, "[AIR" + SYSTEM->toString<long>(this->idnum) + "," + AirPlane::strTypes[this->type] + "] ", 1);
}
// initializes the static arrays
void AirPlane::init() {
	// initialise the airplane type definitions
	this->strTypes = new string [5];
	this->strTypes[0] = "Main";
	this->strTypes[1] = "People(Small)";
	this->strTypes[2] = "Government";
	this->strTypes[3] = "People(Large)";
	this->strTypes[4] = "V.I.P";
}
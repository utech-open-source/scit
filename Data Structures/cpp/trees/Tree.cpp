#include "Tree.h"

Tree::Tree():
	root(NULL)
{

}

void Tree::insertNode(int data)
{
	if(!isFull())
	{
		Node * N = new Node(data);

		if(isEmpty())
			root = N;
		else
		{
			Node * temp = root;

			while(true)
			{

				if(N->getData() < temp->getData())
				{
					if(temp->getLeft() == NULL)
					{
						temp->setLeft(N);
						break;
					}
					else
						temp = temp->getLeft();
				}
				else
				{

					if(temp->getRight() == NULL)
					{
						temp->setRight(N);
						break;
					}
					else
						temp = temp->getRight();
				}
			}
		}
	}
}

Node * Tree::getRoot() const
{
	return this->root;
}

void Tree::preOrderTraversal(Node * t)
{
	if(t != NULL)
	{
		display(t);
		preOrderTraversal(t->getLeft());
		preOrderTraversal(t->getRight());
	}
}


void Tree::inOrderTraversal(Node * t)
{
	if(t != NULL)
	{
		inOrderTraversal(t->getLeft());
		display(t);
		inOrderTraversal(t->getRight());
	}
}

void Tree::postOrderTraversal(Node * t)
{
	if(t != NULL)
	{
		postOrderTraversal(t->getLeft());
		postOrderTraversal(t->getRight());
		display(t);
	}
}

void Tree::display(Node * node)
{
	cout << node->getData() << endl;
}

bool Tree::isEmpty()
{
	if(root == NULL)
		return true;
	else
		return false;
}

bool Tree::isFull()
{
	Node * temp = new Node();
	if(temp != NULL)
	{
		delete temp;
		return false;
	}
	else
		return true;
}
package toc.gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class View extends JFrame implements ActionListener {

	private JLabel numlbl, reslbl;
	private JTextField numtf, restf;
	private JButton convert;
	private JPanel numpan, respan, convertpan, fieldpan;
	private int size;

	public View(int size) {
		super("Decimal To Binary Converter");
		this.size = size;
		setLayout(new GridLayout(0, 1));
		initializeComponents();
		addComponentsToPanel();
		addPanelsToWindow();
		setWindowProperties();
		registerListener();

	}

	public void initializeComponents() {
		numlbl = new JLabel("Decimal Number");
		numtf = new JTextField(15);
		reslbl = new JLabel("Result");
		restf = new JTextField(15);
		convert = new JButton("Compute");
		numpan = new JPanel(new GridLayout(1, 1));
		convertpan = new JPanel(new GridLayout(1, 1));
		respan = new JPanel(new GridLayout(1, 1));
		fieldpan = new JPanel(new GridLayout(1, 1));

	}

	public void addComponentsToPanel() {
		numpan.add(numlbl);
		numpan.add(numtf);
		respan.add(reslbl);
		respan.add(restf);
		convertpan.add(convert);
		for (int x = 0; x < size + 5; x++) {
			fieldpan.add(new JTextField());
		}
	}

	public void addPanelsToWindow() {
		this.add(numpan);
		this.add(respan);
		this.add(fieldpan);
		this.add(convertpan);

	}

	public void setWindowProperties() {
		this.setSize(600, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setResizable(false);
	}

	public void registerListener() {
		convert.addActionListener(this);
	}

	public void updateTape(int index, String val) {
		JTextField field = (JTextField) fieldpan.getComponent(index);
		field.setText(val);
		field.setBackground(Color.WHITE);
	}

	public String userInput() {
		return numtf.getText();
	}
	
	public void setText(String val)
	{
		restf.setText(val);
	}
	
	public void setPositonColor(int index){
		JTextField field = (JTextField) fieldpan.getComponent(index);
		field.setBackground(Color.MAGENTA);
	}
	
	public JButton getConvert(){
		return convert;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource().equals(convert)) {

		}
	}

}

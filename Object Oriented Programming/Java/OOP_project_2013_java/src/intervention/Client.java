package intervention;

public class Client {
	private String fname;
	private String lname;
	private String contactNum;
	private String payType;
	private int clientId;
	
	public Client()
	{
		fname="";
		lname="";
		contactNum="";
		payType= "";
		clientId = 0;
	}
	
	public Client(String fnam,String lnam,String contactNu, String payTyp, int id)
	{
		fname = fnam;
		lname = lnam;
		contactNum = contactNu;
		payType = payTyp;
		clientId = id;
	}
	
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getContactNum() {
		return contactNum;
	}
	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public void display()
	{
		System.out.println("first name is:"+fname);
		System.out.println("last name is:"+lname);
		System.out.println("Contact Number is:"+contactNum);
		System.out.println("Pay Type is:"+payType);
	}
}

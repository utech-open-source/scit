#ifndef VIRTUAL_PROCESSOR_H
#define VIRTUAL_PROCESSOR_H

#include <stdlib.h>
#include "scheduling_queue.h"
#include "process_control_block.h"
#include "defines.h"

#ifdef _WINDOWS_VER
    #include <process.h>
#else
    #include <pthread.h>
#endif

struct Processor
{
	int cycle; //current cycle
	int num_of_process_executed; // the number of process executed
	int state; //current state of the processor
	PCBPtr cpu_bound_process;
};

typedef struct Processor PROCESSOR_ONE;

void virtualProcessor(size_t(*AdditionOperations)(void));

void startVirtualProcessor(PROCESSOR_ONE * p1, void * AdditionOperations);

void stopVirtualProcessor();

bool executeProcess(PCBPtr process);

bool interuptRuningProcess();

#endif

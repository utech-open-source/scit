package intervention;

public class RemovalRequest extends Intervention {
	
	private String outcome;
	private int removalRequestId;
	
	public RemovalRequest()
	{
		super();
		setOutcome("");
		setRemovalRequestId(0);
	}
	
	/**
	 * 
	 * @param idCode
	 * @param client
	 * @param date
	 * @param time
	 * @param address
	 * @param outcome
	 */
	public RemovalRequest(String idCode,Client client,Date date, String time,Address address,String outcome, int id)
	{
		super(idCode,client,date,time,address);
		
		setOutcome(outcome);
		
		setRemovalRequestId(id);
	}


	/**
	 * @return the outcome
	 */
	public String getOutcome() {
		return outcome;
	}


	/**
	 * @param string the outcome to set
	 */
	public void setOutcome(String string) {
		this.outcome = string;
	}


	/**
	 * @return the removalRequestId
	 */
	public int getRemovalRequestId() {
		return removalRequestId;
	}


	/**
	 * @param removalRequestId the removalRequestId to set
	 */
	public void setRemovalRequestId(int removalRequestId) {
		this.removalRequestId = removalRequestId;
	}
}

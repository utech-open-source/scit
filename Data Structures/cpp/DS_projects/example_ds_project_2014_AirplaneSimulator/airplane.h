#ifndef JFF_AIRPLANE_H
#define JFF_AIRPLANE_H

#define JFF_MAIL 1
#define JFF_SMALLPASS 2
#define JFF_GOVERNMENT 3
#define JFF_LARGEPASS 4
#define JFF_VIP 5

#include <iostream>
#include <string>
#include "utility.h"

using namespace std;

class AirPlane {
	private:
		static long idinc;						// used to ensure that all tasks have unique id numbers
		long idnum;										// the unique id number of this task
		int type;											// one of five types: mail, smallpass, government, largepass, vip
		string* strTypes;							// defines the different types of airplanes
	public:
	  // -- Constructors --
		AirPlane();								// default constructor
		AirPlane(int);						// primary constructor
		AirPlane(AirPlane*);			// copy constructor
		// -- Accessors --
		long getIdnum() const;
		int getType() const;
		// -- Mutators --
		void setType(int);
		// -- Other Interfaces --
		void display() const;			// displays the info for the current task
		void init();				// initializes the static arrays
};
#endif
#include "stack.h"

Stack::Stack()
{
	this->top = NULL;
}

void Stack::createStack()
{
	this->top = (Node *)new Node();
}

void Stack::push(Node & data)
{
	Node * temp = new Node(data);

	if(temp != NULL)
	{
		if(top == NULL)
		{
			top = temp;
		}
		else
		{
			temp->setNext(top);
			top = temp;
		}
	}
}


void Stack::push(int data)
{
	push( *( (Node*)new Node(data)) );
}

void Stack::deleteStack()
{
	while(getTop() != NULL)
	{
		pop();
	}
}

Node * Stack::getTop()
{
	return top;
}


void Stack::pop()
{
	Node * tmp = top;

	if(!checkEmpty())
	{
		top = tmp->getNext();

		delete tmp;
	}
}

bool Stack::checkEmpty() const
{
	if(top!= NULL)
	{
		return false;
	}

	return true;
}

bool Stack::checkFull()
{
	Node * tmp = new Node();

	if(tmp != NULL)
	{
		delete tmp;
		return false;
	}

	return true;
}

Stack::~Stack()
{
	deleteStack();
}



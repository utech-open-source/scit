package intervention;

public class ClientVisit extends Intervention {
	private Animal animal;
	private String reason;
	private Date visitDate;
	private int clientVisitId;
	
	public ClientVisit()
	{
		super();
		setReason("");
		setAnimal(new Animal());
		setVisitDate(new Date());
		setClientVisitId(0);
	}

	public ClientVisit(String idCode, Client client, Date date, String time , 
			Address address, Animal animal, String reason, Date visitDate, int id)
	{
		super(idCode,client,date,time,address);
		this.setAnimal(animal);
		this.setReason(reason);
		this.setVisitDate(visitDate);
		setClientVisitId(id);
		
	}

	/**
	 * @return the animal
	 */
	public Animal getAnimal() {
		return animal;
	}

	/**
	 * @param animal the animal to set
	 */
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the visitDate
	 */
	public Date getVisitDate() {
		return visitDate;
	}

	/**
	 * @param visitDate the visitDate to set
	 */
	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}

	/**
	 * @return the clientVisitId
	 */
	public int getClientVisitId() {
		return clientVisitId;
	}

	/**
	 * @param clientVisitId the clientVisitId to set
	 */
	public void setClientVisitId(int clientVisitId) {
		this.clientVisitId = clientVisitId;
	}
}

package toc;

import java.util.HashMap;

public class State<T> {
	private String name;
	private HashMap<String, Action<T>> actions;
	
	public State(String stateName){
		name = stateName;
		actions = new HashMap<String,Action<T>>(0);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addAction(Action<T> action){
		String actionName = "null";
		if(action.getRead() != null) actionName = action.getRead().toString();
		//System.out.print(actionName);
		actions.put(actionName, action);
	}
	
	public boolean actionInMap(String name){
		if(action(name) == null) return false;
		return true;
	}

	public Action<T> action(String name){
		return actions.get(name);
	}
}

package gui;

import java.awt.Color;

public interface Resource {
	
	boolean DEBUG = true;
	String defaultSelection = "Select Option";
	
	String background = "bg.png";
	String splash = "/gui/images/splash.png";
	String [] components = {
							"Database",
							"Address", 
							"Animal",
							"Client",
							"Date",
							"Forms",
							"RemovalRequest",
							"ClientVisit",
							"InterventionProgram"
						   };
	
	/*
	 * Labels For Attributes in the intervention class
	 */
	String idCodeLb = "Intervention Code";
	String timeLb = "Time";
	
	/*
	 * TextField For Attributes in the intervention class
	 */
	int idCodeTf = 10;
	int timeTf = 5;
	
	/*
	 * Labels For Attributes in the Client and ClientVisit
	 */
	String firstNameLb = "First Name";
	String lastNameLb = "Last Name";
	String contactNumLb = "Contact Number"; 
	String payTypeLb = "Payment Type";
	
	/*
	 * TextField For Attributes in the Client
	 */
	int firstNameTf = 20;
	int lastNameTf = 20;
	int contactNumTf = 20;
	
	String [] payTypeTf = {
							defaultSelection, 
							"Cannot",
							"Contribution",
							"Full"
						};
	
	/*
	 * Labels For Attributes in the Date class
	 */
	String dayLb = "DD";
	String monthLb = "MM";
	String yearLb = "YYYY";
	
	/*
	 * TextField For Attributes in the Date class
	 */
	int dayTf = 5;
	int monthTf = 5;
	int yearTf = 5;
	
	/*
	 * Labels For Attributes in the Address class
	 */
	String lotNumLb = "Lot Number";
	String streetLb = "Street";
	String parishLb = "Parish";
	
	/*
	 * TextField For Attributes in the Address class
	 */
	int lotNumTf = 20;
	int streetTf = 20;
	String [] parishTf = {
							defaultSelection, 
							"Clarendon",
							"Hanover",
							"Kingston",
							"Manchester",
							"Portland",
							"St.Andrew",
							"St.Ann",
							"St.Catherine",
							"St.Elizabeth",
							"St.James",
							"St.Mary",
							"St.Thomas",
							"Trelawny",
							"Westmoreland"
						};
	String [] addressTf = {
							defaultSelection,
							"Winchester Road",	
							"Caymanas Track Limited"
							};
	
	String [] winchesterRoad = {
								"",
								"Winchester Road",
								"Kingston"
							};
	
	String [] caymanasTrackLimited = {
										"",
										"Caymanas Track Limited",
										"Kingston"
									};
	
	/*
	 * Labels For Attributes in the Animal class
	 */
	String breedLb = "Breed";
	String genderLb = "Gender";
	String ageLb = "Age";
	String typeLb = "Type";
	
	/*
	 * TextField For Attributes in the Animal class
	 */
	int breedTf = 20;
	String [] genderTf = {"Male", "Female"};
	int ageTf = 20;
	String [] typeTf = {
						defaultSelection, 
						"Dog", 
						"Cat",
						"Bird",
						"Horse",
						"Goat",
						"Cow",
						"Snake",
						"Pig",
						"Rabbit"
					};
	
	/*
	 * Labels For Attributes in the ClientVisit
	 */
	String reasonLb = "Reason";
	
	/*
	 * Labels For Attributes in the RemovalRequest class
	 */
	String outcomeLb = "Outcome";
	
	/*
	 * TextField For Attributes in the RemovalRequest class
	 */
	String [] outcomeTf = {
							defaultSelection, 
							"Adoption", 
							"Euthanized"
							};
	
	/*
	 * TextField default Attributes
	 */
	
	boolean centerTextField = true;
	
	/*
	 * TextArea default Attributes
	 */
	
	int textAreaMargin = 10;
	boolean lineWrap = true;
	boolean wrapStyleWord = true;
	Color textAreaColor = Color.getHSBColor(0,100,100);
	
	
	/*
	 * Default Title Text
	 */
	
	String dateTitle = "Date";
	int labelWidth = 100; 
	int labelheight = 20;
	String secAnimalTitle = "This Section Contains Animal Information";
	String secAddressTitle = "Address of Animal";
	String secClientTitle = "This Section Contains The Client Information";
	
	/*
	 * Default Location Data percentages
	 */
	int UIWidth  = 38;
	int UIHeight = 65;
	
	/*
	 * Client Visit and Removal Request Forms
	 */
	String clientVisitFormName = "Client Visit Add Form";
	String removalRequestFormName = "Removal Request Add Form";
	
	/*
	 * Client Visit and Removal Request Form Width and Height
	 */
	int clientVisitFormWidth = 28;
	int removalRequestFormWidth = 28;
	
	int clientVisitFormHeight = 87;
	int removalRequestFormHeight = 65;
	
	/*
	 * Removal Request table display fields
	 */
	String [] removalRequestDisplayFields = {"InterventionID",
			"Date",
			lotNumLb,
			streetLb,
			parishLb,
			firstNameLb, 
			lastNameLb,
			outcomeLb,
			 "View",
			 "Delete"
		};
	
	/*
	 * Client Visit table display fields
	 */
	String [] clientVisitDisplayFields = {"InterventionID",
			"Date",
			"Address",
			firstNameLb, 
			lastNameLb,
			"Visit Date",
			"Animal "+typeLb,
			"Animal "+breedLb,
			 "View",
			 "Delete"
		};
}

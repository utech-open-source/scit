#ifndef STACK_H
#define STACK_H

#include "node.h"

class Stack{

private:
	Node * top;

public:
	Stack();
	Node * pop();
	void push(char);
	bool isEmpty();
	bool isFull();

};

#endif
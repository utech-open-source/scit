#ifndef AIRPLANESIMULATORGUI_H
#define AIRPLANESIMULATORGUI_H

#define SIMTIME "2"
#define SLEEPTIME "300"
#define SYSTEM_NAME "AirPlane Simulator V1"

#include "resource.h"
#include "utility.h"

#include "airplanegenerator.h"
#include "landingroad.h"
#include "portofarrival.h"
#include "airplane.h"

//macros to identify table element
#ifdef IDC_TABLE_START
#define TABLE_ELEMENT(col, row, s) (IDC_TABLE_START + ((s * row) + col))
#endif

bool performLanding(LandingRoad*, AirPlane*);
void performPorting(LandingRoad*, PortOfArrival*);
void display(LandingRoad *[], PortOfArrival *[], int);
void displayFailedMessage(LandingRoad *, AirPlane *);
int totalAcceptedPlanes(PortOfArrival * []);
void run(void *);

#endif
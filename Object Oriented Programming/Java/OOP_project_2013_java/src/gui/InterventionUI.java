package gui;

import gui.components.ChildDialog;
import gui.components.FileManager;
import gui.components.JPanelBackground;
import gui.components.Location;
import gui.components.SplashWindow;
import gui.components.TableModel;
import gui.forms.ClientVisitForm;
import gui.forms.ClientVisitReportForm;
import gui.forms.FormData;
import gui.forms.RemovalRequestForm;
import gui.forms.RemovalRequestReportForm;
import intervention.InterventionProgram;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.Timer;

import logging.Log;

public class InterventionUI{
	
	public Frame frame = new Frame("Intervention Program");
	public Frame debug = new Frame("Debug");
	
	private ClientVisitForm clientVisitForm;
	private RemovalRequestForm removalRequestForm;
	private Timer t, t2 = null;
	
	//BUttons  (executes an action upon clicking)
	//private JButton addClientVisitBtn = new JButton("Add Client Visit");
	//private JButton addRemovalRequestBtn = new JButton("Add Removal Request");
	private JButton viewAllClientVisitBtn = new JButton("View Client Visit");
	private JButton viewAllRemovalRequestBtn = new JButton("View Removal Request");
	private JButton saveChanges = new JButton("Save");
	private JButton exit = new JButton("Exit");
	
	public InterventionUI()
	{		
		JMenu help = new JMenu("Help");
		
		//set up the jmenu bar
		frame.setJMenuBar(new JMenuBar());
		frame.getJMenuBar().add(help);
		
		//menu items
		JMenuItem Information = new JMenuItem("Help Information");
		//JMenuItem debug = new JCheckBoxMenuItem("Debug");
		JMenuItem debug = new JMenuItem("Debug");
		JMenuItem about = new JMenuItem("About GUI Standard");
		
		help.add(Information);
		help.add(about);
		help.add(debug);
		
		
		about.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e){
				about();
			}
			
		});
	
		Information.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e){
				help();
			}
		});
		
		debug.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e){
				debug();
			}
		});
		
		JPanel inner = new JPanel(new GridLayout(0,3, 10,10));
		
		inner.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		//change buttons to background images

		exit = addBackground(exit, "exit.png");
		saveChanges = addBackground(saveChanges, "save.png");
		//addClientVisitBtn = addBackground(addClientVisitBtn, "button1.png");
		//addRemovalRequestBtn = addBackground(addRemovalRequestBtn, "button2.png");
		viewAllClientVisitBtn = addBackground(viewAllClientVisitBtn, "button3.png");
		viewAllRemovalRequestBtn = addBackground(viewAllRemovalRequestBtn, "button4.png");
		
		inner.add(viewAllClientVisitBtn);
		inner.add(viewAllRemovalRequestBtn);
		//inner.add(saveChanges);
		inner.add(exit);
		
		//banner
		JPanelBackground banner = new JPanelBackground(Resource.background);
		banner.setBackground(Color.WHITE);
		inner.setBackground(Color.GRAY);
		
		viewAllClientVisitBtn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				viewAllClientVisitButton();	
			}
		});
		
		
		viewAllRemovalRequestBtn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				viewAllRemovalRequestButton();	
			}
		});
		
		//exit button action
		exit.addActionListener(new ActionListener()
		{

			public void actionPerformed(ActionEvent event) {
				WindowEvent closingEvent = new WindowEvent(frame, WindowEvent.WINDOW_CLOSING);
				Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(closingEvent);
			}
			
		});

		//add JPanels to the main
		frame.getContentPane().add(banner, BorderLayout.CENTER);
		frame.getContentPane().add(inner, BorderLayout.PAGE_END);
		//modify the frame size location and resizability
		frame.setResizable(false);
	    frame.setSizeAndLocation(new Location(Resource.UIWidth, Resource.UIHeight));
		//necessary for proper frame termination
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//initialize the database
		Event.IP = new InterventionProgram();
		Event.frame = frame;
		//Event.IP.close();
	}
	
	public void addClientVisitButton(){
		
		clientVisitForm = new ClientVisitForm();
		
		FormData.modifyForm(clientVisitForm);
		FormData.addCurDate(clientVisitForm.getVisitDateForm());
		
		ActionListener action = Event.clientVisitSave(clientVisitForm);
		
		Location loc = new Location(Resource.clientVisitFormWidth,Resource.clientVisitFormHeight, true);
		
		ChildDialog.showDialog(frame, clientVisitForm, viewAllClientVisitBtn, 
				action, loc, Resource.clientVisitFormName,"close");
		
		ChildDialog.visible(true);
		
		//view clients after addition
		//viewAllClientVisitButton();
	}
	
	public void addRemovalRequestButton()
	{
		
		removalRequestForm = new RemovalRequestForm();
		
		FormData.modifyForm(removalRequestForm);
		
		ActionListener action = Event.removalRequestSave(removalRequestForm);
		
		Location loc = new Location(Resource.removalRequestFormWidth,Resource.removalRequestFormHeight);
		
		ChildDialog.showDialog(frame, removalRequestForm, viewAllRemovalRequestBtn, 
				action, loc, Resource.removalRequestFormName,"close");
		
		ChildDialog.visible(true);
		
		//view removal request after addition
		//viewAllRemovalRequestButton();
        
	}
	
	public void viewAllClientVisitButton()
	{
		final ClientVisitReportForm form = new ClientVisitReportForm(Resource.clientVisitDisplayFields, 
															Event.clientVisitObjs(
																		Event.IP.viewAllClientVisit()
																	));
		
		//SetTimer
		t = new Timer(400, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	if(Event.IP.getDb().isUpdateQuery())
            	{
	                TableModel tableModel = form.getDm();
	                tableModel.setData(Event.clientVisitObjs(
	                			Event.IP.viewAllClientVisit()
	                		));
	                tableModel.fireTableDataChanged();
            	}
            }
        });
		
        t.start();
		
		ChildDialog dialog = Event.newReport(
					form, 
					viewAllClientVisitBtn,
					"All Client Visit",
					"ClientVisit"
				);
		
		//set default operation for Windowlistener to work
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		dialog.addWindowListener(new WindowAdapter() {
		    public void windowClosed(WindowEvent e) {
		    	super.windowClosed(e);
		        t.stop();
		    }
		});
		
		JButton newB = new JButton("new");
		
		newB.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e){
				addClientVisitButton();
			}
			
		});;

		//add new component
		dialog.getbuttonPane().add(Box.createRigidArea(new Dimension(10, 0)));
		dialog.getbuttonPane().add(newB);
		
		ChildDialog.visible(true);
		
	}
	
	public void viewAllRemovalRequestButton()
	{
		final RemovalRequestReportForm form = new RemovalRequestReportForm(Resource.removalRequestDisplayFields, 
																			Event.removalRequestObjs(
																					Event.IP.viewAllRemovalRequest()
																					));
		//Set Timer
		t = new Timer(400, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	if(Event.IP.getDb().isUpdateQuery())
            	{
	                TableModel tableModel = form.getDm();
	                tableModel.setData(Event.removalRequestObjs(
	                			Event.IP.viewAllRemovalRequest()
	                		));
	                tableModel.fireTableDataChanged();
            	}
            }
        });
		
        t.start();
        
		ChildDialog dialog =  Event.newReport(
				form, 
				viewAllRemovalRequestBtn,
				"All Removal Request",
				"RemovalRequest"
				);
		
		//set default operation for Windowlistener to work
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		dialog.addWindowListener(new WindowAdapter() {
		    public void windowClosed(WindowEvent e) {
		    	super.windowClosed(e);
		        t.stop();
		    }
		});
		
		JButton newB = new JButton("new");
		
		newB.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e){
				addRemovalRequestButton();
			}
		});;

		//add new component
		dialog.getbuttonPane().add(Box.createRigidArea(new Dimension(10, 0)));
		dialog.getbuttonPane().add(newB);
		
		ChildDialog.visible(true);
	    		
	}
	
	/**
	 * 
	 * @param button
	 * @param image
	 * @return
	 */
	public JButton addBackground(JButton button, String image)
	{
		
		try {
			BufferedImage img = ImageIO.read(Frame.class.getResource("/gui/images/"+image));
		    //exit.setIcon(new ImageIcon(img));
			button = new JButton(new ImageIcon(img));
			button.setBorder(BorderFactory.createEmptyBorder());
			button.setContentAreaFilled(false);
		} 
		catch (Exception ex) {
			Log.LogError(ex.getMessage());
		}
		
		return button;
	}
	
	
	public void about() {
		
    	JOptionPane.showMessageDialog(null, 
				new FileManager().readFromURL("/gui/files/about"),
				"About", 
				JOptionPane.PLAIN_MESSAGE
				);		
		
	}
	
	public void help(){
		JOptionPane.showMessageDialog(null,
				new FileManager().readFromURL("/gui/files/help"),
				"Help",
				JOptionPane.PLAIN_MESSAGE
				);
	}
	
	//read debug messages
	public void debug(){
		
		final JTextArea textArea = new JTextArea();
		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
		
		//set text area editablelity to false
		textArea.setEditable(false);
		
		//set childFrame
		frame.setChildFrame(debug);
		
		//set log changed to true
		Log.IsChanged(true);
		
		//Set Timer
		t2 = new Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	if(Log.changed())
            	{
            		textArea.setText(new FileManager("log").readFromFile());
            		Log.IsChanged(false);
            	}
            }
        });
		
        t2.start();
        
		//set default operation for Windowlistener to work properly
		debug.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		debug.addWindowListener(new WindowAdapter() {
			
			@Override
		    public void windowClosed(WindowEvent e) {
		    	super.windowClosed(e);
		    	
		        t2.stop();
		    }
		});
		
		textArea.setLineWrap(Resource.lineWrap);
		textArea.setWrapStyleWord(Resource.wrapStyleWord);
		
		debug.getContentPane().add(scroll);
		debug.setVisible(true);
		debug.setSizeAndLocation(new Location(50,80));
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String args[])
	{		
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	try
            	{	
            		//remove log.lck file if present
            		if(new File("log.lck").delete());
            		
            		//Log.reset();
            		Log.Initialize();
            		SplashWindow.splash(Resource.splash, new InterventionUI());
            		
            		//test to see if exception would work
            		//throw new IndexOutOfBoundsException("This is an Error Test");
            		
            	}
            	catch(Exception e)
            	{
                	JOptionPane.showMessageDialog(null, 
            				"An unexpected error has occured"+"\n"+
            				"the System will now exit"+"\n\n"+
            				e.getMessage(),
            				"Error", 
            				JOptionPane.ERROR_MESSAGE
            		);
                	
                	//exit the system
                	System.exit(0);
            	}
            }
        });
        
	}

}

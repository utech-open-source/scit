#ifndef EDGE_H
#define EDGE_H

#include "Vertice.h"

class Vertice;

class Edge{

private:
	double distance;
	double time;
	Vertice * left;
	Vertice * right;

public:
	Edge(){
		this->distance = 0;
		this->time = 0;
		this->left = NULL;		
		this->left = NULL;
	}

	Edge(double distance, double time, Vertice * left, Vertice * right){
		this->time = time;
		this->distance = distance;
		this->left = left;
		this->right = right;
	}

	void setDistance(double distance){
		this->distance = distance;
	}

	double getDistance()const{
		return this->distance;
	}

	void setTime(double time){
		this->time = time;
	}

	double getTime()const{
		return this->time;
	}

	void setRight(Vertice * vertice)
	{
		this->right = vertice;
	}

	void setLeft(Vertice * vertice)
	{
		this->left = vertice;
	}

	Vertice * getLeft()const{
		return this->left;
	}

	Vertice * getRight()const{
		return this->right;
	}
};

#endif
package gui.forms;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import logging.Log;

public class FormValidation {

	//error messages
	static String phone = "invalid phone number";
	static String blank = "this cannot be empty";
	static String selection = "valid option was not selected";
	static String date = "invalid date";
	
	/**
	 * 
	 * @param input
	 * @return boolean
	 */
	static boolean isblank(String input) 
	{
		   if (input.equals("")) {
		      return true;
		   }

		   return false;
	}
	
	
	/**
	 * 
	 * @param input
	 * @return boolean
	 */
	static boolean isPhoneNumber(String input)
	{

	      Pattern pattern = Pattern.compile("\\d{7}");
	      Matcher matcher = pattern.matcher(input);
	 
	      if (matcher.matches()) {
	    	  return true;
	      }
	      
	      return false;
	}
	
	/**
	 * 
	 * @param input
	 * @return boolean
	 */
	static boolean date(String input)
	{
		return true;
	}
	
	/**
	 * 
	 * @return date
	 */
	static Date curDate()
	{
		Date date = new Date();
		
		return date;
	}
	
	/**
	 * 
	 * @param format
	 * @return String
	 */
	public static String getDate(String format)
	{
		if(format == null)
			format = "dd/MM/yyyy HH:mm";
		
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(curDate());
	}
	
	/**
	 * 
	 * @param date
	 * @return boolean
	 */
	public static boolean checkDate(intervention.Date date)
	{
		Log.LogInfo(date.toString());
		
		if(date.getDay() > 31
				|| date.getMonth() > 12
				)
			return false;
		
		return true;
	}
}

//program to test for a vowel
//Author: Mark Robinson

#include<stdio.h>
#include<conio.h>

void main()
{

	int i;
	char vowel[5] ={'a','e','i','o','u'};
	char letter;

	printf("Enter A letter ");
	scanf("%c", &letter);


	for(i=0; i<5; i++)
	{
		if(letter == vowel[i])
		{
			printf("%c is a vowel", letter);
		}
	}

	getch();
}
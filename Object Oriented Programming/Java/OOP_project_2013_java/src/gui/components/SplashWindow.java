package gui.components;

import gui.InterventionUI;
import gui.Resource;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;


@SuppressWarnings("serial")
public class SplashWindow extends JFrame {

  private static SplashWindow instance;

  private boolean paintCalled = false;

  private Image image;
  
  private static InterventionUI interventionUI;


  private SplashWindow(Image image, InterventionUI interventionUI) {
    super();
    this.image = image;
    JLabel label = new JLabel();
    label.setIcon(new ImageIcon(image));
    this.add(label);    
    this.setUndecorated(true);
    this.setAlwaysOnTop(true);
    this.pack();
    this.setLocationRelativeTo(null);
    
    //initialize intervention here
    SplashWindow.interventionUI = interventionUI;

  }
  
  public static void splash(String image, InterventionUI ui)
  {
	  URL img = SplashWindow.class.getResource(image);
	  splash(img, ui);
  }

  public static void splash(URL imageURL, InterventionUI ui) 
  {
    if (imageURL != null) {
      splash(Toolkit.getDefaultToolkit().createImage(imageURL), ui);
    }
    else
    {
    	System.err.println("not found");
    }
  }

  public static void splash(Image image, InterventionUI ui) 
  {
    if (instance == null && image != null) {
      instance = new SplashWindow(image, ui);
      instance.setVisible(true);
      
      if (!EventQueue.isDispatchThread() && Runtime.getRuntime().availableProcessors() == 1) {

        synchronized (instance) {
          while (!instance.paintCalled) {
            try {
              instance.wait();
            } catch (InterruptedException e) {
            }
          }
          
        }
      }
    }
  }
  
	
  static void renderSplashFrame(Graphics g, int frame) {
      final String[] comps = Resource.components;
      g.setColor(Color.WHITE);
      g.fillRect(30,100,200,10);
      g.setPaintMode();
      g.setColor(Color.BLACK);
      g.drawString("Loading: "+comps[frame]+"...", 30, 110);
  }

  @Override
  public void update(Graphics g) 
  {
    paint(g);
  }

  @Override
  public void paint(Graphics g) 
  {

	g.drawImage(image, 0, 0, this);
	
	for(int i=0; i<9; i++) 
	{
		renderSplashFrame(g, i);
		try {
          Thread.sleep(200);
      }
      catch(InterruptedException e) {
      }
	}
	
    disposeSplash();
    
	if (!paintCalled) {
	  paintCalled = true;
	  synchronized (this) {
	    notifyAll();
	  }
	}
  }

  public static void disposeSplash() {
	interventionUI.frame.display();
    instance.setVisible(false);
    instance.dispose();
  }
}

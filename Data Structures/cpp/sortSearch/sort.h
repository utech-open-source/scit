/**
* author: Mark Robinson
* date: 2/13/2014
**/

#ifndef SORT_H_
#define SORT_H_

template <class S>
class Sort
{

private:
	S * original;
	S * tmp;
	int size;

public:
	Sort(S *, int);
	void insertion();
	void selection();
	void bubble();
	void newTmp();
	int getSize();
	S * getArray();
	~Sort();

private:
	void bubble_(int);//int pass needed parameter the size since function is recursive
};


template<class S>
Sort<S>::Sort(S * A, int s){
	tmp = NULL;
	original = A;
	size = s;
}

template<class S>
void Sort<S>::insertion()
{
	newTmp();

	int index, cnt2, temp;

	index = 1; 

	while(index < getSize())
	{
		temp = *(tmp+index);
		cnt2 = index;

		while(cnt2 >= 0 && temp <= *(tmp+(cnt2-1)) )
		{
			*(tmp+cnt2) = *(tmp+(cnt2-1));
			cnt2--;
		}

		*(tmp+cnt2) = temp;
		index++;
	}

}


template<class S>
void Sort<S>::selection()
{
	newTmp(); 

	int index, indexSmallest, cnt2;

	index = 0;

	while(index < size)
	{
		indexSmallest = index;
		cnt2 = index + 2;

		while(cnt2 < size)
		{
			if(*(tmp+cnt2) < *(tmp+indexSmallest))
				indexSmallest = cnt2;
			cnt2++;
		}

		S temp = *(tmp+index);
		*(tmp+index) = *(tmp+indexSmallest);
		*(tmp+indexSmallest) = temp;
		index++;
	}
}

/*
* bubble - sorts a array using the bubble sort algorithm
* Args
* void
*/
template<class S>
void Sort<S>::bubble()
{
	newTmp();

    bool chkSwap = false;
    
	while(true)
	{
		for(int i = 0; i < size - 1; i++)
		{
			if( *(tmp + i) > *(tmp + (i + 1) ))
			{
				//perform swapping
				S temp = *(tmp + i);
				*(tmp + i) = *(tmp + (i + 1) );
				*(tmp + (i + 1)) = temp;

				chkSwap = true;
			}
		}

		if(chkSwap == false)
			break;
		else
			chkSwap = false;

		size--;
	}
}


/*
* newTmp
* Args
* void
* creates a copy of the orignal array called tmp
* does not modify the original array 
*/
template<class S>
void Sort<S>::newTmp()
{
	int cnt = 0;

	tmp = new S[size];

	while(cnt < size)
	{
		tmp[cnt] = original[cnt];
		cnt++;
	}
}

template<class S>
int Sort<S>::getSize()
{
	return size;
}

/**
* getArray - returns the tmp pointer
**/
template<class S>
S * Sort<S>::getArray()
{
	return tmp;
}

template<class S>
Sort<S>::~Sort(){
	//delete tmp if it is not null
	if(tmp != NULL)
		delete tmp;
}


#endif
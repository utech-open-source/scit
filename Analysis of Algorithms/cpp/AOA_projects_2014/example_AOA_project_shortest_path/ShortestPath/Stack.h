/**
* Generic Stack datastructure built from generic Link List
* Author : Mark Robinson
* Date : April 4, 2014 
**/

#ifndef STACK_H
#define STACK_H

#include "List.h"

template <typename STACKTYPE>
class Stack : private List<STACKTYPE>
{
public:
	bool pop(STACKTYPE & data){
        return this->removeFromFront(data);
	}

	bool push(STACKTYPE & data){
        return this->insertAtFront(data);
	}

	bool isStackEmpty() const{
        return this->isEmpty();
	}
};

#endif

package gui.forms;

import gui.Resource;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BaseForm extends Form{
	
	private DateForm dateForm;
	private AddressForm addressForm;
	private ClientForm clientForm;
	
	/*
	 * Labels For Attributes in the intervention class
	 */
	private JLabel idCodeLb;
	private JLabel timeLb;
	
	/*
	 * TextField For Attributes in the intervention class
	 */
	private JTextField idCodeTf;
	private JTextField timeTf;
	
	
	public BaseForm()
	{
		super();
		
		setFormPanel(new JPanel());
		
		getFormPanel().setLayout(new BoxLayout(getFormPanel(), BoxLayout.Y_AXIS));
		
		setDateForm(new DateForm());
		setAddressForm(new AddressForm());
		setClientForm(new ClientForm());
		
		setIdCodeLb(Label(Resource.idCodeLb));
		setTimeLb(Label(Resource.timeLb));
		
		setIdCodeTf(TextField(Resource.timeTf));
		setTimeTf(TextField(Resource.timeTf, 80, 30));
	}


	/**
	 * @return the dateForm
	 */
	public DateForm getDateForm() {
		return dateForm;
	}


	/**
	 * @param dateForm the dateForm to set
	 */
	public void setDateForm(DateForm dateForm) {
		this.dateForm = dateForm;
	}


	/**
	 * @return the addressForm
	 */
	public AddressForm getAddressForm() {
		return addressForm;
	}


	/**
	 * @param addressForm the addressForm to set
	 */
	public void setAddressForm(AddressForm addressForm) {
		this.addressForm = addressForm;
	}


	/**
	 * @return the idCodeLb
	 */
	public JLabel getIdCodeLb() {
		return idCodeLb;
	}


	/**
	 * @param idCodeLb the idCodeLb to set
	 */
	public void setIdCodeLb(JLabel idCodeLb) {
		this.idCodeLb = idCodeLb;
	}


	/**
	 * @return the timeLb
	 */
	public JLabel getTimeLb() {
		return timeLb;
	}


	/**
	 * @param timeLb the timeLb to set
	 */
	public void setTimeLb(JLabel timeLb) {
		this.timeLb = timeLb;
	}


	/**
	 * @return the idCodeTf
	 */
	public JTextField getIdCodeTf() {
		return idCodeTf;
	}


	/**
	 * @param idCodeTf the idCodeTf to set
	 */
	public void setIdCodeTf(JTextField idCodeTf) {
		this.idCodeTf = idCodeTf;
	}


	/**
	 * @return the timeTf
	 */
	public JTextField getTimeTf() {
		return timeTf;
	}


	/**
	 * @param timeTf the timeTf to set
	 */
	public void setTimeTf(JTextField timeTf) {
		this.timeTf = timeTf;
	}


	/**
	 * @return the clientForm
	 */
	public ClientForm getClientForm() {
		return clientForm;
	}


	/**
	 * @param clientForm the clientForm to set
	 */
	public void setClientForm(ClientForm clientForm) {
		this.clientForm = clientForm;
	}
	

	public void addToPanel()
	{		
		//diable the idcode text field
		getIdCodeTf().setEditable(false);
		
		//diable time entry
		getTimeTf().setEditable(false);
		
		//disable date entry
		getDateForm().getDayTf().setEditable(false);
		getDateForm().getMonthTf().setEditable(false);
		getDateForm().getYearTf().setEditable(false);
		
		getFormPanel().add(wrap(getIdCodeLb(), getIdCodeTf()));
		
		getFormPanel().add(getDateForm().render());
		
		getFormPanel().add(space(wrap(getTimeLb(), getTimeTf()), 10));
		
		getFormPanel().add(space(title(getAddressForm().getSecTitle()), 10));
		
		getFormPanel().add(space(getAddressForm().render(), 10));
		
		getFormPanel().add(space(title(getClientForm().getSecTitle()), 10));
		
		getFormPanel().add(space(getClientForm().render(), 10));
	}

}

package intervention;

import database.InterventionDb;

public class InterventionProgram {
	
	private InterventionDb db;
	
	/**
	 * @return the db
	 */
	public InterventionDb getDb() {
		return db;
	}

	/**
	 * @param db the db to set
	 */
	public void setDb(InterventionDb db) {
		this.db = db;
	}
	
	public InterventionProgram()
	{
		setDb(new InterventionDb());
	}
	
	/**
	 * 
	 * @param clientVisit
	 */
	public void addClientVisit(ClientVisit clientVisit)
	{
		getDb().writeClientVisit(clientVisit);
	}
	
	/**
	 * 
	 * @param clientVisit
	 */
	public void updateClientVisit(ClientVisit clientVisit)
	{
		getDb().updateClientVisit(clientVisit);
	}
	
	/**
	 * 
	 * @param removalRequest
	 */
	public void addRemovalRequest(RemovalRequest removalRequest)
	{
		getDb().writeRemovalRequest(removalRequest);
	}
	
	/**
	 * 
	 * @param removalRequest
	 */
	public void updateRemovalRequest(RemovalRequest removalRequest)
	{
		getDb().updateRemovalRequest(removalRequest);
	}
	
	/**
	 * 
	 * @param clientVisitId
	 */
	public void deleteClientVisit(String clientVisitId)
	{
		getDb().deleteClientVisit(clientVisitId);
		
	}
	
	/**
	 * 
	 * @param clientVisitId
	 */
	public void deleteRemovalRequest(String clientVisitId)
	{
		getDb().deleteRemovalRequest(clientVisitId);
		
	}
	
	/**
	 * 
	 * @return
	 */
	public Object[] viewAllClientVisit()
	{
		return getDb().readClientVisit(null, 10, 0);
	}
	
	/**
	 * 
	 * @return
	 */
	public Object[] viewAllRemovalRequest()
	{
		return getDb().readRemovalRequest(null, 10, 0);
	}
	
	/**
	 * 
	 * @param interventionId
	 * @return
	 */
	public Object[] viewClientVisit(String interventionId)
	{
		return getDb().readClientVisit(interventionId, 1, 0);
	}
	
	
	/**
	 * 
	 * @param interventionId
	 * @return
	 */
	public Object[] viewRemovalRequest(String interventionId)
	{
		return getDb().readRemovalRequest(interventionId, 1, 0);
	}
	
	/**
	 * 
	 * @param report
	 * @return
	 */
	public Object[] searchRemovalRequest(Object [] report)
	{
		return getDb().searchRemovalRequest(report);
	}
	
	/**
	 * 
	 * @param report
	 * @return
	 */
	public Object[] searchClientVisit(Object [] report)
	{
		return getDb().searchClientVisit(report);
	}
	
	public void addAllClientVisit()
	{
		
	}
	
	public void close()
	{
		getDb().getDatabase().close();
	}
}

package gui.components;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import logging.Log;

public class JPanelBackground extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private Image backgroundImage;

	  // Some code to initialize the background image.
	  public JPanelBackground(String fileName){
		  
		URL imgUrl = JPanelBackground.class.getResource("/gui/images/"+fileName);
		
	    try {
			backgroundImage = ImageIO.read(imgUrl);
		} catch (IOException e) {
			Log.LogError(e.getMessage());
		}
	  }

	  public void paintComponent(Graphics g) {
	    super.paintComponent(g);

	    // Draw the background image.
	    g.drawImage(backgroundImage, 0, 0, this);
	  }
}

package intervention;

import gui.Resource;

public class Date {

	private int day;
	private int month;
	private int year;
	
	
	public Date()
	{
		setDay(0);
		setMonth(0);
		setYear(0);
		
	}
	
	public Date(int day, int month,int year)
	{
		this.setDay(day);
		this.setMonth(month);
		this.setYear(year);
	}

	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(int day) {
		this.day = day;
	}

	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		this.month = month;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}
	
	/**
	 * 
	 */
	public String toString()
	{
		String str = getMonth()+"/"+getDay()+"/"+getYear();
		
		return str;
	}
	
	
	public static Date fromString(String str)
	{
		Date date = new Date();
		
		try{
			
			String [] array = str.split("/");
			date = new Date( Integer.parseInt(array[0]), 
							Integer.parseInt(array[1]),
							Integer.parseInt(array[2])
							);
		}
		catch(Exception e)
		{
			if(Resource.DEBUG)
				System.out.println("Error in function fromString");
		}
		
		return date;
	}

	public void display()
	{
		System.out.println("Day is:"+day);
		System.out.println("Month is:"+month);
		System.out.println("Year is:"+year);
	}
	
}

package intervention;

public class Intervention {
	private String idCode;
	private Client client;
	private Date date;
	private String time;
	private Address address;
	//private String information;
	
	
	
	public Intervention()
	{
		setIdCode("");
		setTime("");
		setAddress(new Address());
		setClient(new Client());
		setDate(new Date());
		
	}
	
	public Intervention(String idCode,Client client,Date date, String time,Address address)
	{
		this.setIdCode(idCode);
		this.setTime(time);
		this.setAddress(address);
		this.setClient(client);
		this.setDate(date);

	}

	/**
	 * @return the idCode
	 */
	public String getIdCode() {
		return idCode;
	}

	/**
	 * @param idCode the idCode to set
	 */
	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}

	/**
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * @param client the client to set
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param string the time to set
	 */
	public void setTime(String string) {
		this.time = string;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}
	
}

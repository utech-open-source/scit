#include "airplanesimulatorgui.h"
#include <process.h>

//initialize static utility object instance
Utility Utility::sys = Utility();

/*
* lpClassName :
* BUTTON
* TEXT
* BUTTON
* EDIT
*/

void CreateWindowItems(HWND hwnd, HINSTANCE hInst)
{
	//Label for display text box 1
	SYSTEM->MakeTrans(CreateWindow(TEXT("STATIC"), TEXT("Landing Road Display"),
					WS_CHILD | WS_VISIBLE, 
					350, 30, 200, 25, hwnd, NULL, hInst, 0));
	//display text box 1 
	CreateWindow(TEXT("EDIT"), NULL, 
								WS_CHILD | WS_VISIBLE | WS_HSCROLL | //WS_DISABLED |
                                WS_VSCROLL | ES_LEFT | ES_MULTILINE |
                                ES_AUTOHSCROLL | ES_AUTOVSCROLL,
                  350, 60, 350, 140, hwnd, (HMENU)IDC_TEXT_DISPLAY_PLANE, hInst, NULL);

	//Label for display text box 2
	SYSTEM->MakeTrans(CreateWindow(TEXT("STATIC"), TEXT("Port Of Arrival Display"),
					WS_CHILD | WS_VISIBLE, 
					350, 210, 200, 25, hwnd, NULL, hInst, 0));

	//display text box 2
	CreateWindow(TEXT("EDIT"), NULL, 
								WS_CHILD | WS_VISIBLE | WS_HSCROLL | //WS_DISABLED |
                                WS_VSCROLL | ES_LEFT | ES_MULTILINE |
                                ES_AUTOHSCROLL | ES_AUTOVSCROLL,
                  350, 240, 350, 140, hwnd, (HMENU)IDC_TEXT_DISPLAY_PORT, hInst, NULL);

	CreateWindow(TEXT("STATIC"), NULL, 
								WS_CHILD | WS_VISIBLE ,
                  350, 390, 350, 40, hwnd, (HMENU)IDC_TEXT_STATUS, hInst, NULL);


	//control buttons

	CreateWindow(TEXT("BUTTON"), TEXT("START"),
					WS_CHILD | WS_VISIBLE, 
					10, 30, 80, 25, hwnd, (HMENU)IDC_BUTTON, hInst, 0); 

	CreateWindow(TEXT("BUTTON"), TEXT("STOP"),
					WS_CHILD | WS_VISIBLE, 
					10, 60, 80, 25, hwnd, (HMENU)IDC_BUTTON_STOP, hInst, 0); 

	CreateWindow(TEXT("BUTTON"), TEXT("RESET"),
					WS_CHILD | WS_VISIBLE, 
					10, 90, 80, 25, hwnd, (HMENU)IDC_BUTTON_RESET, hInst, 0);

	CreateWindow(TEXT("BUTTON"), TEXT("EXIT"),
					WS_CHILD | WS_VISIBLE, 
					10, 120, 80, 25, hwnd, (HMENU)IDC_BUTTON_EXIT, hInst, 0);


	//text fields
	SYSTEM->MakeTrans(CreateWindow(TEXT("STATIC"), TEXT("interval time"),
					WS_CHILD | WS_VISIBLE, 
					100, 30, 100, 25, hwnd, NULL, hInst, 0));


	CreateWindow(TEXT("EDIT"), TEXT(SIMTIME),
					WS_CHILD | WS_VISIBLE | WS_BORDER, 
					100, 60, 50, 25, hwnd, (HMENU)IDC_TEXT_INTERVAL, hInst, 0);


	CreateWindow(TEXT("STATIC"), TEXT("sleep time (ms)"),
					WS_CHILD | WS_VISIBLE, 
					100, 100, 100, 25, hwnd, NULL, hInst, 0);

	CreateWindow(TEXT("EDIT"), TEXT(SLEEPTIME),
					WS_CHILD | WS_VISIBLE | WS_BORDER, 
					100, 130, 50, 25, hwnd, (HMENU)IDC_TEXT_SLEEP_TIME, hInst, 0);


	//results table
	//Label for TABLE
	SYSTEM->MakeTrans(CreateWindow(TEXT("STATIC"), TEXT("Tabel Showing Result Of Simulation"),
					WS_CHILD | WS_VISIBLE, 
					40, 180, 240, 25, hwnd, NULL, hInst, 0));

	//table header

	//table data
	int widths[5] = {10, 70, 150, 220, 270};

	int sizes[5] = {50, 70, 60, 40, 70};

	for(int c=0; c < 5; c++)
	{
		int height = 210;

		for(int r=0; r<6; r++)
		{
			//give each column a special identifier
			int id = TABLE_ELEMENT(c, r, 5);

			CreateWindow(TEXT("EDIT"), TEXT(""),
						WS_CHILD | WS_VISIBLE | WS_BORDER | WS_DISABLED, 
						widths[c], height, sizes[c], 25, hwnd, (HMENU)id, hInst, 0);

			//SYSTEM->SetText(id, SYSTEM->toString<int>(id));

			height += 30;
		}
	}


	CreateWindow(TEXT("EDIT"), TEXT(""),
				WS_CHILD | WS_VISIBLE | WS_BORDER | WS_DISABLED, 
				10, 400, 300, 25, hwnd, (HMENU)IDC_TEXT_STATUS_TABLE, hInst, 0);

}

//message handler
LRESULT CALLBACK WinProc(HWND hwnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{

 PAINTSTRUCT ps ;

 switch(Msg)
  {
	case WM_CREATE:
        //empty for now
		break;

	case WM_COMMAND:
		{
		switch(LOWORD(wParam))
			{
			case IDC_BUTTON:
				SYSTEM->flush(IDC_TEXT_DISPLAY_PLANE);
				SYSTEM->flush(IDC_TEXT_DISPLAY_PORT);

				if(!SYSTEM->getActiveOperation())
				{
					//SetFocus((HWND)IDC_BUTTON_STOP);
					 // Create the thread to begin execution on its own.
					String * data = new String(SYSTEM->GetText(IDC_TEXT_INTERVAL));
					_beginthread(run, 0, (void *)data );

				}
			break;
			case IDC_BUTTON_STOP:
				SYSTEM->setActiveOperation(false);
			break;

			case IDC_BUTTON_RESET:
				SYSTEM->SetText(IDC_TEXT_INTERVAL, SIMTIME );
				SYSTEM->SetText(IDC_TEXT_SLEEP_TIME, SLEEPTIME );
			break;

			case IDC_BUTTON_EXIT:
				ExitProcess(0);
			break;

			}
		}
		break;
	case WM_PAINT:
      BeginPaint (hwnd, &ps) ;

      EndPaint (hwnd, &ps) ;
      break;
    case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
    case WM_DESTROY:
		PostQuitMessage(0);
		break;  
  }
  return DefWindowProc(hwnd, Msg, wParam, lParam);
}

BOOL InitializeWindow(HWND hwnd, HINSTANCE hInst, int CmdShow)
{
	//Name of System
	TCHAR* Simple = TEXT(SYSTEM_NAME);
	//windows basic handlers
	WNDCLASS wc;
	//The WNDCLASS struct wc is filled here by assigning all members of the struct
    wc.lpfnWndProc = WinProc; 
    wc.hInstance = hInst;
    wc.style = CS_BYTEALIGNCLIENT; 
    wc.lpszMenuName = NULL; 
    wc.lpszClassName = Simple;
    wc.hIcon = LoadIcon(hInst, NULL);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW); 
    wc.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH); //set background color
    wc.cbWndExtra = 0; 
	wc.cbClsExtra = 0; 

	if (!RegisterClass(&wc))
		MessageBox(hwnd, TEXT("Register class failed!"), TEXT(""), MB_OK);
	//Create Window
	hwnd = CreateWindow(Simple, Simple, WS_OVERLAPPEDWINDOW |WS_VISIBLE, 
			CW_USEDEFAULT, CW_USEDEFAULT, 720, 480, HWND_DESKTOP, NULL, hInst, NULL); 

	if (!hwnd){
		MessageBox(hwnd, TEXT("Error Occured In Creating Window"), TEXT(""), MB_OK);
	}
	//set the main window
	SYSTEM->setHwndMain(hwnd);
	//create the window items
	CreateWindowItems(hwnd, hInst);
	ShowWindow(hwnd, CmdShow);
	UpdateWindow(hwnd); 
	//All is good
	return TRUE;

}

int WINAPI WinMain (HINSTANCE hInst, HINSTANCE hPrev,  LPSTR lpCmd, int nShow)
{
	HWND hwnd = NULL;
    MSG Msg;
	InitializeWindow(hwnd, hInst, nShow);

	while(GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}
	return (int)Msg.wParam;
}

/*
* run funcion efficiency O(n)
*/
void run(void * param)
{
	// initialize objects for use
	bool failed = false;

	const wchar_t * data = (*((String*)param)).c_str();

	//ensure interval is not set to zero
	int interval;
	int sleep_time;

	//catch stoi exception for string to int
	try
	{
		interval = (stoi(String(data)) != 0 ? stoi(String(data)) : stoi(SIMTIME) );
		sleep_time = stoi(SYSTEM->GetText(IDC_TEXT_SLEEP_TIME).c_str());

		//ensure numbers are positive integers
		if(interval < 0)
			interval *= -1;

		if(sleep_time < 0)
			sleep_time *= -1;

	}
	catch(invalid_argument e)
	{
		SYSTEM->SetText(IDC_TEXT_STATUS, e.what());
		SYSTEM->SetText(IDC_TEXT_STATUS, "the argument entered was invalid");
		return;
	}

	//reset interval if it was incorrectly set
	SYSTEM->SetText(IDC_TEXT_INTERVAL, SYSTEM->toString<int>(interval) );

	AirPlaneGenerator gen(interval);


	PortOfArrival* lstPorts[5];

	lstPorts[0] = (PortOfArrival*) new PortOfArrival("PORT-A");
	lstPorts[1] = (PortOfArrival*) new PortOfArrival("PORT-B");
	lstPorts[2] = (PortOfArrival*) new PortOfArrival("PORT-C");
	lstPorts[3] = (PortOfArrival*) new PortOfArrival("PORT-D");
	lstPorts[4] = (PortOfArrival*) new PortOfArrival("PORT-E");

	LandingRoad * lstRoads[5];

	lstRoads[0] = (LandingRoad*) new LandingRoad("A", 5, 0, "MAIL");
	lstRoads[1] = (LandingRoad*) new LandingRoad("B", 12, 0, "SMALL PASSENGER");
	lstRoads[2] = (LandingRoad*) new LandingRoad("C", 10, 0, "GOVERNMENT");
	lstRoads[3] = (LandingRoad*) new LandingRoad("D", 9, 0, "LARGE PASSENGER");
	lstRoads[4] = (LandingRoad*) new LandingRoad("E", 6, 0, "VIP");

	string headers[] = {"LRoad", "Max Size", "NPlanes", "Port", "Accepted"};

	//set initial static datas to gui for ports and roads
	for(int i = 0; i < 5; i++)
	{
		int header = TABLE_ELEMENT(i, 0, 5);
		int road = TABLE_ELEMENT(0, (i+1), 5);
		int max_size = TABLE_ELEMENT(1, (i+1), 5);
		int port = TABLE_ELEMENT(3, (i+1), 5);
		//set names
		SYSTEM->SetText(header, headers[i]);
		SYSTEM->SetText(road, lstRoads[i]->getName());
		SYSTEM->SetText(port, lstRoads[i]->getName());
		SYSTEM->SetText(max_size, SYSTEM->toString(lstRoads[i]->getMaxSize()) );
	}


	//set activeOperation
	SYSTEM->setActiveOperation(true);
	SYSTEM->SetText(IDC_TEXT_STATUS, "SIMULATION ACTIVE" );

	// cycle based on the number of passes given
	while ( !failed && SYSTEM->getActiveOperation()) {
		// try to take a plane from the generator
		AirPlane* p = gen.getNext();
		// if an airplane is landing then add it to the correct landing road then port of arrival
		if (p != NULL) {
			switch (p->getType()) {
				case JFF_MAIL:					// mail airplanes are to be placed in Port-A
					failed = performLanding(lstRoads[0], p);
					if(failed)
						displayFailedMessage(lstRoads[0], p);
					break;
				case JFF_SMALLPASS:			// small passenger airplanes are to be placed in Port-B
					failed = performLanding(lstRoads[1], p);
					if(failed){
						SYSTEM->display(IDC_TEXT_DISPLAY_PLANE," SYSTEM: Performing Overflow", 1);
						failed = performLanding(lstRoads[0], p);
						if(failed)
							displayFailedMessage(lstRoads[1], p);
					}
					break;
				case JFF_GOVERNMENT:		// gonvernment airplanes are to be placed in Port-C
					failed = performLanding(lstRoads[2], p);
					//failed try to add to overflow road B
					if(failed){
						failed = performLanding(lstRoads[1], p);
						SYSTEM->display(IDC_TEXT_DISPLAY_PLANE," SYSTEM: Performing Overflow", 1);
						//failed try to add to overflow road A
						if(failed){
							SYSTEM->display(IDC_TEXT_DISPLAY_PLANE," SYSTEM: Performing Overflow", 1);
							failed = performLanding(lstRoads[0], p);
							if(failed)
								displayFailedMessage(lstRoads[2], p);
						}
					}
					break;
				case JFF_LARGEPASS:			// large passenger airplanes are to be placed in Port-D
					failed = performLanding(lstRoads[3], p);
					if(failed){
						displayFailedMessage(lstRoads[3], p);
					}
					break;
				case JFF_VIP:						// V.I.P. airplanes are to be placed in Port-E
					failed = performLanding(lstRoads[4], p);
					//failed try to add to overflow road D
					if(failed){
						SYSTEM->display(IDC_TEXT_DISPLAY_PLANE," SYSTEM: Performing Overflow", 1);
						failed = performLanding(lstRoads[3], p);
						if(failed)
							displayFailedMessage(lstRoads[4], p);
					}
					break;
				default:
					SYSTEM->display(IDC_TEXT_DISPLAY_PLANE," SYSTEM: AirPlane Has Crashed Because It Cannot Land!", 1);
					failed = true;
					break;
			}

		}

		//ensure plane did not fail to land before 
		//1. showing new display
		//2. trying to perform porting for airplanes
		if(!failed)
		{
			//perform porting operations
			performPorting(lstRoads[0], lstPorts[0]);//A
			performPorting(lstRoads[1], lstPorts[1]);//B
			performPorting(lstRoads[2], lstPorts[2]);//C
			performPorting(lstRoads[3], lstPorts[3]);//D
			performPorting(lstRoads[4], lstPorts[4]);//E

			int cnt = totalAcceptedPlanes(lstPorts);

			if(cnt == TOTALPLANES)
				failed = true;

			//display simulation
			display(lstRoads, lstPorts, cnt);

			//Sleep for reducing speed of program mainly display
			Sleep(sleep_time);
		}
	}

	//TODO: check if list are empty and destroy if necessary

	SYSTEM->SetText(IDC_TEXT_STATUS, "SIMULATION ENDED" );
	//set activeOperation
	SYSTEM->setActiveOperation(false);

	_endthread();
}

/*
* performLanding function efficency O(1)
*/
bool performLanding(LandingRoad * road, AirPlane * p)
{
	if(!road->enqueue(p))
	{
		return true;
	}

	return false;
}

/*
* performPorting function efficency O(1)
*/
void performPorting(LandingRoad * road, PortOfArrival * port)
{
	if(!road->isEmpty())
	{
		if (port->canAccept())
			port->addAirPlane(road->dequeue());
	}
}

/*
* DisplayFailedMessage function efficiency O(1)
*/
void displayFailedMessage(LandingRoad * road, AirPlane * p)
{
	SYSTEM->display(IDC_TEXT_DISPLAY_PLANE,
							" SYSTEM:  AirPlane#"+SYSTEM->toString<long>(p->getIdnum())+" Has Crashed Because Landing Road "+ road->getName()+" Is Not Free!", 1);
}

/*
* totalAcceptedPlanes function efficiency O(1)
*/
int totalAcceptedPlanes(PortOfArrival * lstPorts[])
{
	int total = 0;

	total += lstPorts[0]->totalAccepted();
	total += lstPorts[1]->totalAccepted();
	total += lstPorts[2]->totalAccepted();
	total += lstPorts[3]->totalAccepted();
	total += lstPorts[4]->totalAccepted();

	return total;
}

/*
* Display function efficency O(1)
*/
void display(LandingRoad * lstRoads[], PortOfArrival * lstPorts[], int cnt)
{
	//set initial static datas to gui for ports and roads
	//for(int i = 0; i < 5; i++)
	//{
		int road_1 = TABLE_ELEMENT(2, 1, 5);
		int road_2 = TABLE_ELEMENT(2, 2, 5);
		int road_3 = TABLE_ELEMENT(2, 3, 5);
		int road_4 = TABLE_ELEMENT(2, 4, 5);
		int road_5 = TABLE_ELEMENT(2, 5, 5);

		int port_1 = TABLE_ELEMENT(4, 1, 5);
		int port_2 = TABLE_ELEMENT(4, 2, 5);
		int port_3 = TABLE_ELEMENT(4, 3, 5);
		int port_4 = TABLE_ELEMENT(4, 4, 5);
		int port_5 = TABLE_ELEMENT(4, 5, 5);

		//set data roads
		SYSTEM->SetText(road_1, SYSTEM->toString<int>(lstRoads[0]->getCurrSize()) );
		SYSTEM->SetText(road_2, SYSTEM->toString<int>(lstRoads[1]->getCurrSize()) );
		SYSTEM->SetText(road_3, SYSTEM->toString<int>(lstRoads[2]->getCurrSize()) );
		SYSTEM->SetText(road_4, SYSTEM->toString<int>(lstRoads[3]->getCurrSize()) );
		SYSTEM->SetText(road_5, SYSTEM->toString<int>(lstRoads[4]->getCurrSize()) );

		//set daa ports
		SYSTEM->SetText(port_1, SYSTEM->toString<int>(lstPorts[0]->totalAccepted()) );
		SYSTEM->SetText(port_2, SYSTEM->toString<int>(lstPorts[1]->totalAccepted()) );
		SYSTEM->SetText(port_3, SYSTEM->toString<int>(lstPorts[2]->totalAccepted()) );
		SYSTEM->SetText(port_4, SYSTEM->toString<int>(lstPorts[3]->totalAccepted()) );
		SYSTEM->SetText(port_5, SYSTEM->toString<int>(lstPorts[4]->totalAccepted()) );
	//}

	SYSTEM->SetText(IDC_TEXT_STATUS_TABLE, " TOTAL NUMBER OF PLANES: "+SYSTEM->toString(cnt) );
}
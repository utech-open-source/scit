#ifndef JFF_PORTOFARRIVAL_H
#define JFF_PORTOFARRIVAL_H

#include "airplane.h"
#include <iostream>
#include <string.h>

#include "utility.h"

using namespace std;

class PortOfArrival {
	private:
		bool lstAccept[5];						// used to tell whether the port will accept a plane or not
		int cursor;										// used to mark the current acceptance
		char name[12];								// the given name of this port
		int total;										// tells how many airplanes have used this port
	public:
		// -- Constructor --
		PortOfArrival();							// default constructor
		PortOfArrival(char*);					// primary constructor
		//Accessor
		char * getName() const;
		// -- Other Methods --
		void addAirPlane(AirPlane*);	// attempts to add an airplane to the port
		bool canAccept();							// tells whether this port currently can accept an airplane
		int totalAccepted() const;		// returns the total number of airplanes that have used this port
};
#endif
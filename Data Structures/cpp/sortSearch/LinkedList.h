#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "node.h"

template <class T>
class LinkedList {

private:
	Node<T> * head;
	Node<T> * getHead();


public:
	//List of LinkedList operations
	LinkedList();
	LinkedList(LinkedList<T> &); //copy constructure
	LinkedList & createList() const;
	void insertAtFront(Node<T> *, T data);
	void insertAtBack(Node<T> *, T data);
	//void insertMiddle(Node<T> *, T data);
	void deleteAtFront();
	void deleteAtBack();
	void deleteAtMiddle(Node<T> *);
	void destroyList();
	//utility function
	int size();
	void display();

};

template <class T>
LinkedList<T>::LinkedList()
{
	head = NULL;
}

template <class T>
LinkedList & LinkedList<T>::createList()
{
	return new LinkedList<T>();
}

template <class T>
void LinkedList<T>::destroyList()
{
	Node<T> * tmp = head;
	Node<T> * next = NULL;

	if(head != NULL)
	{
		tmp = head.getNextNode();

		while(tmp != NULL)
		{
			next = tmp.getNextNode().getNextNode();
			delete tmp;

			tmp = next;
		}
		//delete tmp and teh head
		delete tmp;
		delete head;
	}
}


template <class T>
Node<T> * LinkedList<T>::getHead()
{
	Node<T> * tmp = head;
 	return tmp;
}


template <class T>
void LinkedList<T>::insertAtFront(Node<T> * head, T data)
{
	Node<T> * temp = new Node<T>(data);

	if(temp != NULL)
	{
		

	}
}


template <class T>
void LinkedList<T>::insertAtBack(Node<T> * head, T data)
{
	Node<T> * temp = new Node<T>(data);

	if(temp != NULL)
	{
		

	}
}

//utility function
template <class T>
int size()
{
	Node<T> * tmp = head;
	int cnt = 0;

	while(tmp != NULL)
	{
		cnt++;

		tmp = tmp.getNextNode();
	}

	return cnt;
}

/*
* display - Traverse the list and print values
*/
template <class T>
void display()
{
	Node<T> * tmp = head;

	while(tmp != NULL)
	{
		//TODO: implement the print for the generic class
		tmp = tmp.getNextNode();
	}
}
#endif
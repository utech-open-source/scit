//program to play tictactoe
//Author: Mark Robinson

#include<stdio.h>

#define nums 9

void printBoard(int*); //prints game screen
void setBoard(int*); //refresh game screen
int checkWinner(int*, int,int);
int inArray(int*, int, int);

int main()
{

	char name[10];
	int pieces[nums];
	int player_move;
	int player1 = 1;
	int player2 = 2;
	char player_char1 = 'X';
	char player_char2 = 'O';
	int moves_made = 0;

	printf("\nplayer %c move\n", player_char1); //which player is to move
	setBoard(pieces);
	printBoard(pieces);

	while(-1)
	{
		printf("\nmake your move (0 to quit): ");
		scanf("%d", &player_move);
		system("cls");

		player_move = player_move - 1;

		//check if player move is not greater than nums of move available
		if( player_move < nums)
		{
			//check if move is a valid move
			if(pieces[player_move] != player_char1 && pieces[player_move] != player_char2)
			{
				if(moves_made%2 == 0)
				{
					pieces[player_move] = player_char1;
					printf("\nplayer %c move\n", player_char2); //which player is to move
				}
				else //player 2 move
				{
					pieces[player_move] = player_char2;
					printf("\nplayer %c move\n", player_char1); //which player is to move
				}

				moves_made = moves_made + 1;
				printBoard(pieces); //refresh the pieces on the board and reprint the board
			}
			else
			{
				printf("\nyour unable to move there try again\n");
			}
		}

		//break if player_move == 0
		if( player_move == -1)
		{
			break;
		}

		//break if player1 wins
		if(checkWinner(pieces, player_char1, player1) == 1)
		{
			printf("\n\tplayer %c is the winner\n", player_char1);
			break;
		}

		//break if player2 wins
		if(checkWinner(pieces, player_char2, player2) == 1)
		{
			printf("\n\tplayer %c is the winner\n", player_char2);
			break;
		}

		//break if its a draw
		if(moves_made == 9)
		{
			printf("its a draw");
			break;
		}

	}

	system("pause");

	return 0;
}

void printBoard(int* pieces)
{
	printf("\n\t | TIC TAC TOE |\n");
	printf("\n\t  %c  |  %c  |  %c   \n", pieces[0], pieces[1], pieces[2]);
	printf("\t-----+-----+----\n");
	printf("\t  %c  |  %c  |  %c   \n", pieces[3], pieces[4], pieces[5]);
	printf("\t-----+-----+----\n");
	printf("\t  %c  |  %c  |  %c   \n", pieces[6], pieces[7], pieces[8]);
}


void setBoard(int* pieces)
{
	int i;

	for(i=0; i<nums; ++i)
	{
		pieces[i] =  i+49;
	}
}

int checkWinner(int* pieces, int player_char, int player)
{
	int i, pcount = 1;
	int sec[6] = {0}, pos[9] = {0};

	//seperate player moves into sectors
	for(i=1; i<=nums; i++)
	{
		if(pieces[i-1] == player_char)
		{
			sec[pcount] = i;
			pcount += 1;
		}
	}

	//check matching only if pcount is greater than 3
	if(pcount > 3)
	{
		for(i = 1; i <= nums; i++)
			pos[i] = inArray(sec, i, 6);

		//check if found is through center at 5
		if(pos[5]
		&&( (pos[1] && pos[9]) || (pos[2] && pos[8]) || (pos[3] && pos[7]) || (pos[4] && pos[6]) )
			)
			return 1;
		else
			if( pos[1]
			&& ((pos[2] && pos[3]) || (pos[4] && pos[7]))
				|| pos[9]
			&& ((pos[3] && pos[6]) || (pos[7] && pos[8]))
				)
				return 1;

	}

	return 0;
}

int inArray(int *Array, int num, int count)
{
	int i;

	for(i = 0; i < count; i++)
		if( *(Array+i) == num)
			return 1;

	return 0;
}

/*
Name: Mark Robinson
Date: 16-feb-2013
*/

#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<math.h>
#include<string.h>

#define true 1
#define false 0

#define BUFFSIZE 1020

//banks
#define FCIB "FCIB"
#define FGB "FGB"
#define BNS "BNS"
#define NCB "NCB"

//currencies
#define US "USD"
#define GB "GBD"
#define CA "CAD"

#define BUY_RATE 0
#define SELL_RATE 1

//file
#define MODE_A "ab"
#define MODE_W "wb"
#define MODE_R "rb"
#define MODE_RP "r+b"
#define MAX_RECORD 365
#define EXTENSION ".ex"

//define messages
#define NEW_RECORD "NEW RECORD ADDED"
#define NO_RECORD "NO RECORD IN FILE"
#define NOT_FOUND "NO RECORD FOUND"
#define RECORD_DELETED "RECORD DELETED FROM FILE"
#define FILE_LOADED "FILE DATA LOADED"
#define DATE_FORMATE "dd-mmm-yyyy"
#define OUT_OF_MEMORY "OUT OF MEMORY"
#define EXCHANGE_UNABLE_TO_LOAD "EXCHANGE DATA COULD NOT BE SAVED"

//define case messages
#define MESSAGE_1 "GENERATE BUYING AND SELLING PRICES"
#define MESSAGE_2 "DISPLAY FOREIGN EXCHANGE SUMMARY"
#define MESSAGE_3 "BUY FOREIGN EXCHANGE"
#define MESSAGE_4 "SELL FOREIGN EXCHANGE"
#define MESSAGE_5 "WELCOME TO ABOUT PAGE"
#define MESSAGE_DEFAULT "OPTION INVALID PLEASE TRY AGAIN"


struct currency
{
	char *name;
	double val;
};

struct bank
{
	struct currency *currencies;
	char *name;
	int size_name;
};

struct exchange
{
	struct bank *banks;
	char *date;
	int bk_count;
	int cur_count;
};

struct exrecord
{
	struct exchange* exs;
	int count;
};

//create a typdef record
typedef struct exrecord record;

//function prototypes
void welcome();
void printMenu();
void printmsg(char *);
void prepScreen(char *);
void reload();
void printAbout();
//currency
double getRandCurrency();
double genrateRand(int, int, int);
void initCurrency(struct bank*, char**, int);
int getUserCur(char **, int);
char **getActiveCur(int);
char **getActiveBank(int);
//banks
void initBanks(struct bank*, char**, char**,int, int);
void printBanksInfo(struct bank*, int, int, int, int);
//exchange
struct exchange* getEmptyEx(char **, char **,int, int);
void initExchange(char *, char*, char **, char **, int, int);
struct exchange *getExchange(char *, char **, char **, int);
int saveExchange(struct exchange*, int, char *, char *);
size_t calOffset(struct exchange*);
//file
int fileExist(char *);
int initFile(char *, char **, char **);
int calRecordPos(char *);
void InputOutputOperation( struct exchange *, size_t(*foo)(void *, size_t, size_t, FILE *), FILE *);
//utility
char *getDate();
int getMonth(char *);
int getVal();
char getChar();
int checkDate(char *);
char *getString(char *, int, int);
int cmpstr(const char *, const char *);
int isEmpty(record *);
//records
void initRecords(record*, int);
void showRecord(record*, int);
void showAllRecord(record *, int);
record *getExRecord(char *,char **, char **, int);
//transaction
void transaction(int, record *, char **, char **);


void main()
{
	int a, option;

	//set records to NULL
	record *ptrRecord = NULL;

	char *filename;
	char *cdate;

	//currencies and banks
	char *curs[] = {US,GB,CA};
	char *bks[] = {FCIB, FGB, BNS, NCB};

	int size_cur = sizeof(curs)/sizeof(char*);
	int size_bks = sizeof(bks)/sizeof(char*);

	//seed random number
	srand(time(NULL));

	welcome();

	do
	{
		printf("\n\t\tENTER OPTION: ");

		fflush(stdin);
		option = (int)getChar();

		switch(option - 48)
		{
		case 1: //initialize and reload exchanges from file
			prepScreen(MESSAGE_1);
			cdate = getDate();

			if(calRecordPos(cdate))
			{
				filename = strcat(getString(cdate, 7, 11), EXTENSION);

				if(!fileExist(filename))
					initFile(filename, bks, curs);
				initExchange(cdate, filename, bks, curs, size_cur, size_bks);
			}

			//print new record msg
			printmsg(NEW_RECORD);
			break;
		case 2:	//display records found
			prepScreen(MESSAGE_2);
			cdate = getDate();

			if(calRecordPos(cdate))
			{
				filename = strcat(getString(cdate, 7, 11), EXTENSION);
				if(fileExist(filename))
					ptrRecord = getExRecord(filename, bks, curs, calRecordPos(cdate)*2 );
				if(!isEmpty(ptrRecord))
				{
					showRecord(ptrRecord, getUserCur(curs, ptrRecord->exs[0].cur_count));
				}
			}
			break;
		case 3: //buy exchange
			prepScreen(MESSAGE_3);
			cdate = getDate();

			if(calRecordPos(cdate))
			{
				filename = strcat(getString(cdate, 7, 11), EXTENSION);
				if(fileExist(filename))
					ptrRecord = getExRecord(filename, bks, curs, calRecordPos(cdate)*2 );

				if(!isEmpty(ptrRecord))
				{
					showAllRecord(ptrRecord, BUY_RATE);
					transaction(BUY_RATE, ptrRecord, bks, curs);
				}
			}
			break;
		case 4: //sell exchange
			prepScreen(MESSAGE_4);
			cdate = getDate();

			if(calRecordPos(cdate))
			{
				filename = strcat(getString(cdate, 7, 11), EXTENSION);
				if(fileExist(filename))
					ptrRecord = getExRecord(filename, bks, curs, calRecordPos(cdate)*2 );

				if(!isEmpty(ptrRecord))
				{
					showAllRecord(ptrRecord, SELL_RATE);
					transaction(SELL_RATE, ptrRecord,bks, curs);
				}
			}
			break;
		case 5:
			prepScreen(MESSAGE_5);
			printAbout();
			break;
		case 0: //attempt free memory of ptrRecord
			free(ptrRecord);
			exit(1);
			break;
		default:
			printmsg(MESSAGE_DEFAULT);
			break;
		}

		//reload
		reload();

	}while(-1);

	system("pause");
}

void prepScreen(char *msg)
{
	system("cls");
	printmsg(msg);
	printf("\n");
}

void reload()
{
	printf("\n\n");
	system("pause");
	system("cls");
	printMenu();
}

void welcome()
{
	printf("\n\
		   --------------------------------\n\
		   |   WELCOME TO FOREIGN EXCHANGE  |\n\
		   |                                |\n\
		   |        BY: MARK ROBINSON       |\n\
		   |                                |\n\
		   |        ID NUM: 1204716         |\n\
		   --------------------------------\n\
		   ");

	reload();
}

void printMenu()
{
	printf("\n\
		   1. Generate Buying/Selling Price\n\n\
		   2. Display Foreign Exchange Summary\n\n\
		   3. Buy Foreign Exchange\n\n\
		   4. Sell Foreign Exchange\n\n\
		   5. About\n\n\
		   0. Exit\n\n\
		   ");
}

void printAbout()
{
	printf("\n\n\
		   Campus Forex Brokers (CFB) is a startup company on the UTech				\n\
		   campus, owned, operated and managed by an enterprizing group				\n\
		   of Computing (SCIT) and Business Administration (BisAdd) students.			\n\
		   ThecComany plans to operate as a foreign currency brokerage for			\n\
		   four banks: FCIB, FGB, BNS and NCB. The currencies it will deal			\n\
		   with initially are US dollars, Great Britain pounds, and Canadian dollars.	\n\
		   It will store the selling and buying price for any given day, and			\n\
		   allow that information to be displayed. Customers can then buy or			\n\
		   sell foreign currency from/to the brokerage on behalf of the banks.		\n\
		   Before the company goes fully online, it wants you to write a				\n\
		   simulation of how the company's system will function.						\n\
		   ");

}

void printmsg(char *msg)
{
	printf("\n\t\t%s\n",
		msg);
}

int isEmpty(record *records)
{
	if(records == NULL)
	{
		printmsg(NO_RECORD);
		return true;
	}
	else //if date matches dateformate then record isEmpty
		if( strcmp(records->exs[0].date, DATE_FORMATE) == 0)
		{
			printmsg(NO_RECORD);
			return true;
		}
		else
		{
			return false;
		}
}

double getRandCurrency()
{
	return genrateRand(80, 150, 100);
}

/*
** generate random number
** offset: used to specify where the rand number starts
**		   gerating from
** limit : max a random number generates to
** dec   : used to specify decimal place
*/
double genrateRand(int offset, int limit, int dec)
{
	//prevent 0 for decimal
	double r;

	if(dec == 0)
		dec = 1;

	r = ((offset*dec)+rand()%((limit-offset)*dec));
	return r/dec;
}

int getMonth(char *mnt)
{
	char *months[] = {"jan", "feb", "mar", "apr","may", "jun", "jul", "agu", "sep", "oct", "nov", "dec" };

	int i = 0, cnt = sizeof(months)/sizeof(char*);

	while(i < cnt)
	{
		if(strcmp(months[i], mnt) == false)
		{
			return i;
		}
		i++;
	}

	return -1;
}

int checkDate(char *date)
{
	int i=0, dashcount =0, day, year;

	int mnt;

	while(date[i] != '\0')
	{
		i++;
	}

	if(i < 11)
	{
		return false;
	}
	//check for - in dates
	if(date[2] != '-' && date[6] != '-')
	{
		return false;
	}

	day = atoi(getString(date, 0, 2));
	mnt = getMonth(getString(date, 3, 6));
	year = atoi(getString(date, 7, 11));

	//check day
	if(!(day > 0 && day <= 31))
	{
		return false;
	}
	else
		if(year < 2012)
		{
			return false;
		}
		else
			if(mnt == -1)
			{
				return false;
			}

			return true;
}

/*
**
*/
int getVal()
{
	int val;
	scanf("%d", &val);
	return val;
}

char getChar()
{
	char val;
	scanf("%c", &val);
	return val;
}

/* getDate used to get date from user in the fromate dd-mmm-yyyy
**
*/
char* getDate()
{
	char *date = (char *)malloc(sizeof(char)*strlen(DATE_FORMATE));

	do
	{
		printf("Enter a valid date in formate '%s': ", DATE_FORMATE);
		scanf("%s", date);

	}
	while(!checkDate(date));

	return date;
}

char * getString(char * input, int start, int end)
{
	int i,len = end - start;
	char *var = (char *)malloc(sizeof(char)*len+1);

	for(i=start; i<end; i++)
	{
		var[i-start] = input[i];
	}

	var[len] = '\0';

	return var;
}

int cmpstr(const char *string1, const char *string2)
{
	int c,s;

	c = strlen(string2);

	for(s=0; s<c; s++)
	{
		if(string1[s] != string2[s] )
		{
			return false;
		}
	}
	return true;
}

int getUserCur(char **curs, int cur_count)
{
	int fcurrency, i;

	//print menu for currency
	for(i = 0; i < cur_count; i++)
	{
		if(i == 0)
			printf("\n");
		printf("\t( %d ) %s\n", i+1, curs[i]);
	}

	//select currency
	do
	{
		printf("\nSelect currency: ");
		fcurrency = getVal();
	}
	while(fcurrency < 1 || fcurrency > cur_count);

	return fcurrency-1;
}

void initCurrency(struct bank* bk, char **curs, int size_cur)
{
	int c;

	for(c = 0; c < size_cur; c++)
	{
		bk->currencies[c].name = (char *)malloc(sizeof(char)*strlen(curs[c]));
		bk->currencies[c].name = curs[c];
		bk->currencies[c].val = 0;
	}
}

void initBanks(struct bank *bk, char **bks, char **curs, int size_bks, int size_cur)
{
	int b,c;

	for(b=0; b < size_bks; b++)
	{
		//initialize banks
		bk[b].name = bks[b];
		bk[b].size_name = strlen(bks[b]) + 1;
		initCurrency(&bk[b], curs, size_cur);
	}

}

void initExchange(char *date, char *filename, char **bks, char **curs, int size_cur, int size_bks)
{
	struct exchange *ex1, *ex2;
	int offset, b, c;

	ex1 = getEmptyEx(bks, curs, size_bks, size_cur);
	ex2 = getEmptyEx(bks, curs, size_bks, size_cur);

	ex1->date = date;
	ex2->date = date;

	for(b=0; b < size_bks; b++)
	{
		for(c=0; c<size_cur; c++)
		{
			ex1->banks[b].currencies[c].val = getRandCurrency();
			ex2->banks[b].currencies[c].val = getRandCurrency();
		}
	}

	offset = calRecordPos(date) * 2;

	if(!saveExchange(ex1, offset, filename, MODE_RP) || !saveExchange(ex2, offset - 1, filename, MODE_RP))
	{
		printmsg(EXCHANGE_UNABLE_TO_LOAD);
	}

}

/* getEmptyEx
** used to generate and return empty exchange pointer
*/
struct exchange* getEmptyEx(char **bks, char **curs, int bkcount, int curcount)
{
	int i;

	struct exchange *ex;
	struct bank *bk;

	ex = (struct exchange*)malloc(sizeof(struct exchange));
	bk = (struct bank*)malloc(sizeof(struct bank)*bkcount);

	if(bk == NULL || ex == NULL)
	{
		printmsg(OUT_OF_MEMORY);
		exit(0);
	}

	for(i=0; i< bkcount; i++)
	{
		bk[i].currencies = (struct currency *)malloc(sizeof(struct currency)*curcount);
		//set bank name to five chars
		bk[i].name = (char *)malloc(sizeof(char)*5);
		bk[i].size_name = 5;
	}

	ex->banks = bk;
	initBanks(ex->banks, bks, curs, bkcount, curcount);

	ex->date = (char *)malloc(sizeof(char)*strlen(DATE_FORMATE));
	//ex->date = DATE_FORMATE;
	ex->bk_count = bkcount;
	ex->cur_count = curcount;

	return ex;
}

/* get exchange rate
**
*/
struct exchange *getExchange(char *filename,char **bks, char **curs, int offset)
{

	FILE *fp;
	int bkcount = 4, curcount = 3, i = 0, b = 0;

	struct exchange *ex = getEmptyEx(bks, curs, bkcount, curcount);

	fp = fopen(filename, MODE_R);

	if(fp == NULL)
	{
		return NULL;
	}


	fseek(fp, calOffset(ex)*(offset) ,SEEK_SET);

	InputOutputOperation(ex, &fread, fp);

	fclose(fp);

	return ex;

}

int saveExchange(struct exchange* ex, int offset, char *filename, char * mode)
{
	int b = 0,i = 0, write;

	//creating a file pointer
	FILE *fp;

	//opening a file write values to it
	fp = fopen(filename, mode);

	if(fp == NULL)
	{
		return false;
	}

	fseek(fp, calOffset(ex)*(offset)  , SEEK_SET);

	InputOutputOperation(ex, &fwrite, fp);

	fclose(fp);

	return true;
}

void printBanksInfo(struct bank* bk, int cur, int size_cur, int size_bks, int option)
{
	int b,i;

	printf("\t\t\tBanks\n%20s \t", "Currency");

	//print banks
	for(b=0; b < size_bks; b++)
	{
		printf("%s \t", bk[b].name);
	}

	//print currency info
	if(option == false)
	{
		for(b=0, i=0; b < size_bks, i< size_cur;  b++, i++)
		{
			printf("\n%20s \t", bk[0].currencies[i].name);

			for(b=0; b < size_bks;  b++)
			{
				printf("%5.2f \t", bk[b].currencies[i].val);
			}
		}
	}
	else
	{

		printf("\n%20s \t", bk[0].currencies[cur].name);

		for(b=0; b< size_bks; b++)
		{
			printf("%5.2f \t", bk[b].currencies[cur].val);
		}
	}
	printf("\n------------------------------------------------------\n");
}

void transaction(int a, record *records, char **bks, char **curs)
{
	int fbank, fcurrency;

	int value;

	int i, bk, cur;

	for(i = 0; i<records->exs[a].bk_count; i++)
	{
		if(i == 0)
			printf("\n");
		printf("\t( %d ) %s\n", i+1, bks[i]);
	}

	//selection of bank
	do
	{
		printf("\nSelect bank: ");
		fbank = getVal();
	}
	while(fbank < 1 || fbank > records->exs[a].bk_count);

	fbank = fbank -1;

	fcurrency = getUserCur(curs, records->exs[a].cur_count);


	//enter an amount greater than zero
	do
	{
		printf("\nEnter amount: ");
		value = getVal();
	}
	while(value < 0);

	//compare bank
	for(i=0; i < 4; i++ )
	{
		if(cmpstr(records->exs[a].banks[i].name, bks[fbank]))
		{
			bk = i;
			break;
		}
	}

	//compare currency
	for(i=0; i < 3; i++ )
	{
		if(cmpstr(records->exs[a].banks[bk].currencies[i].name, curs[fcurrency]))
		{
			cur = i;
			break;
		}
	}

	printf("\n AMOUNT: %.2lf\n",records->exs[a].banks[bk].currencies[cur].val * value);

}

void showRecord(record *records, int cur)
{
	printf("\n-----------------Date: %s-------------------\n", records->exs[0].date);
	printf("BUY RATE\n");
	printBanksInfo(records->exs[BUY_RATE].banks, cur, records->exs[BUY_RATE].cur_count, records->exs[BUY_RATE].bk_count, true);
	printf("SELL RATE\n");
	printBanksInfo(records->exs[SELL_RATE].banks, cur, records->exs[SELL_RATE].cur_count,records->exs[SELL_RATE].bk_count,  true);
}

void showAllRecord(record *records, int option)
{

	printf("\n-----------------Date: %s-------------------\n", records->exs[0].date);
	if(option == BUY_RATE)
	{
		printf("BUY RATE\n");
		printBanksInfo(records->exs[BUY_RATE].banks, NULL, records->exs[BUY_RATE].cur_count, records->exs[BUY_RATE].bk_count, false);
	}
	else if(option == SELL_RATE)
	{
		printf("SELL RATE\n");
		printBanksInfo(records->exs[SELL_RATE].banks, NULL, records->exs[SELL_RATE].cur_count,records->exs[SELL_RATE].bk_count,  false);
	}
}

void initRecords(record* records, int lines)
{
	//set memory allocation for record
	records->count = (int)malloc(sizeof(int));
	records->exs = (struct exchange*)malloc(sizeof(struct exchange)*lines);

	//set records count to lines
	records->count = lines;
}

record *getExRecord(char *filename,char **bks, char **curs, int offset)
{
	int lines = 2, i=0;
	//ensure the records pointer is set to new object
	record *records = (record*)malloc(sizeof(record));
	struct exchange *ex = NULL;

	initRecords(records, lines);

	while(i < lines)
	{
		ex = getExchange(filename, bks, curs, offset-i );
		records->exs[i] = *ex;
		i++;
	}

	return records;
}

int calRecordPos(char *date)
{
	int day, month, pos;

	//year = atoi(getString(date, 7, 11));
	day = atoi(getString(date, 0, 2));
	month = getMonth(getString(date, 3, 6));
	pos = day + (month * 30);

	return pos;
}

int fileExist(char *filename)
{
	FILE *fp;

	fp = fopen(filename, MODE_R);

	if(fp == NULL)
		return false;
	else
		return true;
}

int initFile(char *filename, char **bks, char **curs)
{
	FILE *fp;

	int i, bkcount = 4, curcount = 3;

	struct exchange *ex = getEmptyEx(bks, curs, bkcount, curcount);

	fp = fopen(filename, MODE_W);

	if(fp == NULL)
	{
		return false;
	}

	for(i =0; i < MAX_RECORD * 2; i++)
	{
		//set default date here
		ex->date = DATE_FORMATE;
		InputOutputOperation(ex, &fwrite, fp);
	}

	fclose(fp);

	return true;
}

void InputOutputOperation( struct exchange *ex, size_t(*foo)(void *, size_t, size_t, FILE *), FILE *fp)
{
	int i =0, b =0;

	foo(ex->date, 1, strlen(DATE_FORMATE) + 1, fp);
	foo(&ex->bk_count, sizeof(ex->bk_count), 1, fp);
	foo(&ex->cur_count, sizeof(ex->cur_count), 1, fp);

	while(b < ex->bk_count)
	{

		while(i < ex->cur_count)
		{
			foo(&ex->banks[b].currencies[i].val, sizeof(ex->banks[b].currencies[i].val), 1, fp);
			//printf("%s: %lf\n", ex->banks[b].currencies[i].name, ex->banks[b].currencies[i].val);
			i++;
		}
		//printf("NAME: %s\n", ex->banks[b].name);

		b++;
		i = 0;
	}

}

size_t calOffset(struct exchange* ex)
{
	size_t size;

	int b = 0,i = 0;

	size = (strlen(DATE_FORMATE) + 1) + sizeof(ex->bk_count) + sizeof(ex->cur_count);

	while(b < ex->bk_count)
	{

		while(i < ex->cur_count)
		{
			size += sizeof(ex->banks[b].currencies[i].val);
			i++;
		}

		b++;
		i = 0;
	}

	return size;
}

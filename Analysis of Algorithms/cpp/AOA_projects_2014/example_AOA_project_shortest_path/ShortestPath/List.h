/**
* Generic LinkList
* Author : Mark Robinson
* Date : April 4, 2014 
**/

#ifndef LIST
#define LIST

#include "ListNode.h"

template <typename LISTTYPE>
class List{

private:
	ListNode<LISTTYPE> * front;
	ListNode<LISTTYPE> * back;

public:
	List();
	bool insertAtBack(const ListNode<LISTTYPE> &);
	bool insertAtFront(const ListNode<LISTTYPE> &);
	bool removeFromBack(LISTTYPE &);
	bool removeFromFront(LISTTYPE &);
	bool getFront(LISTTYPE &);
	bool isEmpty() const;
	bool isFull();

};

template <typename LISTTYPE>
List<LISTTYPE>::List():
front(0), back(0)
{
}

template <typename LISTTYPE>
bool List<LISTTYPE>::insertAtBack(const ListNode<LISTTYPE> & node) {
	if(!isFull()) {
		ListNode<LISTTYPE> * temp = new ListNode<LISTTYPE>(node);

		if(isEmpty()) {
			back = front = temp;
		}
		else {

			back->nextPtr = temp;
			temp->prevPtr = back;
			back = temp;
		}

		return true;
	}

	return false;
}


template <typename LISTTYPE>
bool List<LISTTYPE>::insertAtFront(const ListNode<LISTTYPE> & node) {
	if(!isFull()) {

		ListNode<LISTTYPE> * temp = new ListNode<LISTTYPE>(node);

		if(isEmpty()) {
			back = front = temp;
		}
		else {

			temp->nextPtr = front;
			front->prevPtr = temp;
			front = temp;
		}

		return true;

	}

	return false;
}


template <typename LISTTYPE>
bool List<LISTTYPE>::removeFromBack(LISTTYPE & node) {
	if(!isEmpty()) {

		ListNode<LISTTYPE> * temp = back;

		if(front == back) front = back = 0;
		else {
			back = back->prevPtr;
			back->nextPtr = 0;
		}

		node = temp->data;
		delete temp;

		return true;
	}

	return false;
}

template <typename LISTTYPE>
bool List<LISTTYPE>::removeFromFront(LISTTYPE & node) {
	if(!isEmpty()) {

		ListNode<LISTTYPE> * temp = front;

		if(front == back)
			back = 0;

		front = front->nextPtr;

		if(front != 0)
			front->prevPtr = 0;

		node = temp->data;
		delete temp;

		return true;
	}

	return false;
}

template <typename LISTTYPE>
bool List<LISTTYPE>::getFront(LISTTYPE & node) {
	if(!isEmpty()) {
		ListNode<LISTTYPE> * temp = front;
		node = temp->data;
		return true;
	}
	return false;
}

template <typename LISTTYPE>
bool List<LISTTYPE>::isEmpty() const
{
	return front == 0;
}

template <typename LISTTYPE>
bool List<LISTTYPE>::isFull()
{
	LISTTYPE data;
	ListNode<LISTTYPE> * temp = new ListNode<LISTTYPE>(data);
	if(temp != 0 )
	{
		delete temp;
		return false;
	}
	return true;
}

#endif
Examples for Utech Students
=======================


What Is This?
-------------

This set of solutions/examples is intended to provide working examples of 
programming projects and class work. The examples strive to be simple, well 
documented and modification friendly, in order to help students quickly 
learn from them.

These examples are meant to teach you about code-level development using either
c, cpp or java. With knowledge of these solutions students will be better able to
complete their programming assignments without having to re-invent the wheel.


How To Use The Examples
-----------------------

There are three main ways to interact with the examples in this project:

1. Download and compile the source code on your computer.

2. Read the code. Much effort has gone into making the example code readable,
not only in terms of the code itself, but also the extensive inline comments
and documentation blocks.

3. Browse the code and documentation on the web.
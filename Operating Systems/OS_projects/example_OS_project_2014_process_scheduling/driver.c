#include "defines.h"
#include "scheduling_queue.h"
#include "virtual_processor.h"
#include "process_control_block.h"

#ifdef _WINDOWS_VER
    #include <conio.h>
#endif

PROCESSOR_ONE p1 = {0, 0, WAITING, 0};
PCBPtr A = NULL;
int i = 0, number_of_pcb = 0;

PCBPtr kernelLevel();
void AdditionOperations();
bool getArgs(int argc, char *argv[]);

int main(int argc, char *argv[]){
	/*PCB Arr[5];
	Arr[0] = generateProcessControlBlockStatic(1, 2, 2, 6, 10);
	Arr[1] = generateProcessControlBlockStatic(2, 2, 3, 3, 3);
	Arr[2] = generateProcessControlBlockStatic(3, 2, 1, 12, 0);
	Arr[3] = generateProcessControlBlockStatic(4, 2, 2, 4, 7);
	Arr[4] = generateProcessControlBlockStatic(5, 2, 3, 7, 8);
	A = Arr;//*/

    //set the default output for the file
    setFileName("output.txt");

	if(!getArgs(argc, argv))
	{
        printf("\nUsage: OS_Project_V3 -n [number] -o [file]\n-n number of pcb to generate[0<n<=30]\n-o output file default output.txt\n");
        exit(EXIT_SUCCESS);
    }

    //create output file
    createFile();
    //initialize scheduling queues
    createShedulingQueues();

	A = generateProcessControlBlocks(number_of_pcb);
	sortProcessControlBlock(A, number_of_pcb);
	output("\n-----------------------Blocks Snapshot---------------------\n\n");
	displayProcessControlBlocks(A, number_of_pcb);
	output("\n------------------------End Snapshot-----------------------\n\n");
	output("Id\tType\tPrior\tBTime\tATime\tSTime\tETime\tRBTime\n");
	startVirtualProcessor(&p1, &AdditionOperations);

	#ifdef _WINDOWS_VER

	_getch();
	#endif

	output("\n-----------------------Blocks Execution Snapshot---------------------\n\n");
	displayProcessControlBlocks(A, number_of_pcb);
	output("\n------------------------Execution End Snapshot-----------------------\n\n");

	return 0;
}

bool getArgs(int argc, char *argv[])
{
    int x = 0;
    if( (argc - 1) % 2 == 0)
    {
        for(x = 1; x < argc; x++)
        {
            if( strcmp(argv[x], "-n") == 0)
            {
                number_of_pcb = atoi(argv[x+1]); /* convert strings to integers */
            }
            else
                if( strcmp(argv[x], "-o") == 0)
                {
                    setFileName(argv[x+1]);
                }
        }
        if(number_of_pcb > 30 || number_of_pcb < 1)
            return false;

        return number_of_pcb;
    }
    return false;
}

//addition task to do at the begining of each virtual processor cycle
void AdditionOperations()
{
    PCBPtr temp;

	if(i < number_of_pcb)
	{
		while( A[i].arrival_time == p1.cycle )
		{
			//displayProcessControlBlock(A[i]);
			if(AddProcessToShedulingQueue(&A[i]))
			{
				//printf("Added to sheduling Queue Process: %d cycle: %d\n", A[i].process_id, p1.cycle);
				//printf("Count %d\n", getSQBatCnt());
				i++;
			}
		}
	}
	temp = kernelLevel();

	if( temp != NULL) //get pcb from kernel level algorithm
	{
	    //displayProcessControlBlock(*p1.cpu_bound_process);
		executeProcess(temp); //try to execute process
	}

	if( p1.num_of_process_executed == number_of_pcb && p1.state == WAITING )
	{
		stopVirtualProcessor();
        output("\nSIMULATION ENDED!\n");
	}
	//if(p1.cycle == 50) stopVirtualProcessor();
}

PCBPtr ShortestRemainingTimeFirst(int ptype)
{
    PCB tmpBlock;

    if(FrontOfShedulingQueueSys(&tmpBlock))
    {
        //interupt the current running process if the
        //remaining burst time is greater than the min found in system queue
        if( p1.state == RUNNING && p1.cpu_bound_process->process_type == 1 &&
           tmpBlock.remaining_burst_time < p1.cpu_bound_process->remaining_burst_time )
        {
            interuptRuningProcess();
        }
        else
        if(p1.state == WAITING)
        {
            return RemoveFromShedulingQueueSys();
        }
	}
	return NULL;
}

//q = quantum
PCBPtr RoundRobin(int q, int ptype)
{
	if(p1.state == WAITING)
	{
		//pick process based on priority
		return RemoveFromShedulingQueueInt();
	}
	else if(p1.state == RUNNING && p1.cpu_bound_process->process_type == ptype )
	{
		if((p1.cpu_bound_process->burst_time - p1.cpu_bound_process->remaining_burst_time) % q == 0)
		{
			interuptRuningProcess();
		}
	}
	return NULL;
}

PCBPtr PriorityPre(int ptype)
{
	PCB  tmpBlock;

    if(FrontOfShedulingQueueBat(&tmpBlock))
    {
        //interupt the current running process if the
        //priority is greater than the min found in system queue
        if( p1.state == RUNNING && p1.cpu_bound_process->process_type == ptype &&
           tmpBlock.priority < p1.cpu_bound_process->priority )
        {
            interuptRuningProcess();
        }
        else
        if(p1.state == WAITING)
        {
            return RemoveFromShedulingQueueBat();
        }
    }
	return NULL;
}

PCBPtr kernelLevel()
{
    PCBPtr temp = NULL;
    //pick process based on priority and priority types
    int len,order[] = {0, SYSTEM_ALGO, INTERACTIVE_ALGO, BATCH_ALGO};

    for(len = 1; len < 4; len++)
    {
        switch(order[len])
        {
        case SHORTEST_REMAINING_BURST_TIME:
            temp = ShortestRemainingTimeFirst(len);
            break;
        case PRIORITY_PRE:
            temp = PriorityPre(len);
            break;
        case ROUND_ROBIN:
            temp = RoundRobin(ROUND_ROBIN_QUANTUM, len);
            break;
        }

        if(temp != NULL)
            break;
    }

    return temp;
}

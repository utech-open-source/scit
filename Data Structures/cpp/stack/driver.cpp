#include "stack.h"

void main()
{
	cout << "STACKS" <<endl;

	Stack stack;

	stack.push( 20 );
	stack.push( 40 );
	stack.push( 10 );
	stack.push( 30 );

	while(stack.getTop() != NULL)
	{
		cout << stack.getTop()->getData() << endl;
		stack.pop();
	}

	if(stack.checkEmpty())
	{
		cout<<"stack is empty"<<endl;
	}

	system("pause");
}
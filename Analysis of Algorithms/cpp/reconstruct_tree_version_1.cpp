#include<iostream>
#include<string>

using namespace std;

class Node{
public:
	Node * left;
	Node * right;
	int data;

	Node(){
		left = NULL;
		right = NULL;
	}
};

void printLeft(int list[], int start, Node * ptr){
	if( start > 0 ){
		(ptr+start-1)->left = ptr+start;
		(ptr+start-1)->left->data = list[start];
		cout << "Left of("<<list[start-1]<<"):"<< list[start] << endl;
	}
}

void printRight(int list[], int start, int sep, Node * ptr){
	if( start > 0 ){
		(ptr+start-1)->right = ptr+sep;
		(ptr+start-1)->right->data = list[sep];
		cout << "Right of("<<list[start-1]<<"):"<< list[sep] << endl;
	}
}

/*
* Reconstruct Binary Search Tree From PreOrder.
* Uses inplace array where the start and end of the sub array is passed in recursively.
* leftcnt and rightcnt keep track of all the left subarray and the first element of the left subarray
* sep keep track of the seperation between the subarrays left and right.
*/
void reconTreePreOrder(int list[], int start, int end, Node * ptr){
	int leftcnt = 0, rightcnt = 0, sep = end;
	for(int i = start; i < end; i++){
		if(list[i] < list[start-1]) {
			leftcnt++;
		} else {
			sep = i;
			rightcnt++;
			break;
		}
	}
	if( start == 0 ){
		cout << "Root: "<< list[start] << endl;
		ptr->data = list[start];
	}
	if(leftcnt > 0){
		printLeft(list, start, ptr);
		reconTreePreOrder(list, start+1, sep, ptr);
	}
	if(rightcnt > 0){
		printRight(list, start, sep, ptr);	
		reconTreePreOrder(list, sep+1, end, ptr);
	}
}

void preOrder(Node * root){
	if(root != NULL){
		cout << root->data << endl;
		preOrder(root->left);
		preOrder(root->right);
	}
}

void postOrder(Node * root){
	if(root != NULL){
		postOrder(root->left);
		postOrder(root->right);
		cout << root->data << endl;
	}
}

void inOrder(Node * root){
	if(root != NULL){
		inOrder(root->left);
		cout << root->data << endl;
		inOrder(root->right);
	}
}


void main(){

	cout << "Reconstruct Binary Search Tree\n" << endl;
	cout <<"List 1 "<<"{15,13,10,7,12,20,25,40}\n"<<endl;

	int list[] = {15,13,10,7,12,20,25,40};
	int size = sizeof(list)/sizeof(list[0]);

	Node * tree = new Node[size];
	reconTreePreOrder(list,0, size, tree);

	cout << "Pre Order" << endl;
	preOrder(tree);

	cout << "Post Order" << endl;
	postOrder(tree);

	cout << "In Order" << endl;
	inOrder(tree);

	system("pause");
}
#include "stack.h"

Stack::Stack():
top(NULL)
{
}

void Stack::push(char B)
{
	if(!isFull())
	{
		Node * temp = new Node(B);

		if(isEmpty())
			top = temp;
		else
		{
			temp->setNext(top);
			top = temp;
		}
	}

}

Node * Stack::pop()
{
	if(!isEmpty())
	{
		Node * B = NULL;
		Node * temp = top;

		top = top->getNext();

		if(temp != NULL)
			B = new Node(temp->getData());
		
		delete temp;

		return B;

	}
	else
		return NULL;

}


bool Stack::isEmpty()
{
	if(top == NULL)
		return true;
	else
		return false;
}

bool Stack::isFull()
{
	Node * temp = new Node();

	if(temp != NULL)
	{
		delete temp;
		return false;
	}
	else
		return true;
}
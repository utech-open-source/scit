#include "output.h"

string FILENAME = NULL;

/*
** setFileName
*/
void setFileName(string fileName)
{
    FILENAME = malloc(FILENAME_LIMIT);
    sprintf(FILENAME,"%s", fileName);
}

/*
** getFileName
*/
string getFileName()
{
    return FILENAME;
}

/*
** create a file
*/
bool createFile()
{
    FILE * file = NULL;
    if(getFileName() != NULL)
    {
        file = fopen(getFileName(), "w");
        if(file != NULL)
        {
            fclose(file);
            return true;
        }
    }
    return false;
}

/*
** open a file in append mode
*/
FILE * updateFile()
{
    return fopen(getFileName(), "a+");
}

/*
** output
** displays data to screen and uses WriteToFile function
*/
void output(string data)
{
    printf(data);
    if(getFileName() != NULL)
    {
        writeToFile(data);
    }
    else
        printf("\n unable to write to file\n");
}

/*
** WriteToFile
**
*/
void writeToFile(string data )
{
    FILE * file = updateFile();

    if(file != NULL)
    {
        fprintf(file, data);
        fclose(file);
    }
}

#include "Node.h"

Node::Node():
data(0), left(NULL), right(NULL)
{

}

Node::Node(int data):
data(data), left(NULL), right(NULL)
{

}

int Node::getData() const
{
	return this->data;
}

Node * Node::getLeft() const
{
	return this->left;
}

Node * Node::getRight() const
{
	return this->right;
}

void Node::setData(int data)
{
	this->data = data;
}

void Node::setLeft(Node * left)
{
	this->left = left;
}

void Node::setRight(Node * right)
{
	this->right = right;
}
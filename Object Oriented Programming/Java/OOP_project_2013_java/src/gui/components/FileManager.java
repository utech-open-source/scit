package gui.components;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import logging.Log;

public class FileManager {
	private File file;

	public FileManager(String filename){
		this.setFile(new File(filename));
	}
	
	public FileManager()
	{
		file = null;
	}
	
	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	public String readFromFile()
	{
		String str = "";
		
		try{
			Scanner file = new Scanner(getFile());
			
			while(file.hasNextLine())
			{
				str += file.nextLine()+"\n";
			}
			
			file.close();
			
		}catch(IOException e)
		{
			Log.LogError(e.getMessage());
		}
		
		return str;
	}
	
	public String readFromURL(String url)
	{
		
		String str = "";
		
		InputStreamReader streamReader = new InputStreamReader(getClass().getResourceAsStream(url));
		BufferedReader txtReader = new BufferedReader(streamReader);
		
		try {
			
			for (String line; (line = txtReader.readLine()) != null;) {
				str += line+"\n";
			}
			
			txtReader.close();
		} catch (IOException e) {
			Log.LogError(e.getMessage());
		}
		
		return str;

	}
	
}

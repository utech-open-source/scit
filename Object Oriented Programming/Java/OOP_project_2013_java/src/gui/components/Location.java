package gui.components;

public class Location {
	public float widthP;
	public float heightP;
	public boolean center = true;
	
	/**
	 * 
	 * @param widthP
	 * @param heightP
	 * @param center
	 */
	public Location(float widthP, float heightP, boolean center)
	{
		this.widthP = widthP;
		this.heightP = heightP;
		this.center = center;
	}
	
	/**
	 * 
	 * @param widthP
	 * @param heightP
	 */
	public Location(float widthP, float heightP)
	{
		this.widthP = widthP;
		this.heightP = heightP;
	}
}

#include "form.h"
#include "ui_form.h"
#include <QDebug>
#include <QWebFrame>
#include <QWebElement>
#include <QMessageBox>


Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);

    getVertices(this->vertices);

    connect(ui->goButton, SIGNAL(clicked()), this, SLOT(goClicked()));
    connect(ui->pbShowAllMarkers, SIGNAL(clicked()), this, SLOT(initializeMap()));

    connect(&m_geocodeDataManager, SIGNAL(coordinatesReady(double,double,QString)), this, SLOT(showCoordinates(double,double,QString)));
    connect(&m_geocodeDataManager, SIGNAL(errorOccured(QString)), this, SLOT(errorOccured(QString)));

    QWebSettings::globalSettings()->setAttribute(QWebSettings::PluginsEnabled, true);
    ui->webView->setUrl(QUrl("qrc:/html/google_maps.html"));
}

Form::~Form()
{
    delete ui;
}


void Form::displayShortestPath(int type)
{
    QString str = QString("");

    showShortestPath(this->vertices, verticeSize, type);

    Vertice * temp = this->vertices[verticeSize-1];

    clearMarkers();

    while( temp != NULL )
    {
        str+=setMarker(temp, false);

        if(temp->getPred() != NULL)
        {
            Edge edge = Edge(temp->getWeight(), temp->getWeight(), temp->getPred(), temp);
            str+=showRoutes(&edge);
        }

        temp = temp->getPred();
    }

    ui->webView->page()->currentFrame()->documentElement().evaluateJavaScript(str);
}

void Form::initializeMap()
{
    QString str = QString("");

    clearMarkers();

    for(int i = 0; i < verticeSize; i++)
    {
        bool engage = false;
        if(i==0 || i== verticeSize-1 )
            engage = true;

        str+=setMarker(vertices[i], false);

        for(size_t e = 0; e < vertices[i]->getEdges().size(); e++)
        {
            Edge edge = vertices[i]->getEdges().at(e);
            str+=showRoutes(&edge);
        }
    }

    ui->webView->page()->currentFrame()->documentElement().evaluateJavaScript(str);

}

QString Form::showRoutes(Edge * edge)
{
    double fromNorth=0, fromEast=0, toNorth=0, toEast=0;

    fromNorth = edge->getLeft()->getLat();
    fromEast = edge->getLeft()->getLng();
    toNorth = edge->getRight()->getLat();
    toEast = edge->getRight()->getLng();

    QString str =
        QString("createPolyLine(%1, %2, %3, %4);").arg(fromNorth).arg(fromEast).arg(toNorth).arg(toEast);

        //QString("google.maps.event.addListener(line, 'click', function() {")+
          //      QString("var infowindow = new google.maps.InfoWindow({ size: new google.maps.Size(150,50) });")+
          //      QString("infowindow.setContent(%1);").arg(route->getDistance())+
          //      QString("infowindow.open(map,line);")+
        //QString("});");

    qDebug() << str;

    return str;

}

void Form::showCoordinates(double east, double north, QString address)
{

}

QString Form::setMarker(Vertice * vertice, bool engage)
{
    for (int i=0; i<m_markers.size(); i++)
    {
        if (m_markers[i]->getName() == vertice->getName()) return " ";
    }

    QString str = createMarker(QString("new google.maps.LatLng(%1, %2)").arg(vertice->getLat()).arg(vertice->getLng()),
                               QString(vertice->getName().c_str()) + QString(" %1").arg(vertice->getWeight()) , engage);

    m_markers.append(vertice);

    //adding capton to ListWidget
    ui->lwMarkers->addItem(QString(vertice->getName().c_str()) + QString(" %1").arg(vertice->getWeight()));

    return str;
}

QString Form::createMarker(QString latlng, QString html, bool engage)
{
    QString str = QString("createMarker(%1, %2, %3);").arg(latlng).arg("\""+html+"\"").arg(engage);

    qDebug() << str;

    return str;
}

void Form::goClicked()
{
    int type = TIME;
    getVertices(this->vertices);

    if(ui->comboBoxType->currentText().compare("distance", Qt::CaseInsensitive) == 0)
        type = DISTANCE;
    displayShortestPath(type);
}

void Form::errorOccured(const QString& error)
{
    QMessageBox::warning(this, tr("Geocode Error"), error);
}

void Form::on_lwMarkers_currentRowChanged(int currentRow)
{
    if (currentRow < 0) return;
    QString str =
            QString("var newLoc = new google.maps.LatLng(%1, %2); ").arg(m_markers[currentRow]->getLat()).arg(m_markers[currentRow]->getLng()) +
            QString("map.setCenter(newLoc);");

    qDebug() << str;

    ui->webView->page()->currentFrame()->documentElement().evaluateJavaScript(str);
}

void Form::clearMarkers()
{
        m_markers.clear();
        ui->lwMarkers->clear();

        QString str =
                QString("deleteMarkers();deleteOverlays();");
        qDebug() << str;
        ui->webView->page()->currentFrame()->documentElement().evaluateJavaScript(str);
}

void Form::on_pbRemoveMarker_clicked()
{
    if (ui->lwMarkers->currentRow() < 0) return;

    QString str =
            QString("markers[%1].setMap(null); markers.splice(%1, 1);").arg(ui->lwMarkers->currentRow());
    //qDebug() << str;
    ui->webView->page()->currentFrame()->documentElement().evaluateJavaScript(str);

    //deleteing caption from markers list
    delete m_markers.takeAt(ui->lwMarkers->currentRow());
    //deleteing caption from ListWidget
    delete ui->lwMarkers->takeItem(ui->lwMarkers->currentRow());
}

void Form::on_zoomSpinBox_valueChanged(int arg1)
{
    QString str =
            QString("map.setZoom(%1);").arg(arg1);
//    qDebug() << str;
    ui->webView->page()->currentFrame()->documentElement().evaluateJavaScript(str);
}

void Form::resizeEvent(QResizeEvent* event)
{
   QWidget::resizeEvent(event);
   QString str =
           QString("var newLoc = new google.maps.LatLng(%1, %2); ").arg(18.142925).arg(-77.344667)+
           QString("map.setCenter(newLoc);");

   //qDebug() << str;

   //ui->webView->page()->currentFrame()->documentElement().evaluateJavaScript(str);
}

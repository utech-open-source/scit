/**
* Generic Heap
* Date : November 1, 2014 
**/

#ifndef HEAP_H
#define HEAP_H

#include "Node.h"
#include "Queue.h"
#include "Stack.h"

#include<iostream>

using namespace std;

template <typename TYPE>
class Heap{

private:
	Node<TYPE> * front;
	Queue< Node<TYPE> * > heapQueue;
	Queue< Node<TYPE> * > deletionQueue;
	Stack< Node<TYPE> * > heapOrder;
	
public:
	Heap();
	bool insert(TYPE);
	void shiftUp(Node<TYPE> *);
	void shiftDown(Node<TYPE> * node);
	bool isEmpty()const;
	bool isEmpty(Node<TYPE> *)const;
	bool isFull();
	bool isFullParent(Node<TYPE> *)const;
	Node<TYPE> * getEmptyChild(Node<TYPE> *)const;
	TYPE min();
	void removeMin(TYPE&);
	template <typename KEY>
	void decreaseKey(void * wrapper, TYPE data, KEY value);
	template <typename KEY>
	void decreaseKeyUncheck(Node<TYPE> * node, TYPE data, KEY value);
};

template <typename TYPE>
Heap<TYPE>::Heap():
	front(NULL)
{
}

template <typename TYPE>
TYPE Heap<TYPE>::min()
{
	TYPE temp;
	if(!isEmpty(front))
		temp = front->getData();
	return temp;
}

template <typename TYPE>
void Heap<TYPE>::removeMin(TYPE & data)
{
	data = min();
	Node<TYPE> * temp, * parent = NULL;
	heapOrder.pop(temp);

	parent = temp->parent;
	//parent reach the front
	if(parent == NULL)
	{
		front = NULL;
		return;
	}

	if(parent->right == NULL)
	{
		parent->left = NULL;
	}
	else
	{
		parent->right = NULL;
	}
	front->setData(temp->getData());
	//set new wrapper for front -> left
	front->data->setWrapper((void *)front);
	//---------------------------------------//
	temp->setData(data);
	//set new wrapper for front -> left
	temp->data->setWrapper((void *)temp);
	//---------------------------------------//
	delete temp;

	shiftDown(front);
}


template <typename TYPE>
template <typename KEY>
void Heap<TYPE>::decreaseKey(void * wrapper, TYPE data, KEY value)
{
	Node<TYPE> * temp = (Node<TYPE> *)wrapper;
	decreaseKeyUncheck(temp, data, value);
}

template <typename TYPE>
template <typename KEY>
void Heap<TYPE>::decreaseKeyUncheck(Node<TYPE> * temp, TYPE data, KEY value)
{
	//static int i = 0;
	if(temp == NULL)
		return;

	if(temp->getData() == data)
	{
		temp->data->operator=(value);
		shiftUp(temp);
		shiftDown(temp);
		return;
	}
	/*
	//data value must be greater than or equal to the left data value 
	//to be found below it
	if(!isEmpty(temp->left) && data->operator>=(temp->left->data))
		decreaseKeyUncheck(temp->left, data, value);

	//data value must be greater than or equal to the right data value 
	//to be found below it
	if(!isEmpty(temp->right) && data->operator>=(temp->right->data))
		decreaseKeyUncheck(temp->right, data, value);
	*/

	//cout << "TIMES: " << i << endl;
	//i++;
}

template <typename TYPE>
bool Heap<TYPE>::insert(TYPE data)
{
	if(!isFull())
	{
		Node<TYPE> * temp = new Node<TYPE>(data);
		//set new wrapper for parent -> left
		temp->data->setWrapper((void *)temp);

		if(isEmpty(front))
		{
			front = temp;
		}
		else 
		{
			Queue< Node<TYPE> * > * queueTemp = &heapQueue;

			if(!deletionQueue.isQueueEmpty())
			{
				queueTemp = &deletionQueue;
			}

			if(!queueTemp->isQueueEmpty())
			{
				Node<TYPE> * parent = NULL, *old= NULL;
				queueTemp->queueFront(parent);
				
				if(parent->left == NULL)
				{
					parent->left = temp;
					parent->left->parent = parent;
				}
				else
				{
					parent->right = temp;
					parent->right->parent = parent;
				}
				
				if(isFullParent(parent))
				{
					queueTemp->dequeue(old);
				}

				shiftUp(temp);
			}

		}

		heapQueue.enqueue(temp);
		heapOrder.push(temp);

		return true;
	}
	return false;
}

template <typename TYPE>
void Heap<TYPE>::shiftUp(Node<TYPE> * node)
{
	if(node != NULL && node->parent != NULL)
	{
		Node<TYPE> * parent = node->parent;
		TYPE tmp;
		if(node->data->operator<(parent->data) )
		{
			tmp = parent->data;
			parent->setData(node->data);
			//set new wrapper for parent -> data
			parent->data->setWrapper((void *)parent);
			//---------------------------------------//
			node->setData(tmp);
			//set new wrapper for node -> data
			node->data->setWrapper((void *)node);
			//---------------------------------------//
			shiftUp(parent);
		}
	}
}

template <typename TYPE>
void Heap<TYPE>::shiftDown(Node<TYPE> * node)
{
	if(node != NULL)
	{
		Node<TYPE> * minChild = NULL, * parent = node;
		TYPE tmp;

		if(parent->right == NULL)
		{
			if(parent->left == NULL)
				return;
			else
				minChild = parent->left;
		}
		else
		{
			if(parent->left->data->operator<=(parent->right->data) )
				minChild = parent->left;
			else
				minChild = parent->right;
		}
		if(parent->data->operator>(minChild->data) )
		{
			tmp = minChild->getData();
			minChild->setData(parent->getData());
			//set new wrapper for minChild -> data
			minChild->data->setWrapper((void *)minChild);
			//---------------------------------------//
			parent->setData(tmp);
			//set new wrapper for parent -> data
			parent->data->setWrapper((void *)parent);
			//---------------------------------------//
			shiftDown(minChild);
		}
	}
}

template <typename TYPE>
bool Heap<TYPE>::isEmpty(Node<TYPE> * node) const
{
	return node == NULL;
}


template <typename TYPE>
bool Heap<TYPE>::isEmpty() const
{
	return front == NULL;
}


template <typename TYPE>
bool Heap<TYPE>::isFull()
{
	TYPE data;
	Node<TYPE> * temp = new Node<TYPE>(data);
	if(temp != NULL )
	{
		delete temp;
		return false;
	}
	return true;
}

template <typename TYPE>
bool Heap<TYPE>::isFullParent(Node<TYPE> * parent)const
{
	return (parent->left != NULL && parent->right != NULL);
}

template <typename TYPE>
Node<TYPE> * Heap<TYPE>::getEmptyChild(Node<TYPE> * parent)const
{
	if(parent->left != NULL)
		return parent->right;
	else
		return parent->right;
}

#endif
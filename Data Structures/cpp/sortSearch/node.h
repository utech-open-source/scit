#ifndef NODE_H
#define NODE_H

template <class T>
class Node{

private:
	T data;
	Node * nextNode;
	Node * prevNode;

public:
	//constructors
	Node(T data);
	Node(Node<T> *, Node<T> *, T);
	//operator overloads
	Node<T> & operator=(Node<T> &) const;
	bool operator==(Node<T> &);
	//getters and setters
	Node<T> * getNextNode() const;
	Node<T> * getPrevNode() const;
	void setData(T data);
	T getData();
	void setNextNode(Node *);
	void setPrevNode(Node *);
};

#endif

template <class T>
Node<T> & Node<T>::operator=(Node<T> & node)
{
	setNextNode(node.getNextNode());
	setPrevNode(node.getPrevNode);
	setData(node.getData()));
	return *this;
}

template <class T>
bool Node<T>::operator==(Node<T> & node)
{
	if(node.geData() == getData())
	{
		return true;
	}
	return false;
}

template <class T>
Node<T>::Node(T dat)
	:data(dat)
{
	nextNode = NULL;
	prevNode = NULL;
}
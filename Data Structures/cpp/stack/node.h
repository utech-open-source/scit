#ifndef NODE_H
#define NODE_H

#include<iostream>
#include<string>

using namespace std;

class Node{

private:
	int data;
	Node * next;

public:
	Node(int = 0, Node* = NULL);
	Node(const Node&);
	int getData() const;
	void setData(int);
	Node * getNext() const;
	void setNext(Node*);

};

#endif